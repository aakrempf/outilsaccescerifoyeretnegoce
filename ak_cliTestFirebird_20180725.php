#!/usr/bin/php -q
<?php
## IrigaNetworks
## ~~~~~~~~~~~~~
##
##

## ----------------------------------------------------------------------------------------------------
## Fonction usage()
## ----------------------------------------------------------------------------------------------------
function	usage()
{
	global $Script;

	echo "$Script
COMMANDES :
	--testPdo		: Test de l'acces PDO
	--testOdbc	: Test de l'acces ODBC

PARAMETRES :
	[--dsn=<string>]	: mysql ou firebird pour PDO, Nom du driver pour ODBC
	[--dbname=<string>]	: nom de la BDD
	[--host=<string>]	: localhost ou adresse IP du serveur de BDD
	[--cmd=<string>]	: commande SQL à lancer
	[--login=<string>]	:
	[--passwd=<string>]	:
	[--format=<string>]	: TXT par defaut, CSV

	\n";
	exit(1);
}

	## ----------------------------------------------------------------------------------------------------
	## Fonction main()
	## ----------------------------------------------------------------------------------------------------

	## ----------------------------------------------------------------------------------------------------
	## Décodage des paramètres d'appel
	## ----------------------------------------------------------------------------------------------------
	$Script		= basename($_SERVER[argv][0]);

	$params		= array();
	$paramsTxt	= "";
	for ($i=1; $i<$_SERVER[argc]; $i++) {
		$paramsTxt	.= " ".$_SERVER[argv][$i];

		if (ereg("^--(testPdo)$", $_SERVER[argv][$i], $regs))				{ $mode			= $regs[1]; continue; }
		if (ereg("^--(testOdbc)$", $_SERVER[argv][$i], $regs))				{ $mode			= $regs[1]; continue; }

		if (ereg("^--dsn=(.*)$", $_SERVER[argv][$i], $regs))				{ $params[dsn]		= rtrim($regs[1]); continue; }
		if (ereg("^--dbname=(.*)$", $_SERVER[argv][$i], $regs))			{ $params[dbname]	= rtrim($regs[1]); continue; }
		if (ereg("^--host=(.*)$", $_SERVER[argv][$i], $regs))				{ $params[host]	= rtrim($regs[1]); continue; }
		if (ereg("^--cmd=(.*)$", $_SERVER[argv][$i], $regs))				{ $params[cmd]		= rtrim($regs[1]); continue; }
		if (ereg("^--login=(.*)$", $_SERVER[argv][$i], $regs))				{ $params[login]	= rtrim($regs[1]); continue; }
		if (ereg("^--passwd=(.*)$", $_SERVER[argv][$i], $regs))			{ $params[passwd]	= rtrim($regs[1]); continue; }

		echo "Paramètre inconnu :".$_SERVER[argv][$i]."\n";
		usage();
	}

	if (!$mode) {
		usage();
	}

	if (!$params[dsn] || !$params[dbname] || !$params[host] || !$params[login] || !$params[passwd] || !$params[cmd]) {
		echo "tous les parametres sont obligatoires\n";
		usage();
	}

	#------------------------------------------------------------------------------------------------------------
	if ($mode == "testPdo") {
	#------------------------------------------------------------------------------------------------------------
		$str_conn	= "$params[dsn]:dbname=$params[dbname];host=$params[host]";

		try	{
			echo "str_conn=$str_conn\n";
			$dbh	= new PDO($str_conn, $params[login], $params[passwd]);
		} catch (PDOException $ex) {
			echo "ERREUR: ".$ex->getMessage()."\n";
			exit(__LINE__);
		}
		if (!$dbh) {
			echo "Pas de handler ???\n";
			exit(__LINE__);
		}
		echo "avant prepare\n";

		// Lancement de la commande SQL
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		try	{
			//$stmt	= $dbh->prepare("show tables");
			$stmt	= $dbh->prepare($params[cmd]);
		} catch (PDOException $ex) {
			echo "ERREUR: ".$ex->getMessage()."\n";
			exit(__LINE__);
		}
		echo "avant execute\n";

		if ($stmt) {
			try	{
				$stmt->execute();
			} catch (PDOException $ex) {
				echo "ERREUR: ".$ex->getMessage()."\n";
				exit(__LINE__);
			}

			echo "apres execute\n";
			//var_dump($stmt->fetchAll(PDO::FETCH_ASSOC));
			//echo "stmt="; print_r($stmt->fetchAll(PDO::FETCH_ASSOC));
			while ($row =  $stmt->fetch(PDO::FETCH_ASSOC)) {
				echo "  stmt=".json_encode($row)."\n";
			}
			echo "OK\n";

		} else {
			echo "stmt est vide ??\n";
			exit(__LINE__);
		}

	#------------------------------------------------------------------------------------------------------------
	} elseif ($mode == "testOdbc") {
	#------------------------------------------------------------------------------------------------------------
		$str_conn	= "Driver=$params[dsn];Server=$params[host];Database=$params[dbname];";

		echo "str_conn=$str_conn\n";
		$ODBC	= odbc_connect($str_conn, $params[login], $params[passwd]);
		if ($ODBC === FALSE) {
			echo "ERREUR odbc_connect: ".odbc_errormsg()."\n";
			exit(__LINE__);
		}

		#echo "avant exec\n";

		// Lancement de la commande SQL
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		$result	= odbc_exec($ODBC, $params[cmd]);
		if ($result === FALSE) {
			echo "ERREUR odbc_exec: ".odbc_errormsg()."\n";
			exit(__LINE__);
		}
		#echo "apres exec\n";

		echo "nb lignes resultat : ".odbc_num_rows($result)."\n";

		while (($row=odbc_fetch_array($result))) {
			echo "row=".json_encode($row, JSON_UNESCAPED_UNICODE)."\n";	
		}

		echo "OK\n";

	}else {
		usage();
	}

	exit(0);


# ================================
# Local variables:
# tab-width: 5
# End:
# vim: set ts=5 sw=5:
?>
