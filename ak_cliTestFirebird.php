#!/usr/bin/php -q
<?php
## IrigaNetworks
## ~~~~~~~~~~~~~
##
##

## ----------------------------------------------------------------------------------------------------
## Fonction usage()
## ----------------------------------------------------------------------------------------------------
function	usage()
{
	global $Script;

	echo "$Script
COMMANDES :
	--testPdo		: Test de l'acces PDO
	--testOdbc	: Test de l'acces ODBC
	--showBdd   	: Affichage de la description de la BDD

PARAMETRES :
	[--dsn=<string>]	: mysql ou firebird pour PDO, Nom du driver pour ODBC
	[--dbname=<string>]	: nom de la BDD
	[--host=<string>]	: localhost ou adresse IP du serveur de BDD
	[--cmd=<string>]	: commande SQL à lancer
	[--login=<string>]	:
	[--passwd=<string>]	:
	[--format=<string>]	: TXT par defaut, CSV

	\n";
	exit(1);
}

	## ----------------------------------------------------------------------------------------------------
	## Fonction main()
	## ----------------------------------------------------------------------------------------------------

	## ----------------------------------------------------------------------------------------------------
	## Décodage des paramètres d'appel
	## ----------------------------------------------------------------------------------------------------
	$Script		= basename($_SERVER['argv'][0]);

	$params		= array();
	$paramsTxt	= "";
	for ($i=1; $i<$_SERVER['argc']; $i++) {
		$paramsTxt	.= " ".$_SERVER['argv'][$i];

		if (mb_ereg("^--(testPdo)$", $_SERVER['argv'][$i], $regs))			{ $mode			= $regs[1]; continue; }
		if (mb_ereg("^--(testOdbc)$", $_SERVER['argv'][$i], $regs))			{ $mode			= $regs[1]; continue; }
		if (mb_ereg("^--(showBdd)$", $_SERVER['argv'][$i], $regs))			{ $mode			= $regs[1]; continue; }

		if (mb_ereg("^--dsn=(.*)$", $_SERVER['argv'][$i], $regs))				{ $params['dsn']	= rtrim($regs[1]); continue; }
		if (mb_ereg("^--dbname=(.*)$", $_SERVER['argv'][$i], $regs))			{ $params['dbname']	= rtrim($regs[1]); continue; }
		if (mb_ereg("^--host=(.*)$", $_SERVER['argv'][$i], $regs))			{ $params['host']	= rtrim($regs[1]); continue; }
		if (mb_ereg("^--cmd=(.*)$", $_SERVER['argv'][$i], $regs))				{ $params['cmd']	= rtrim($regs[1]); continue; }
		if (mb_ereg("^--login=(.*)$", $_SERVER['argv'][$i], $regs))			{ $params['login']	= rtrim($regs[1]); continue; }
		if (mb_ereg("^--passwd=(.*)$", $_SERVER['argv'][$i], $regs))			{ $params['passwd']	= rtrim($regs[1]); continue; }
		if (mb_ereg("^--format=(.*)$", $_SERVER['argv'][$i], $regs))			{ $params['format']	= rtrim($regs[1]); continue; }

		echo "Paramètre inconnu :".$_SERVER['argv'][$i]."\n";
		usage();
	}

	if (!$mode) {
		usage();
	}

	if (!$params['dsn'] || !$params['dbname'] || !$params['host'] || !$params['login'] || !$params['passwd']) {
		echo "les parametres --dsn, --dbname, --host, --login et --passwd sont obligatoires\n";
		usage();
	}

	#------------------------------------------------------------------------------------------------------------
	if ($mode == "testPdo") {
	#------------------------------------------------------------------------------------------------------------
		if (!$params['cmd']) {
			echo "--cmd est obligatoire\n";
			usage();
		}
		$str_conn	= "$params[dsn]:dbname=$params[dbname];host=$params[host]";

		try	{
			echo "## str_conn=$str_conn\n";
			$dbh	= new PDO($str_conn, $params['login'], $params['passwd']);
			echo "class dbh=".get_class($dbh)."\n";

			## Définition du mode de gestion des erreurs
			## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		} catch (PDOException $ex) {
			echo "## new PDO : ERREUR: ".$ex->getMessage()."\n";
			exit(__LINE__);
		}
		if (!$dbh) {
			echo "## Pas de handler ???\n";
			exit(__LINE__);
		}

		echo "## avant prepare\n";

		// Lancement de la commande SQL
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		try	{
			$cmd = $params['cmd'];
			mb_convert_variables('ISO-8859-1', 'UTF-8', $cmd);
			$stmt	= $dbh->prepare($cmd);
		} catch (PDOException $ex) {
			echo "## prepare : ERREUR: ".$ex->getMessage()."\n";
			exit(__LINE__);
		}
		echo "\n## PDOStatement="; print_r($stmt);
		echo "\n## avant execute\n";

		if ($stmt) {
			try	{
				$stmt->execute();
			} catch (PDOException $ex) {
				echo "## execute : ERREUR: ".$ex->getMessage()."\n";
				exit(__LINE__);
			}

			echo "## apres execute\n";
			$indLigne	= 0;
			if ($params['format'] == "CSV") {
				$filename	= "/tmp/testFirebird_".date("Ymd_His").".csv";
				$out		= fopen($filename, 'w');
			}
			while ($row =  $stmt->fetch(PDO::FETCH_ASSOC)) {
				if ($params['format'] == "CSV") {
					if (!$indLigne) {
						## insertion de l'entete des colonnes
						## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						fputcsv($out, array_keys($row), ";");
					}
					## Insertion de la ligne standard
					## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					fputcsv($out, $row, ";");
				} else {
					mb_convert_variables('UTF-8', 'ISO-8859-1', $row);
					echo "  stmt=".json_encode($row)."\n";
				}
				$indLigne ++;
			}
			if ($params['format'] == "CSV") {
				fclose($out);
				echo "## OK : $filename\n";
			} else {
				echo "## OK\n";
			}

		} else {
			echo "## stmt est vide ??\n";
			exit(__LINE__);
		}

	#------------------------------------------------------------------------------------------------------------
	} elseif ($mode == "testOdbc") {
	#------------------------------------------------------------------------------------------------------------
		if (!$params['cmd']) {
			echo "--cmd est obligatoire\n";
			usage();
		}
		$str_conn	= "Driver=$params[dsn];Server=$params[host];Database=$params[dbname];";

		echo "## str_conn=$str_conn\n";
		$ODBC	= odbc_connect($str_conn, $params['login'], $params['passwd']);
		if ($ODBC === FALSE) {
			echo "## ERREUR odbc_connect: ".odbc_errormsg()."\n";
			exit(__LINE__);
		}

		#echo "avant exec\n";

		// Lancement de la commande SQL
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		$result	= odbc_exec($ODBC, $params['cmd']);
		if ($result === FALSE) {
			echo "## ERREUR odbc_exec: ".odbc_errormsg()."\n";
			exit(__LINE__);
		}
		#echo "apres exec\n";

		echo "## nb lignes resultat : ".odbc_num_rows($result)."\n";

		$indLigne	= 0;
		if ($params['format'] == "CSV") {
			$filename	= "/tmp/testFirebird_".date("Ymd_His").".csv";
			$out		= fopen($filename, 'w');
		}
		while (($row=odbc_fetch_array($result))) {
			if ($params['format'] == "CSV") {
				if (!$indLigne) {
					## insertion de l'entete des colonnes
					## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					fputcsv($out, array_keys($row), ";");
				}
				## Insertion de la ligne standard
				## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
				fputcsv($out, $row, ";");
			} else {
				echo "row=".json_encode($row, JSON_UNESCAPED_UNICODE)."\n";	
			}
			$indLigne ++;
		}

		if ($params['format'] == "CSV") {
			fclose($out);
			echo "## OK : $filename\n";
		} else {
			echo "## OK\n";
		}

	#------------------------------------------------------------------------------------------------------------
	} elseif ($mode == "showBdd") {
	#------------------------------------------------------------------------------------------------------------
		## Affichage des paramètres généraux de la BDD
		## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		$cmd	= sprintf("echo \"show database; exit;\" | /usr/bin/isql-fb -z -user '%s' -pass '%s' '%s'", $params['login'], $params['passwd'], $params['dbname']);
		echo "===================================================================================================================\n";
		echo "Paramètres de la BDD\n";
		echo "===================================================================================================================\n";
		system($cmd);

		## Affichage de la liste des tables
		## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		$cmd	= sprintf("echo \"show tables; exit;\" | /usr/bin/isql-fb -user '%s' -pass '%s' '%s'", $params['login'], $params['passwd'], $params['dbname']);
		echo "===================================================================================================================\n";
		echo "Liste des tables\n";
		echo "===================================================================================================================\n";
		$output	= `$cmd`;
		echo "$output\n";

		## Construire la liste des tables
		## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		$tables	= array();
		foreach (explode("\n", $output) as $line) {
			if (preg_match('/\s+(\w+)\s+(\w+)\s+$/', $line, $match)) {
				$tables[]	= $match[1];
				$tables[]	= $match[2];
			} elseif (preg_match('/\s+(\w+)\s+$/', $line, $match)) {
				$tables[]	= $match[1];
			}
		}
		
		## Parcourir la description de toutes les tables
		## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		foreach ($tables as $table) {
			$cmd	= sprintf("echo \"show table %s; exit;\" | /usr/bin/isql-fb -user '%s' -pass '%s' '%s'", $table, $params['login'], $params['passwd'], $params['dbname']);
			echo "===================================================================================================================\n";
			echo "Table : $table\n";
			echo "===================================================================================================================\n";
			system($cmd);
		}
	}else {
		usage();
	}

	exit(0);


# ================================
# Local variables:
# tab-width: 5
# End:
# vim: set ts=5 sw=5:
?>
