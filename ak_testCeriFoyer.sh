#!/bin/bash
./ak_cliTestFirebird.php --testPdo --dsn=firebird --dbname=81.252.7.211:W:\\AppDelphi\\CFS\\Donnees\\Data\\WeasyLoc\\42CJPB_H.fdb --host=81.252.7.211 --cmd="select * from VERSION" --login=SYSDBA --passwd=masterkey
./ak_cliTestFirebird.php --testPdo --dsn=firebird --dbname=81.252.7.211:W:\\AppDelphi\\CFS\\Donnees\\Data\\WeasyLoc\\42CJPB_H.fdb --host=81.252.7.211 --cmd="select * from ETABL" --login=SYSDBA --passwd=masterkey
./ak_cliTestFirebird.php --testPdo --dsn=firebird --dbname=81.252.7.211:W:\\AppDelphi\\CFS\\Donnees\\Data\\WeasyLoc\\42CJPB_H.fdb --host=81.252.7.211 --cmd="Select V.CDCLIENT, V.NBLITS, E.NO_ETABL, E.NOM, E.CP, E.VILLE, E.NO_TYPE, E.TEL,E. E_MAIL From ETABL E, VERSION V" --login=SYSDBA --passwd=masterkey


./ak_cliTestFirebird.php --testPdo --dsn=firebird --dbname=81.252.7.211:W:\\AppDelphi\\CFS\\Donnees\\Data\\WeasyLoc\\92MARMARIS_H.FDB --host=81.252.7.211 --cmd="Select V.CDCLIENT, V.NBLITS, E.NO_ETABL, E.NOM, E.CP, E.VILLE, E.NO_TYPE, E.TEL,E. E_MAIL From ETABL E, VERSION V" --login=SYSDBA --passwd=masterkey


echo "select * from VERSION; exit;" | isql-fb -user SYSDBA -pass masterkey 81.252.7.211:W:\\AppDelphi\\CFS\\Donnees\\Data\\WeasyLoc\\42CJPB_H.fdb

## Liste des tables
echo "show tables; exit;" | isql-fb -user SYSDBA -pass masterkey 81.252.7.211:W:\\AppDelphi\\CFS\\Donnees\\Data\\WeasyLoc\\42CJPB_H.fdb

## Detail des champs d'une table
echo "show table ADRESSES; exit;" | isql-fb -user SYSDBA -pass masterkey 81.252.7.211:W:\\AppDelphi\\CFS\\Donnees\\Data\\WeasyLoc\\42CJPB_H.fdb

## Liste des factures et réglements
./ak_cliTestFirebird.php --testPdo --dsn=firebird --dbname=81.252.7.211:W:\\AppDelphi\\CFS\\Donnees\\Data\\WeasyLoc\\42CJPB_H.fdb --host=81.252.7.211 --login=SYSDBA --passwd=masterkey --cmd="SELECT FACTURES.*, LIGNES_F.*, PRESTATIONS.*, TYPES_PRESTA.LIBELLE AS TYPES_PRESTA_LIBELLE, PRESTA_RESIDENTS.* FROM LIGNES_F LEFT JOIN FACTURES ON FACTURES.NO_FACTURE=LIGNES_F.NO_FACTURE LEFT JOIN PRESTATIONS ON LIGNES_F.NO_PRESTATION=PRESTATIONS.NO_PRESTATION LEFT JOIN TYPES_PRESTA ON TYPES_PRESTA.NO_TYPE_PRESTA=PRESTATIONS.NO_TYPE_PRESTA  LEFT JOIN PRESTA_RESIDENTS ON PRESTA_RESIDENTS.NO_PRESTA_RESIDENTS=LIGNES_F.NO_PRESTA_RESIDENTS WHERE FACTURES.NO_RESIDENT=2213 AND FACTURES.NO_FACT=24676"

## Liste des reservations du PMS
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
./ak_cliTestFirebird.php --testPdo --dsn=firebird --dbname=81.252.7.211:W:\\AppDelphi\\CFS\\Donnees\\Data\\WeasyLoc\\42CJPB_H.fdb --host=81.252.7.211 --login=SYSDBA --passwd=masterkey --cmd="SELECT SEJOURS.NO_SEJOUR as NO_SEJOUR, SEJOURS.NO_RESIDENT as NO_RESIDENT, MVT_SEJOURS.NO_MVT_SEJOUR as NO_MVT_SEJOUR, MVT_SEJOURS.NO_LOGT as NO_LOGT, MVT_SEJOURS.NO_LOGEMENT as NO_LOGEMENT, ETABL.NO_ETABL as NO_ETABL, RESIDENTS.NOM as RESIDENT_NOM, RESIDENTS.PRENOM as RESIDENT_PRENOM, SEJOURS.DATE_DEB as SEJOUR_DATE_DEB, SEJOURS.DATE_FIN as SEJOUR_DATE_FIN, MVT_SEJOURS.DATE_DEB as MVT_DATE_DEB, MVT_SEJOURS.DATE_FIN as MVT_DATE_FIN, ETABL.NOM as ETABL_NOM, SEJOURS.CONFIRME as CONFIRME, SEJOURS.EN_COURS as EN_COURS, SEJOURS.CLOTUREE as CLOTUREE, RESIDENTS.RES_PRESENT as RES_PRESENT \
FROM SEJOURS \
LEFT JOIN RESIDENTS ON SEJOURS.NO_RESIDENT = RESIDENTS.NO_RESIDENT \
LEFT JOIN MVT_SEJOURS ON SEJOURS.NO_SEJOUR = MVT_SEJOURS.NO_SEJOUR \
LEFT JOIN ETABL ON ETABL.NO_ETABL = MVT_SEJOURS.NO_ETABL \
LEFT JOIN LOGEMENTS ON LOGEMENTS.NO_LOGT = MVT_SEJOURS.NO_LOGT \
ORDER BY SEJOURS.NO_RESIDENT, SEJOURS.NO_SEJOUR\
"

## Accès BDD FoyerCiteDesFleurs
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/home/aak/outilsaccescerifoyeretnegoce/ak_cliTestFirebird.php --testPdo --dsn=firebird --dbname=81.252.7.211:W:\\AppDelphi\\CFS\\Donnees\\Data\\WeasyLoc\\75LACITEDESFLEURS_H.fdb --host=81.252.7.211 --login=SYSDBA --passwd=masterkey --cmd="SELECT * FROM VERSION"

## Accès BDD Philanthropique
## ~~~~~~~~~~~~~~~~~~~~~~~~~
/home/aak/outilsaccescerifoyeretnegoce/ak_cliTestFirebird.php --testPdo --dsn=firebird --dbname=81.252.7.211:W:\\AppDelphi\\CFS\\Donnees\\Data\\WeasyLoc\\75SOCPHILLVS_H.fdb --host=81.252.7.211 --login=SYSDBA --passwd=masterkey --cmd="SELECT * FROM VERSION"
/home/aak/outilsaccescerifoyeretnegoce/ak_cliTestFirebird.php --testPdo --dsn=firebird --dbname=81.252.7.211:W:\\AppDelphi\\CFS\\Donnees\\Data\\WeasyLoc\\75SOCPHILFEJA_H.fdb --host=81.252.7.211 --login=SYSDBA --passwd=masterkey --cmd="SELECT * FROM VERSION"

## Accès BDD ACAJ-CAEN
## ~~~~~~~~~~~~~~~~~~~
/home/aak/outilsaccescerifoyeretnegoce/ak_cliTestFirebird.php --testPdo --dsn=firebird --dbname=81.252.7.211:W:\\AppDelphi\\CFS\\Donnees\\#FoyerSelf\\14xCaen.RobertReme\\Data\\14CAEN_AUBERGE_H.FDB --host=81.252.7.211 --login=SYSDBA --passwd=masterkey --cmd="SELECT * FROM VERSION"
echo "select * from VERSION; exit;" | isql-fb -user SYSDBA -pass masterkey 81.252.7.211:W:\\AppDelphi\\CFS\\Donnees\\#FoyerSelf\\14xCaen.RobertReme\\Data\\14CAEN_AUBERGE_H.FDB

## Accès BDD NotreDame
## ~~~~~~~~~~~~~~~~~~~
echo "select * from VERSION; exit;" | isql-fb -user SYSDBA -pass masterkey 81.252.7.211:W:\\Clients\\FoyerSelf\\67x_Strasbourg.FoyerNotreDame\\Support\\BD_83103\\Data\\67TOMIUNGERER_H.FDB

## Exemple de UPDATE de BDD
## ~~~~~~~~~~~~~~~~~~~~~~~~
echo "select REF_LIGNE, NO_FACTURE, NO_LIGNE, TOTAL_TTC, DATE_DEB from LIGNES_F WHERE REF_LIGNE='3805'; exit;" | isql-fb -user SYSDBA -pass masterkey 81.252.7.211:W:\\Clients\\FoyerSelf\\75x_Paris.SocietePhilanthropique\\Support\\BD_84401_LVS\\Data\\75SOCPHILLVS_H.FDB
echo "UPDATE LIGNES_F SET DATE_DEB='2019-02-12' WHERE REF_LIGNE='3805'; exit;" | isql-fb -user SYSDBA -pass masterkey 81.252.7.211:W:\\Clients\\FoyerSelf\\75x_Paris.SocietePhilanthropique\\Support\\BD_84401_LVS\\Data\\75SOCPHILLVS_H.FDB
echo "UPDATE LIGNES_F SET NO_PRESTATION=66 WHERE REF_LIGNE='2798'; exit;" | isql-fb -user SYSDBA -pass masterkey 81.252.7.211:W:\\Clients\\FoyerSelf\\75x_Paris.SocietePhilanthropique\\Support\\BD_84401_LVS\\Data\\75SOCPHILLVS_H.FDB
echo "UPDATE LIGNES_F SET NO_PRESTATION=66 WHERE REF_LIGNE='4696'; exit;" | isql-fb -user SYSDBA -pass masterkey 81.252.7.211:W:\\Clients\\FoyerSelf\\75x_Paris.SocietePhilanthropique\\Support\\BD_84401_LVS\\Data\\75SOCPHILLVS_H.FDB
echo "UPDATE LIGNES_F SET NO_PRESTATION=66 WHERE REF_LIGNE='6055'; exit;" | isql-fb -user SYSDBA -pass masterkey 81.252.7.211:W:\\Clients\\FoyerSelf\\75x_Paris.SocietePhilanthropique\\Support\\BD_84401_LVS\\Data\\75SOCPHILLVS_H.FDB
echo "UPDATE FACTURES SET NO_ETABL='7' WHERE NO_FACT='8548'; exit;" | isql-fb -user SYSDBA -pass masterkey 81.252.7.211:W:\\Clients\\FoyerSelf\\75x_Paris.SocietePhilanthropique\\Support\\BD_84401_LVS\\Data\\75SOCPHILLVS_H.FDB
echo "UPDATE FACTURES SET NO_ETABL='10' WHERE NO_FACT='1161'; exit;" | isql-fb -user SYSDBA -pass masterkey 81.252.7.211:W:\\Clients\\FoyerSelf\\75x_Paris.SocietePhilanthropique\\Support\\BD_84401_LVS\\Data\\75SOCPHILLVS_H.FDB
echo "UPDATE FACTURES SET NO_ETABL='10' WHERE NO_FACT='1792'; exit;" | isql-fb -user SYSDBA -pass masterkey 81.252.7.211:W:\\Clients\\FoyerSelf\\75x_Paris.SocietePhilanthropique\\Support\\BD_84401_LVS\\Data\\75SOCPHILLVS_H.FDB
echo "UPDATE FACTURES SET NO_ETABL='10' WHERE NO_FACT='2228'; exit;" | isql-fb -user SYSDBA -pass masterkey 81.252.7.211:W:\\Clients\\FoyerSelf\\75x_Paris.SocietePhilanthropique\\Support\\BD_84401_LVS\\Data\\75SOCPHILLVS_H.FDB

echo "SELECT NO_REGLT, NO_RESIDENT, NO_BANQUE FROM REGLT WHERE NO_REGLT='15071'; exit;" | isql-fb -user SYSDBA -pass masterkey 197.197.197.9:E:\\AppCERI\\CERI-Foyer\\Donnees\\Data\\59_AEU_H.FDB
echo "UPDATE REGLT SET NO_BANQUE='2' WHERE NO_REGLT='15071'; exit;" | isql-fb -user SYSDBA -pass masterkey 197.197.197.9:E:\\AppCERI\\CERI-Foyer\\Donnees\\Data\\59_AEU_H.FDB
