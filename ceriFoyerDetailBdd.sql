===================================================================================================================
Paramètres de la BDD
===================================================================================================================
ISQL Version: LI-V2.5.7.27050 Firebird 2.5
Server version:
WI-V2.5.2.26539 Firebird 2.5
WI-V2.5.2.26539 Firebird 2.5/tcp (SRVWS2012)/P12
LI-V2.5.7.27050 Firebird 2.5/tcp (vps265095.ovh.net)/P12
Database: 81.252.7.211:W:\AppDelphi\CFS\Donnees\Data\WeasyLoc\42CJPB_H.fdb
        Owner: SYSDBA                         
PAGE_SIZE 4096
Number of DB pages allocated = 15050
Sweep interval = 20000
Forced Writes are ON
Transaction - oldest = 35275
Transaction - oldest active = 35276
Transaction - oldest snapshot = 35276
Transaction - Next = 35279
ODS = 11.2
Default Character set: NONE
===================================================================================================================
Liste des tables
===================================================================================================================
       ACADEMIE                               ACTIONS                        
       ACTIVITES_RES                          ACTIVITE_PROF                  
       ADRESSES                               AIDES                          
       AIDES_RESIDENTS                        ALT_CLASSE                     
       ALT_PLANNINGS                          ALT_PLANNING_PRESTA            
       ALT_PLANNING_PRESTA_RES                ALT_SEJOURS                    
       ANALYT_CO                              ARCHIVE                        
       ARFO                                   BANQUES                        
       BAREMES_AIDES                          BAREMES_PRESTA                 
       BATIMENTS                              BQ_EMETTRICE                   
       CAF                                    CATEGORIES_RES                 
       CAUTIONS                               CH_LOGT_FJT                    
       COMMENTAIRES                           COMPTES_CAISSES                
       COURRIERS                              CPTA_PRESTA                    
       CPTA_REGLT                             CSP                            
       DEPARTS                                DETAIL_ETATS                   
       DETAIL_USER                            DIPLOMES                       
       DIPLOME_RES                            DOC_RESIDENTS                  
       DOC_TYPES_RES                          DV_CONTRAT_AIDES               
       ECART_CONVERSION                       EMPLOYEUR                      
       ENTOURAGE_RES                          ENVOI_COURRIERS                
       ETABL                                  ETABL_PARAM                    
       ETATS_LIEUX                            EVENEMENTS                     
       FACTURES                               FAMILLE_PRESTA                 
       FAMILLE_PRESTA_TMP                     FAMILLE_RES                    
       FCMB_CAMPAGNE                          FCMB_COMPETENCESPRO            
       FCMB_CONVENTION_COLL                   FCMB_DIPLOME                   
       FCMB_ETAPETDF                          FCMB_ETATCOMPAGNON             
       FCMB_ETB                               FCMB_METIER                    
       FCMB_OBJ_COMPAGNON                     FCMB_OBJ_FORMATION             
       FCMB_RECRUT_INITIAL                    FCMB_SOCIETE                   
       FCMB_VILLE                             FORMATIONS                     
       HAJPUBLICLOGES                         HISTO_SAISIE                   
       INDISPO                                INDUS                          
       INFOS                                  INTERVENANTS                   
       INTERVENTIONS                          INTERV_MAT                     
       INTERV_TIERS                           INVENTAIRE                     
       ITI_CONTRAT                            ITI_DISCIPLINE                 
       ITI_LANGUE                             ITI_LOCOMOTION                 
       ITI_NIVEAU                             ITI_TITRE                      
       LIGNES_F                               LISTE_ETATS                    
       LIST_ACTIVITEPRINCIPALE                LIST_AGREMENTCASF              
       LIST_AGREMENTCCH                       LIST_AIDESETGARANTIEMOBILISEES 
       LIST_AUTRESACTIVITES                   LIST_AUTRESAGREMENTSCONVENTIONS
       LIST_CLASSEAGE                         LIST_CLASSEDUREESEJOUR         
       LIST_CLASSERESSOURCES                  LIST_COLLECTIFVSDIFFUS         
       LIST_COMMENTAVEZVOUSEUIDEESDE          LIST_CONTEXTEACCOMPAGNEMENT    
       LIST_CONTINGENTSPECIFIQUE              LIST_CONVENTIONSLIEESAULOGT    
       LIST_DEPT                              LIST_FINANCEPAR                
       LIST_GARANTIEMISEENOEUVRE              LIST_ISSUESETSUITESSEJOUACC    
       LIST_LOCALISATIONLOGTAVSEJOUR          LIST_MOTIVATIONNONATTRIBUTION  
       LIST_NIVEAUSCOLAIRE                    LIST_ORIENTATIONPROPOSE        
       LIST_PRECISIONSURORIENTATION           LIST_PRINCIPALERAISONDEPARTLOGT
       LIST_QUELLESONTACTUELACTIVITES         LIST_RAISONCHOIXSOLUTIONHAJ    
       LIST_RAISONRECHERCHELOGT               LIST_REGION                    
       LIST_SEJOURCARACTERISTIQUE             LIST_SEJOURTYPE                
       LIST_SOLUTIONHBGTTROUVEE               LIST_STATUTDEMANDE             
       LIST_STATUTGESTIONNAIRE                LIST_TYPELOGEMENT              
       LIST_TYPELOGTDEBUTSEJOUR               LIST_TYPELOGTFINSEJOUR         
       LIST_TYPEPROPRIETAIRE                  LIST_TYPERDV                   
       LOGEMENTS                              MOTIFS_DEP                     
       MOTIVATIONS                            MT_REVENUS                     
       MVT_CAISSE                             MVT_CPTES                      
       MVT_SEJOURS                            NATIONALITES                   
       NATURE_PRESTA                          NE_A                           
       ORDINATEURS                            ORG_GEO                        
       PARAM_SAISIE_RES                       PERS_1L                        
       PRESTATIONS                            PRESTA_RESIDENTS               
       PRISES_CHARGES                         PROFESSION_RES                 
       PROFIL                                 PROF_PARENTS                   
       PROSPECT_STATUT                        RECUS                          
       REGIMES                                REGLT                          
       REMBOURSEMENTS                         RESIDENTS                      
       RESSOURCES                             RNI                            
       SAL_CLIENTS                            SAL_DISPOSITIONS               
       SAL_PARAMS                             SAL_PLANNING                   
       SAL_SALLES                             SAL_TRANCHES                   
       SAL_TYPES                              SAL_VF                         
       SECTEURS                               SEJOURS                        
       SITUATIONS_PROF                        SLCHL_ACTIVITEFORM             
       SLCHL_DEREXPPRO                        SLCHL_DURABILITE               
       SLCHL_EMPLOYEUR                        SLCHL_LIEU                     
       SLCHL_MOTIVSITUA                       SLCHL_MOYDPLCT                 
       SLCHL_NATUREAIDE                       SLCHL_NIVFORMATION             
       SLCHL_ORIGRESSOURCES                   SLCHL_PARCLOGT                 
       SLCHL_REVENUS                          SLCHL_SITUAFAMIL               
       SLCHL_SOLUTION                         SLCHL_SPECIFICITE              
       SLCHL_STATUT                           SLCHL_STATUTCPLT               
       SLCHL_TYPCONTACT                       SLCHL_TYPEAIDE                 
       SLCHL_TYPLOGT                          SLCHL_TYPLOGTADEM              
       SL_REF_PRESCRIPTEUR                    SL_SITUA_JEUNE                 
       SL_SS_SERVICE                          SL_SUIVI_JEUNE                 
       SOLDE_AIDES_CAF                        TAUX_TVA                       
       THEB_ANT                               TIERS                          
       TLOG_SORT                              TYPES_ABS                      
       TYPES_AIDES                            TYPES_CONTRAT                  
       TYPES_DG                               TYPES_EVENEMENTS               
       TYPES_INTERV                           TYPES_INTERVENTIONS            
       TYPES_LOGT                             TYPES_PRESTA                   
       TYPES_REG                              TYPES_RES                      
       TYPES_RESSOURCES                       TYPES_SITUATIONS               
       USERS                                  VENTE_DIRECTE                  
       VENTILE_REGLT                          VENTILE_REMB                   
       VERSION                         

===================================================================================================================
Table : ACADEMIE
===================================================================================================================
IDACADEMIE                      INTEGER Not Null 
LIBELLE                         VARCHAR(40) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_130:
  Primary key (IDACADEMIE)

Triggers on Table ACADEMIE:
SET_IDACADEMIE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ACTIONS
===================================================================================================================
IDENTIFIANT                     VARCHAR(5) Nullable 
DATE_ACTION                     TIMESTAMP Nullable 
INFOS                           VARCHAR(255) Nullable 
===================================================================================================================
Table : ACTIVITES_RES
===================================================================================================================
NO_ACTIV_RES                    INTEGER Nullable 
LIBELLE                         VARCHAR(50) Nullable 
LIB                             VARCHAR(255) Nullable 

Triggers on Table ACTIVITES_RES:
SET_NO_ACTIV_RES, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ACTIVITE_PROF
===================================================================================================================
NO_ACTIVITE_PROF                INTEGER Nullable 
LIBELLE                         VARCHAR(50) Nullable 
LIB                             VARCHAR(255) Nullable 
CD_AN                           INTEGER Nullable 
SAISIE                          VARCHAR(1) Nullable 

Triggers on Table ACTIVITE_PROF:
SET_NO_ACTIVITE_PROF, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ADRESSES
===================================================================================================================
NO_ADRESSE                      INTEGER Nullable 
IDENTIFIANT                     VARCHAR(1) Nullable 
NO_IDENTIFIANT                  INTEGER Nullable 
ADRESSE1                        VARCHAR(40) Nullable 
ADRESSE2                        VARCHAR(40) Nullable 
ADRESSE3                        VARCHAR(40) Nullable 
CP                              VARCHAR(10) Nullable 
VILLE                           VARCHAR(40) Nullable 
PAYS                            VARCHAR(40) Nullable 
TYPE_ADR                        VARCHAR(20) Nullable 

Triggers on Table ADRESSES:
SET_NO_ADRESSE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : AIDES
===================================================================================================================
NO_AIDE                         INTEGER Nullable 
DATE_AIDE                       TIMESTAMP Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
DATE_DEB                        TIMESTAMP Nullable 
DATE_FIN                        TIMESTAMP Nullable 
PERPETUEL                       VARCHAR(1) Nullable 
NO_RESIDENT                     INTEGER Nullable 
NO_TIERS                        INTEGER Nullable 
NO_TIERS_V                      INTEGER Nullable 
MOIS_COMPLET                    VARCHAR(1) Nullable 
VALIDE_AP                       VARCHAR(1) Nullable 
FACTURE                         VARCHAR(1) Nullable 
DEJA_FACTURE                    DOUBLE PRECISION Nullable 
VERSE                           VARCHAR(1) Nullable 
PAR_BAREME                      VARCHAR(1) Nullable 
PAR_JOUR                        VARCHAR(1) Nullable 
VERSE_LE                        TIMESTAMP Nullable 
PREVISIONNELLE                  VARCHAR(1) Nullable 
LAST_FACT                       TIMESTAMP Nullable 
LIBELLE                         VARCHAR(80) Nullable 
MONNAIE                         VARCHAR(1) Nullable 
COMPTA_LE                       TIMESTAMP Nullable 
INDUS                           DOUBLE PRECISION Nullable 
REMBOURSE                       DOUBLE PRECISION Nullable 
MODIFIABLE                      VARCHAR(1) Nullable 
PARAM_CPTA                      VARCHAR(1) Nullable 
JL                              VARCHAR(10) Nullable 
COMPTE                          VARCHAR(15) Nullable 
COMPTE_ANA                      VARCHAR(15) Nullable 
COMPTE_TVA                      VARCHAR(15) Nullable 
T_TYPE                          DOUBLE PRECISION Nullable 
MT_DIFF                         DOUBLE PRECISION Nullable 

Triggers on Table AIDES:
SET_NO_AIDE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : AIDES_RESIDENTS
===================================================================================================================
NO_AIDE_RESIDENT                INTEGER Nullable 
NO_AIDE                         INTEGER Nullable 
NO_RESIDENT                     INTEGER Nullable 
DATE_AIDE_RESIDENT              TIMESTAMP Nullable 
MONTANT                         DOUBLE PRECISION Nullable 

Triggers on Table AIDES_RESIDENTS:
SET_NO_AIDE_RESIDENT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ALT_CLASSE
===================================================================================================================
NO_RESIDENT                     INTEGER Not Null 
ID_PLANNING                     INTEGER Not Null 
===================================================================================================================
Table : ALT_PLANNINGS
===================================================================================================================
ID_PLANNING                     INTEGER Not Null 
LIBELLE                         VARCHAR(50) Not Null 
DEBUT                           TIMESTAMP Nullable 
FIN                             TIMESTAMP Nullable 
COULEUR                         VARCHAR(10) Nullable 
ACTIF                           VARCHAR(1) Nullable 
NO_FAMILLE                      INTEGER Nullable 
CONSTRAINT INTEG_7:
  Primary key (ID_PLANNING)

Triggers on Table ALT_PLANNINGS:
BI_ALT_PLANNINGS_ID_PLANNING, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ALT_PLANNING_PRESTA
===================================================================================================================
ID_PLANNING                     INTEGER Not Null 
NO_PRESTATION                   INTEGER Not Null 
ID_PP                           INTEGER Not Null 
MONTANT                         DOUBLE PRECISION Nullable 
FREQUENCE                       INTEGER Nullable 
TAUX_TVA                        INTEGER Nullable 
REGULIER                        VARCHAR(1) Nullable 
QTE                             DOUBLE PRECISION Nullable 
CONSTRAINT INTEG_20:
  Primary key (ID_PP)

Triggers on Table ALT_PLANNING_PRESTA:
BI_ALT_PLANNING_PRESTA_ID_PP, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ALT_PLANNING_PRESTA_RES
===================================================================================================================
ID_PLANNING                     INTEGER Not Null 
NO_PRESTATION                   INTEGER Not Null 
NO_RESIDENT                     INTEGER Not Null 
MONTANT                         DOUBLE PRECISION Nullable 
FREQUENCE                       SMALLINT Nullable 
TAUX_TVA                        INTEGER Nullable 
REGULIER                        VARCHAR(1) Nullable 
QTE                             DOUBLE PRECISION Nullable 
ID_PPR                          INTEGER Not Null 
CONSTRAINT FK_ALT_PLANNING_PRESTA_RES1:
  Foreign key (ID_PLANNING)    References ALT_PLANNINGS (ID_PLANNING) On Update Cascade On Delete Cascade
CONSTRAINT FK_ALT_PLANNING_PRESTA_RES2:
  Foreign key (NO_PRESTATION)    References PRESTATIONS (NO_PRESTATION) On Update Cascade On Delete Cascade
CONSTRAINT FK_ALT_PLANNING_PRESTA_RES3:
  Foreign key (NO_RESIDENT)    References RESIDENTS (NO_RESIDENT) On Update Cascade On Delete Cascade
CONSTRAINT INTEG_25:
  Primary key (ID_PPR)

Triggers on Table ALT_PLANNING_PRESTA_RES:
BI_ALT_PLANNING_PRESTA_RES, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ALT_SEJOURS
===================================================================================================================
ID_SEJOURS                      INTEGER Not Null 
ENTREE                          TIMESTAMP Not Null 
SORTIE                          TIMESTAMP Not Null 
ID_PLANNING                     INTEGER Not Null 
CONSTRAINT INTEG_12:
  Primary key (ID_SEJOURS)

Triggers on Table ALT_SEJOURS:
BI_ALT_SEJOURS_ID_SEJOURS, Sequence: 0, Type: BEFORE INSERT, Active
ALT_SEJOURS_AD, Sequence: 0, Type: AFTER DELETE, Active
ALT_SEJOURS_AI_AU, Sequence: 0, Type: AFTER INSERT OR UPDATE, Active
===================================================================================================================
Table : ANALYT_CO
===================================================================================================================
NO_ANALYT_CO                    INTEGER Nullable 
POURCENT                        DOUBLE PRECISION Nullable 
CODE_ANA                        VARCHAR(17) Nullable 
COMPTE_ANA                      VARCHAR(17) Nullable 

Triggers on Table ANALYT_CO:
SET_NO_ANALYT_CO, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ARCHIVE
===================================================================================================================
NO_ARCH                         INTEGER Nullable 
DT_ARCH                         TIMESTAMP Nullable 
NO_RES                          INTEGER Nullable 
NOM                             VARCHAR(40) Nullable 
PRENOM                          VARCHAR(40) Nullable 
NE_LE                           TIMESTAMP Nullable 
NO_SEJOUR                       INTEGER Nullable 
DT_DEB                          TIMESTAMP Nullable 
DT_FIN                          TIMESTAMP Nullable 
FLAG_ARCH                       VARCHAR(1) Nullable 

Triggers on Table ARCHIVE:
SET_NO_ARCH, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ARFO
===================================================================================================================
NO_ARFO                         INTEGER Nullable 
NO_RESIDENT                     INTEGER Nullable 
NO_ETABL                        INTEGER Nullable 
F1BIS                           VARCHAR(1) Nullable 
F2_SEUL                         VARCHAR(1) Nullable 
F2_COUPLE                       VARCHAR(2) Nullable 

Triggers on Table ARFO:
SET_NO_ARFO, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : BANQUES
===================================================================================================================
NO_BANQUE                       INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
GUICHET                         VARCHAR(5) Nullable 
LIB_ETABL                       VARCHAR(5) Nullable 
REF_COMPTE                      VARCHAR(11) Nullable 
CLE_RICE                        VARCHAR(2) Nullable 
PAR_DEFAUT                      VARCHAR(1) Nullable 
NOM_RESP                        VARCHAR(40) Nullable 
VIRT_NOM                        VARCHAR(24) Nullable 
IBAN                            VARCHAR(34) Nullable 
BIC                             VARCHAR(11) Nullable 
NO_ETABL                        INTEGER Nullable 

Triggers on Table BANQUES:
SET_NO_BANQUE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : BAREMES_AIDES
===================================================================================================================
NO_BAREMES_AIDES                INTEGER Nullable 
REFERENCE                       VARCHAR(1) Nullable 
DU                              TIMESTAMP Nullable 
AU                              TIMESTAMP Nullable 
DE                              DOUBLE PRECISION Nullable 
A                               DOUBLE PRECISION Nullable 
VALEUR                          DOUBLE PRECISION Nullable 
NO_TYPE_AIDE                    INTEGER Nullable 
T_TYPE                          VARCHAR(1) Nullable 

Triggers on Table BAREMES_AIDES:
SET_NO_BAREMES_AIDES, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : BAREMES_PRESTA
===================================================================================================================
NO_BAREMES_PRESTA               INTEGER Nullable 
REFERENCE                       DOUBLE PRECISION Nullable 
DE                              DOUBLE PRECISION Nullable 
A                               DOUBLE PRECISION Nullable 
DU                              TIMESTAMP Nullable 
AU                              TIMESTAMP Nullable 
PAR_FORMULE                     VARCHAR(1) Nullable 
FORMULE                         BLOB segment 80, subtype TEXT Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
FREQUENCE                       INTEGER Nullable 
T_TYPE                          DOUBLE PRECISION Nullable 
NO_PRESTATION                   INTEGER Nullable 

Triggers on Table BAREMES_PRESTA:
SET_NO_BAREMES_PRESTA, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : BATIMENTS
===================================================================================================================
NO_BATIMENT                     INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 

Triggers on Table BATIMENTS:
SET_NO_BATIMENT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : BQ_EMETTRICE
===================================================================================================================
NO_BQE                          INTEGER Nullable 
LIBELLE                         VARCHAR(48) Nullable 

Triggers on Table BQ_EMETTRICE:
SET_NOBQEM, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : CAF
===================================================================================================================
NO_CAF                          INTEGER Nullable 
NO_TRAVAIL                      CHAR(9) Nullable 
LIBELLE_TRAVAIL                 CHAR(50) Nullable 
DATE_TRAVAIL                    TIMESTAMP Nullable 
VALIDATION_TRAVAIL              CHAR(1) Nullable 
NO_NATIONAL_EMETTEUR            CHAR(6) Nullable 
CODE_BAILLEUR                   CHAR(5) Nullable 
CODE_AGENCE_BAILLEUR            CHAR(3) Nullable 
DATE_FICHIER                    CHAR(6) Nullable 
RAISON_SOCIALE_CAISSE           CHAR(24) Nullable 
RAISON_SOCIALE_ETABL            CHAR(24) Nullable 
RIB_CODE_ETABL                  CHAR(5) Nullable 
RIB_CODE_GUICHET                CHAR(5) Nullable 
RIB_NO_COMPTE                   CHAR(11) Nullable 
LIBELLE_CODE                    CHAR(1) Nullable 
LIBELLE_LIBELLE                 CHAR(30) Nullable 
CODE_MOTIF                      CHAR(1) Nullable 
CODE_PROGRAMME                  CHAR(5) Nullable 
NO_LOCATAIRE                    CHAR(13) Nullable 
NO_ALLOCATAIRE                  CHAR(15) Nullable 
NOM_ALLOCATAIRE                 CHAR(20) Nullable 
PRENOM_ALLOCATAIRE              CHAR(12) Nullable 
CODE_COLOCATAIRE_BAILLEUR       CHAR(1) Nullable 
PERIODE                         CHAR(4) Nullable 
CODE_MAJ                        CHAR(1) Nullable 
CODE_SITUATION                  CHAR(1) Nullable 
DATE_RADIATION                  CHAR(6) Nullable 
DATE_FIN_BAIL                   CHAR(6) Nullable 
CODE_MAJ_IDENTIFIANT            CHAR(1) Nullable 
CODE_AGENCE_RECTIFIE            CHAR(3) Nullable 
CODE_PROGRAMME_RECTIFIE         CHAR(5) Nullable 
NO_LOCATAIRE_RECTIFIE           CHAR(13) Nullable 
DATE_COLOCATION                 CHAR(6) Nullable 
CODE_PLAN_APUREMENT             CHAR(1) Nullable 
DATE_DEB_PLAN_APUREMENT         CHAR(6) Nullable 
CODE_SAISINE_SDAPL              CHAR(1) Nullable 
DATE_SAISINE_SDAPL              CHAR(6) Nullable 
MONTANT_LOYER                   FLOAT Nullable 

Triggers on Table CAF:
SET_NO_CAF, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : CATEGORIES_RES
===================================================================================================================
NO_CAT_RES                      INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
NBRE_PERSONNE                   INTEGER Nullable 

Triggers on Table CATEGORIES_RES:
SET_NO_CAT_RES, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : CAUTIONS
===================================================================================================================
NO_CAUTION                      INTEGER Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
LIBELLE                         VARCHAR(40) Nullable 
PAYEE                           VARCHAR(1) Nullable 
PAYEE_LE                        TIMESTAMP Nullable 
DATE_CAUTION                    TIMESTAMP Nullable 
DEJA_PAYEE                      NUMERIC(15, 2) Nullable DEFAULT 0
FACTURABLE                      VARCHAR(1) Nullable 
FACTURE                         VARCHAR(1) Nullable 
NO_FACTURE                      VARCHAR(15) Nullable 
NO_TIERS                        INTEGER Nullable 
MONNAIE                         VARCHAR(1) Nullable 
COMPTA_LE                       TIMESTAMP Nullable 
REMBOURSE                       DOUBLE PRECISION Nullable 
NO_SEJOUR                       INTEGER Nullable 
A_REMBOURSER                    VARCHAR(1) Nullable 
CPTA_REMB_LE                    TIMESTAMP Nullable 
DATE_REMB_LE                    TIMESTAMP Nullable 
MT_DIFF                         DOUBLE PRECISION Nullable 
T_TYPE_DG                       INTEGER Nullable 
SUR_FACTURE                     VARCHAR(1) Nullable 
MENTION                         VARCHAR(1) Nullable 
NO_RESIDENT                     INTEGER Nullable 
TP_NO_ROLE                      INTEGER Nullable 
TP_NO_RGLT                      INTEGER Nullable 

Triggers on Table CAUTIONS:
SET_NO_CAUTION, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : CH_LOGT_FJT
===================================================================================================================
NO_CH_LOGT_FJT                  INTEGER Nullable 
LIBELLE                         VARCHAR(50) Nullable 
LIB                             VARCHAR(255) Nullable 
CD_AN                           INTEGER Nullable 
SAISIE                          VARCHAR(1) Nullable 

Triggers on Table CH_LOGT_FJT:
SET_NO_CH_LOGT_FJT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : COMMENTAIRES
===================================================================================================================
NO_COMMENT                      INTEGER Nullable 
NO_RESIDENT                     INTEGER Nullable 
NO_TIERS                        INTEGER Nullable 
DATE_COMMENTAIRE                TIMESTAMP Nullable 
TEXTE                           BLOB segment 80, subtype TEXT Nullable 
T_TYPE                          VARCHAR(1) Nullable 
PASSE                           VARCHAR(1) Nullable 

Triggers on Table COMMENTAIRES:
SET_NO_COMMENT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : COMPTES_CAISSES
===================================================================================================================
NO_CPTE_C                       INTEGER Nullable 
COMPTE                          VARCHAR(15) Nullable 
LIBELLE                         VARCHAR(80) Nullable 
NO_ETABL                        INTEGER Nullable 
CODE_ANA                        VARCHAR(20) Nullable 

Triggers on Table COMPTES_CAISSES:
SET_NO_CPTE_C, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : COURRIERS
===================================================================================================================
NO_COURRIER                     INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
FICHIER                         VARCHAR(1) Nullable 
TYPE_DOCUMENT                   VARCHAR(1) Nullable 
ETAT                            VARCHAR(120) Nullable 
CONSERVE                        VARCHAR(1) Nullable 
PRESTA_PAR_TYPE                 VARCHAR(20) Nullable 

Triggers on Table COURRIERS:
SET_NO_COURRIER, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : CPTA_PRESTA
===================================================================================================================
NO_CPTA_PRESTA                  INTEGER Nullable 
COMPTE                          VARCHAR(15) Nullable 
REPARTITION                     DOUBLE PRECISION Nullable 
JL                              VARCHAR(10) Nullable 
REPARTITION_POURCENT            DOUBLE PRECISION Nullable 
NO_PRESTATION                   INTEGER Nullable 
CODE_ANA                        VARCHAR(20) Nullable 

Triggers on Table CPTA_PRESTA:
SET_NO_CPTA_PRESTA, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : CPTA_REGLT
===================================================================================================================
NO_CPTA_REGLT                   INTEGER Nullable 
NO_TYPE_REG                     INTEGER Nullable 
NO_BANQUE                       INTEGER Nullable 
COMPTE                          VARCHAR(15) Nullable 
JL                              VARCHAR(10) Nullable 
COMPTE_ANA                      VARCHAR(20) Nullable 

Triggers on Table CPTA_REGLT:
SET_NO_CPTA_REGLT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : CSP
===================================================================================================================
NO_CSP                          INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
NO_INSEE                        INTEGER Nullable 

Triggers on Table CSP:
SET_NO_CSP, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : DEPARTS
===================================================================================================================
NO_DEPART                       INTEGER Nullable 
LIBELLE                         VARCHAR(50) Nullable 
LIB                             VARCHAR(255) Nullable 
CD_AN                           INTEGER Nullable 
SAISIE                          VARCHAR(1) Nullable 
CODE                            VARCHAR(1) Nullable 
FAMILLE                         VARCHAR(1) Nullable 

Triggers on Table DEPARTS:
SET_NO_DEPART, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : DETAIL_ETATS
===================================================================================================================
NO_DETAIL_ETAT                  INTEGER Nullable 
NO_ETAT_LIEU                    INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
ETAT                            VARCHAR(20) Nullable 
MEMO                            BLOB segment 80, subtype TEXT Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
LIEU                            VARCHAR(30) Nullable 
LE_TYPE                         VARCHAR(30) Nullable 
QTE                             INTEGER Nullable 
NO_INVENTAIRE                   INTEGER Nullable 

Triggers on Table DETAIL_ETATS:
SET_NO_DETAIL_ETAT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : DETAIL_USER
===================================================================================================================
NO_ACCES                        INTEGER Nullable 
NO_USER                         INTEGER Nullable 
NO_OPTION                       INTEGER Nullable 
PRIS                            VARCHAR(1) Nullable 
MODULE                          VARCHAR(1) Nullable 
NO_RANG                         INTEGER Nullable 
LIBELLE                         VARCHAR(120) Nullable 
CODE                            VARCHAR(6) Nullable 
NO_PROFIL                       INTEGER Nullable 
NO_ETABL                        INTEGER Nullable 

Triggers on Table DETAIL_USER:
SET_NO_ACCES, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : DIPLOMES
===================================================================================================================
NO_DIPLOME                      INTEGER Nullable 
LIBELLE                         VARCHAR(50) Nullable 
CODE                            VARCHAR(4) Nullable 
FAMILLE                         VARCHAR(15) Nullable 

Triggers on Table DIPLOMES:
SET_NO_DIPLOME, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : DIPLOME_RES
===================================================================================================================
NO_DIPLOME                      INTEGER Not Null 
NO_RES                          INTEGER Nullable 
TITRE                           VARCHAR(40) Nullable 
DISCIPLINE                      VARCHAR(24) Nullable 
DT_OBTENTION                    VARCHAR(10) Nullable 
VILLE_OBTENTION                 VARCHAR(40) Nullable 
CONSTRAINT INTEG_113:
  Primary key (NO_DIPLOME)

Triggers on Table DIPLOME_RES:
SET_NODIPLOMERES, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : DOC_RESIDENTS
===================================================================================================================
NO_DOC_RES                      INTEGER Nullable 
NO_DOC_TR                       INTEGER Nullable 
DATE_LIMITE                     TIMESTAMP Nullable 
FAIT                            VARCHAR(1) Nullable 
NO_TIERS                        INTEGER Nullable 
NO_SEJOUR                       INTEGER Nullable 
NO_RESIDENT                     INTEGER Nullable 

Triggers on Table DOC_RESIDENTS:
SET_NO_DOC_RES, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : DOC_TYPES_RES
===================================================================================================================
NO_DOC_TR                       INTEGER Nullable 
NO_TYPE                         INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
NO_TIERS                        INTEGER Nullable 
DELAI                           INTEGER Nullable 

Triggers on Table DOC_TYPES_RES:
SET_NO_DOC_TR, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : DV_CONTRAT_AIDES
===================================================================================================================
NO_CONTRAT_AIDE                 INTEGER Nullable 
LIBELLE                         VARCHAR(50) Nullable 
LIB                             VARCHAR(255) Nullable 
CD_AN                           INTEGER Nullable 
SAISIE                          VARCHAR(1) Nullable 

Triggers on Table DV_CONTRAT_AIDES:
SET_NO_CONTRAT_AIDE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ECART_CONVERSION
===================================================================================================================
ID                              VARCHAR(1) Nullable 
NOM_TABLE                       VARCHAR(50) Nullable 
NOM_CHAMP                       VARCHAR(50) Nullable 
LA_CLE                          VARCHAR(15) Nullable 
NO_PIECE                        VARCHAR(50) Nullable 
MONTANT_FRANC                   DOUBLE PRECISION Nullable 
MONTANT_EURO                    DOUBLE PRECISION Nullable 
ECART_FRANC                     DOUBLE PRECISION Nullable 
===================================================================================================================
Table : EMPLOYEUR
===================================================================================================================
NO_EMPLOY                       INTEGER Nullable 
NO_TIERS_EMP                    INTEGER Nullable 
NO_RES                          INTEGER Nullable 
DT_DEB                          TIMESTAMP Nullable 
DT_FIN                          TIMESTAMP Nullable 
TYPE_CONTRAT                    INTEGER Nullable 

Triggers on Table EMPLOYEUR:
SET_NO_EMPLOY, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ENTOURAGE_RES
===================================================================================================================
NO_ENTOURAGE                    INTEGER Nullable 
NO_RESIDENT                     INTEGER Nullable 
NOM_PP                          VARCHAR(40) Nullable 
PRENOM_PP                       VARCHAR(40) Nullable 
PARENTE_PP                      VARCHAR(40) Nullable 
MEMO_PP                         BLOB segment 80, subtype TEXT Nullable 
TEL_PP                          VARCHAR(15) Nullable 
ORDRE_PP                        INTEGER Nullable 
COURRIEL                        VARCHAR(80) Nullable 
TEL_MOBILE_PP                   VARCHAR(15) Nullable 

Triggers on Table ENTOURAGE_RES:
SET_NO_ENTOURAGE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ENVOI_COURRIERS
===================================================================================================================
NO_ENVOI                        INTEGER Nullable 
DATE_COURRIER                   TIMESTAMP Nullable 
COURRIER                        VARCHAR(120) Nullable 
LIBELLE                         VARCHAR(40) Nullable 
NO_COURRIER                     INTEGER Nullable 
NO_RESIDENT                     INTEGER Nullable 
NO_TIERS                        INTEGER Nullable 
REQUETE                         BLOB segment 80, subtype TEXT Nullable 
NO_SEJOUR                       INTEGER Nullable 

Triggers on Table ENVOI_COURRIERS:
SET_NO_ENVOI, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ETABL
===================================================================================================================
NO_ETABL                        INTEGER Nullable 
NOM                             VARCHAR(40) Nullable 
ADRESSE1                        VARCHAR(40) Nullable 
ADRESSE2                        VARCHAR(40) Nullable 
ADRESSE3                        VARCHAR(40) Nullable 
VILLE                           VARCHAR(40) Nullable 
NOM_USER                        VARCHAR(18) Nullable 
MONNAIE                         VARCHAR(1) Nullable 
NO_TYPE                         SMALLINT Nullable 
MOT_PASSE                       VARCHAR(18) Nullable 
VALID_CODE                      VARCHAR(12) Nullable 
CODE_BAILLEUR                   VARCHAR(8) Nullable 
TYPE_AIDE                       INTEGER Nullable 
POINTAGE_APL                    INTEGER Nullable 
TEXTE_BAS_F                     BLOB segment 80, subtype TEXT Nullable 
NOM_IMG_FACT                    VARCHAR(120) Nullable 
NOM_TEXTE_FACT                  VARCHAR(120) Nullable 
NOM_COMPTA                      VARCHAR(40) Nullable 
NOM_COMPTA1                     VARCHAR(120) Nullable 
NOM_COMPTA2                     VARCHAR(120) Nullable 
NOM_RESP                        VARCHAR(40) Nullable 
FONCTION                        VARCHAR(40) Nullable 
NO_PIECE                        INTEGER Nullable 
NO_PIECE_C                      INTEGER Nullable 
CP                              VARCHAR(10) Nullable 
TEL                             VARCHAR(15) Nullable 
FAX                             VARCHAR(15) Nullable 
COMPTE_CAUTION                  VARCHAR(15) Nullable 
JL_CAUTION                      VARCHAR(10) Nullable 
DELAI_STANDARD                  INTEGER Nullable 
COLLECTIF                       VARCHAR(15) Nullable 
TYPE_NO_FACT                    INTEGER Nullable 
SOLDE_FACT_AIDE                 VARCHAR(1) Nullable 
MONNAIE_APL                     VARCHAR(1) Nullable 
TIERS_APL                       INTEGER Nullable 
LIB_ETABL                       VARCHAR(5) Nullable 
GUICHET                         VARCHAR(5) Nullable 
REF_COMPTE                      VARCHAR(11) Nullable 
CLE_RICE                        VARCHAR(2) Nullable 
NO_NTL_EMETTEUR                 VARCHAR(6) Nullable 
VIRT_NOM                        VARCHAR(24) Nullable 
UTIL_DECOUV                     VARCHAR(1) Nullable 
DECOUVE                         DOUBLE PRECISION Nullable 
NO_TYPESELF                     INTEGER Nullable 
CODE_SUPPR                      VARCHAR(5) Nullable 
NOM_TXT                         VARCHAR(120) Nullable 
NOM_CPTA                        VARCHAR(120) Nullable 
NOM_PAIE                        VARCHAR(120) Nullable 
COMPTE_INDU                     VARCHAR(15) Nullable 
COMPTE_ATTENTE                  VARCHAR(15) Nullable 
JL_ATTENTE                      VARCHAR(10) Nullable 
MODE_PRLVTS                     INTEGER Nullable 
DEB_CPTE                        VARCHAR(8) Nullable 
DEB_CPTE_A                      VARCHAR(8) Nullable 
NO_CPTE                         INTEGER Nullable 
NO_CPTE_A                       INTEGER Nullable 
NOM_FICHIER_CAF                 VARCHAR(160) Nullable 
NOM_FICHIER_CAF2                VARCHAR(160) Nullable 
LAST_NO_LOT                     INTEGER Nullable 
NO_CIEL                         INTEGER Nullable 
CODE_FACTURE                    VARCHAR(3) Nullable 
CODE_AVOIR                      VARCHAR(3) Nullable 
INTERCONSULT_LIB                VARCHAR(20) Nullable 
INTERCONSULT_NO                 INTEGER Nullable 
CONVERSION_EURO                 VARCHAR(5) Nullable 
RECALC_EURO                     VARCHAR(1) Nullable 
FACT_EC                         VARCHAR(1) Nullable 
VERSION                         VARCHAR(10) Nullable 
COMPTE_CLI                      VARCHAR(20) Nullable 
CPT_TRANSIT_VD                  VARCHAR(20) Nullable 
CPT_TIERS_VD                    VARCHAR(20) Nullable 
NOM_CAISSE_CAF                  VARCHAR(50) Nullable 
DEB_CPTE_DG                     VARCHAR(8) Nullable 
NO_CPTE_DG                      INTEGER Nullable 
DT_ARCHIVE                      TIMESTAMP Nullable 
RELANCE                         BLOB segment 80, subtype TEXT Nullable 
RELANCE_B                       BLOB segment 80, subtype TEXT Nullable 
RELANCE1                        BLOB segment 80, subtype TEXT Nullable 
RELANCE_B1                      BLOB segment 80, subtype TEXT Nullable 
RELANCE2                        BLOB segment 80, subtype TEXT Nullable 
RELANCE_B2                      BLOB segment 80, subtype TEXT Nullable 
RELANCE3                        BLOB segment 80, subtype TEXT Nullable 
RELANCE_B3                      BLOB segment 80, subtype TEXT Nullable 
RELANCE4                        BLOB segment 80, subtype TEXT Nullable 
RELANCE_B4                      BLOB segment 80, subtype TEXT Nullable 
RELANCE5                        BLOB segment 80, subtype TEXT Nullable 
RELANCE_B5                      BLOB segment 80, subtype TEXT Nullable 
DATE_MAJ                        TIMESTAMP Nullable 
COLOR_INFO_TEXTE                VARCHAR(10) Nullable 
COLOR_INFO_FOND                 VARCHAR(10) Nullable 
COLOR_INFO_TRANSP               VARCHAR(1) Nullable 
VITESSE_INFO                    INTEGER Nullable 
FINSEJ_AL                       VARCHAR(1) Nullable 
FINSEJ_TPS                      INTEGER Nullable 
FJT_CD_ET                       VARCHAR(10) Nullable 
FJT_CD_PM                       VARCHAR(10) Nullable 
FJT_PM                          VARCHAR(255) Nullable 
E_MAIL                          VARCHAR(80) Nullable 
MODIF_DOS_VERT                  VARCHAR(1) Nullable 
TEXTE_INFO                      VARCHAR(255) Nullable 
ANNEE_DOS_VERT                  INTEGER Nullable 
NO_NTL_EMETTEUR_CAF             VARCHAR(6) Nullable 
SISCO_DOSS                      VARCHAR(5) Nullable 
SISCO_LGCD                      VARCHAR(3) Nullable 
TYPE_AIDE_RAPPEL                INTEGER Nullable 
AFFICHE_DETTE_LOYER             CHAR(1) Nullable 
PERS_1L_V                       VARCHAR(1) Nullable 
PERS_1L_L                       VARCHAR(20) Nullable 
PERS_2MA_V                      VARCHAR(1) Nullable 
PERS_2MA_L                      VARCHAR(20) Nullable 
PERS_3MB_V                      VARCHAR(1) Nullable 
PERS_3MB_L                      VARCHAR(20) Nullable 
PERS_4TA_V                      VARCHAR(1) Nullable 
PERS_4TA_L                      VARCHAR(20) Nullable 
PERS_5TB_V                      VARCHAR(1) Nullable 
PERS_5TB_L                      VARCHAR(20) Nullable 
PERS_6TC_V                      VARCHAR(1) Nullable 
PERS_6TC_L                      VARCHAR(20) Nullable 
PERS_7TD_V                      VARCHAR(1) Nullable 
PERS_7TD_L                      VARCHAR(20) Nullable 
PERS_8TE_V                      VARCHAR(1) Nullable 
PERS_8TE_L                      VARCHAR(20) Nullable 
PERS_9TF_V                      VARCHAR(1) Nullable 
PERS_9TF_L                      VARCHAR(20) Nullable 
PERS_10NA_V                     VARCHAR(1) Nullable 
PERS_10NA_L                     VARCHAR(20) Nullable 
PERS_11NB_V                     VARCHAR(1) Nullable 
PERS_11NB_L                     VARCHAR(20) Nullable 
PERS_12NC_V                     VARCHAR(1) Nullable 
PERS_12NC_L                     VARCHAR(20) Nullable 
GRP_LIGNE_FACT_SELF             CHAR(1) Nullable 
LIB_LIGNE_FACT_SELF             VARCHAR(40) Nullable 
PRELEV_CHEMIN                   VARCHAR(250) Nullable 
PRELEV_FICHIER                  VARCHAR(80) Nullable 
PARAM_SAISIE_RES                VARCHAR(1) Nullable 
HISTO_SAISIE                    VARCHAR(1) Nullable 
DATE_BUTOIR                     TIMESTAMP Nullable 
RETARD_REGLT1                   INTEGER Nullable 
RETARD_REGLT2                   INTEGER Nullable 
RETARD_REGLT3                   INTEGER Nullable 
DEB_CPTE_T                      VARCHAR(8) Nullable 
NO_CPTE_T                       INTEGER Nullable 
AFF_TOT_JOUR                    VARCHAR(1) Nullable DEFAULT 'F'
TREEVIEW_TOUS                   VARCHAR(1) Nullable DEFAULT 'F'
CPTA_EIG_DOSSIER                VARCHAR(4) Nullable 
CPTA_EIG_EXERCICE               VARCHAR(2) Nullable 
CPTA_EIG_NOTRSFT                INTEGER Nullable 
COMPTE_CLI_ANA_VF               VARCHAR(1) Nullable 
FCMB_ST_FICHE_A3                VARCHAR(80) Nullable 
SAGE100_NOPLAN                  INTEGER Nullable 
NBNUITSMAX                      INTEGER Nullable 
UNHAJ_LIBSOC                    VARCHAR(30) Nullable 
TP_LGN1                         VARCHAR(96) Nullable 
TP_LGN2                         VARCHAR(96) Nullable 
TP_LGN3                         VARCHAR(96) Nullable 
TP_CP                           VARCHAR(10) Nullable 
TP_VILLE                        VARCHAR(96) Nullable 
ROLCOL                          INTEGER Nullable 
ROLNAT                          INTEGER Nullable 
ROLPER                          INTEGER Nullable 
ROLREC                          VARCHAR(2) Nullable 
ROLIMP                          VARCHAR(30) Nullable 
ROLROL                          INTEGER Nullable 
ROLEX                           INTEGER Nullable 
TP_CPT                          VARCHAR(10) Nullable 
TP_TITRE                        INTEGER Nullable 
TP_TXT1                         BLOB segment 80, subtype TEXT Nullable 
TP_TXT2                         BLOB segment 80, subtype TEXT Nullable 
COMPTE_CLI_ANA                  VARCHAR(20) Nullable 
COMPTE_INDU_ANA                 VARCHAR(20) Nullable 
COMPTE_DG_ANA                   VARCHAR(20) Nullable 
ICS                             VARCHAR(13) Nullable 
IBAN                            VARCHAR(34) Nullable 
BIC                             VARCHAR(11) Nullable 
SEPAREF                         VARCHAR(17) Nullable 
CODE_IFACTURE                   VARCHAR(3) Nullable 
TEXTE_BAS_F_TVA                 BLOB segment 80, subtype TEXT Nullable 
RC_AL                           VARCHAR(1) Nullable 
RC_TPS                          INTEGER Nullable 
TITRSEJ_AL                      VARCHAR(1) Nullable 
TITRSEJ_TPS                     INTEGER Nullable 
MENTION_ATTESTATION             VARCHAR(255) Nullable 
FC_SIEGE                        VARCHAR(1) Nullable 
FC_INCREMENTCARNET              INTEGER Nullable 
CODE_CAF_AGENCE                 VARCHAR(3) Nullable 
ID_CAISSE                       VARCHAR(5) Nullable 
CPTA_FIRST_V4                   VARCHAR(8) Nullable 
NOM_AFFICHE                     VARCHAR(40) Nullable 
CODE_LIC                        VARCHAR(24) Nullable 
CD_PAYS                         VARCHAR(2) Nullable 
SIRET                           VARCHAR(14) Nullable 

Triggers on Table ETABL:
SET_NO_ETABL, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ETABL_PARAM
===================================================================================================================
IDETABL                         INTEGER Not Null 
NOETABL                         INTEGER Not Null 
IDRGRPM                         INTEGER Nullable 
IDASSO                          INTEGER Nullable 
IDETB                           INTEGER Nullable 
HAJCDPERSMORALE                 VARCHAR(254) Nullable 
HAJCDETB                        VARCHAR(254) Nullable 
HAJREGION                       INTEGER Nullable 
HAJDEPT                         INTEGER Nullable 
FTPHOST                         VARCHAR(254) Nullable 
FTPPORT                         INTEGER Nullable 
FTPUSER                         VARCHAR(45) Nullable 
FTPPASSWORD                     VARCHAR(45) Nullable 
FTPWORKINGDIRECTORY             VARCHAR(254) Nullable 
FTPDATA                         VARCHAR(45) Nullable 
CONSTRAINT INTEG_33:
  Primary key (IDETABL)

Triggers on Table ETABL_PARAM:
SET_IDETABL, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ETATS_LIEUX
===================================================================================================================
NO_ETAT_LIEU                    INTEGER Nullable 
NO_LOGEMENT                     VARCHAR(10) Nullable 
DATE_ETAT                       TIMESTAMP Nullable 
NO_ETABL                        INTEGER Nullable 
TYPE_ETAT                       VARCHAR(1) Nullable 
NO_SEJOUR                       INTEGER Nullable 
NO_RESIDENT                     INTEGER Nullable 

Triggers on Table ETATS_LIEUX:
SET_NO_ETAT_LIEU, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : EVENEMENTS
===================================================================================================================
NO_EVENEMENT                    INTEGER Nullable 
NO_RESIDENT                     INTEGER Nullable 
NO_TIERS                        INTEGER Nullable 
NO_TYPE                         INTEGER Nullable 
DAT_                            TIMESTAMP Nullable 
DATE_DEB                        TIMESTAMP Nullable 
DATE_FIN                        TIMESTAMP Nullable 

Triggers on Table EVENEMENTS:
SET_NO_EVENEMENT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : FACTURES
===================================================================================================================
NO_FACT                         INTEGER Nullable 
NO_FACTURE                      VARCHAR(15) Nullable 
MONTANT_HT                      DOUBLE PRECISION Nullable 
MONTANT_TTC                     DOUBLE PRECISION Nullable 
ACOMPTE                         DOUBLE PRECISION Nullable 
AVOIRS_HT                       DOUBLE PRECISION Nullable 
AVOIRS_TTC                      DOUBLE PRECISION Nullable 
AIDES                           DOUBLE PRECISION Nullable 
DEJA_PAYEE                      DOUBLE PRECISION Nullable 
DEJA_UTILISE                    DOUBLE PRECISION Nullable 
MODIFIABLE                      VARCHAR(1) Nullable 
MONNAIE                         VARCHAR(1) Nullable 
DATE_CALCULS                    TIMESTAMP Nullable 
DATE_VALEUR                     TIMESTAMP Nullable 
DATE_EDITION                    TIMESTAMP Nullable 
DATE_COMPTA                     TIMESTAMP Nullable 
PROVISOIRE                      VARCHAR(1) Nullable 
NO_ETABL                        INTEGER Nullable 
DATE_DEB                        TIMESTAMP Nullable 
DATE_FIN                        TIMESTAMP Nullable 
DU                              DOUBLE PRECISION Nullable 
SOLDE_AIDE                      DOUBLE PRECISION Nullable 
EDIT_Q_LE                       TIMESTAMP Nullable 
SOLDE_EDIT                      DOUBLE PRECISION Nullable 
SOLDE                           DOUBLE PRECISION Nullable 
DATE_ECHEANCE                   TIMESTAMP Nullable 
NIVEAU_RELANCE                  INTEGER Nullable 
REMBOURSE                       DOUBLE PRECISION Nullable 
SOLDE_CAUTION                   DOUBLE PRECISION Nullable 
AVOIRS_I_HT                     DOUBLE PRECISION Nullable 
AVOIRS_I_TTC                    DOUBLE PRECISION Nullable 
P_AVOIR                         VARCHAR(1) Nullable 
NB_BP                           INTEGER Nullable 
NO_CLIENT                       INTEGER Nullable 
CAUTIONS                        DOUBLE PRECISION Nullable 
NO_SEJOUR                       INTEGER Nullable 
NO_RESIDENT                     INTEGER Nullable 
NO_TIERS                        INTEGER Nullable 
TP_NO_ROLE                      INTEGER Nullable 
TP_NO_RGLT                      INTEGER Nullable 
FACT_EXTERNE                    VARCHAR(1) Nullable 
SAISI_PAR                       VARCHAR(10) Nullable 

Triggers on Table FACTURES:
SET_NO_FACT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : FAMILLE_PRESTA
===================================================================================================================
NO_FAMILLE                      INTEGER Nullable 
LIBELLE                         VARCHAR(60) Nullable 

Triggers on Table FAMILLE_PRESTA:
SET_NO_FAMILLE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : FAMILLE_PRESTA_TMP
===================================================================================================================
NO_PRESTATION                   INTEGER Nullable 
NO_FAMILLE                      INTEGER Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
FREQUENCE                       SMALLINT Nullable 
TAUX_TVA                        INTEGER Nullable 
REGULIER                        VARCHAR(1) Nullable 
QTE                             DOUBLE PRECISION Nullable 
===================================================================================================================
Table : FAMILLE_RES
===================================================================================================================
NO_FAMILLE_RES                  INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 

Triggers on Table FAMILLE_RES:
SET_NO_FAMILLE_RES, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : FCMB_CAMPAGNE
===================================================================================================================
IDCAMPAGNE                      INTEGER Not Null 
LIBELLE                         VARCHAR(40) Nullable 
NO_VILLE                        INTEGER Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_120:
  Primary key (IDCAMPAGNE)

Triggers on Table FCMB_CAMPAGNE:
SET_IDCAMPAGNE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : FCMB_COMPETENCESPRO
===================================================================================================================
IDCOMPETENCESPRO                INTEGER Not Null 
NO_RES                          INTEGER Nullable 
CLASSECHELON                    VARCHAR(80) Nullable 
IDNIV                           INTEGER Nullable 
COEF                            INTEGER Nullable 
ENTREPRISEINITIAL               VARCHAR(80) Nullable 
ANNEE                           VARCHAR(4) Nullable 
DEPT                            VARCHAR(3) Nullable 
CONSTRAINT INTEG_116:
  Primary key (IDCOMPETENCESPRO)

Triggers on Table FCMB_COMPETENCESPRO:
SET_IDCOMPETENCESPRO, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : FCMB_CONVENTION_COLL
===================================================================================================================
IDCONVENTION_COLL               INTEGER Not Null 
LIBELLE                         VARCHAR(40) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_122:
  Primary key (IDCONVENTION_COLL)

Triggers on Table FCMB_CONVENTION_COLL:
SET_IDCONVENTION_COLL, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : FCMB_DIPLOME
===================================================================================================================
IDDIPLOME                       INTEGER Not Null 
NO_RES                          INTEGER Nullable 
IDTITRE                         INTEGER Nullable 
IDDISCIPLINE                    INTEGER Nullable 
ANNEE                           VARCHAR(4) Nullable 
CONTROL                         VARCHAR(1) Nullable 
IDETB                           INTEGER Nullable 
IDVILLE                         INTEGER Nullable 
DEPT                            VARCHAR(3) Nullable 
IDACADEMIE                      INTEGER Nullable 
TITREAUTRE                      VARCHAR(80) Nullable 
DISCIPLINEAUTRE                 VARCHAR(80) Nullable 
CONSTRAINT INTEG_115:
  Primary key (IDDIPLOME)

Triggers on Table FCMB_DIPLOME:
SET_IDDIPLOME, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : FCMB_ETAPETDF
===================================================================================================================
IDETAPE                         INTEGER Not Null 
NO_RES                          INTEGER Nullable 
IDVILLE                         INTEGER Nullable 
PERIODE                         VARCHAR(15) Nullable 
IDCONTRAT                       INTEGER Nullable 
ENTREPRISE                      VARCHAR(80) Nullable 
COURRIEL                        VARCHAR(80) Nullable 
IDCAMPAGNE                      INTEGER Nullable 
TUTORAT                         VARCHAR(1) Nullable 
IDEMPLOYEUR                     INTEGER Nullable 
CONSTRAINT INTEG_114:
  Primary key (IDETAPE)

Triggers on Table FCMB_ETAPETDF:
SET_IDETAPE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : FCMB_ETATCOMPAGNON
===================================================================================================================
NO_ETAT                         INTEGER Nullable 
NO_RES                          INTEGER Nullable 
ETAT                            VARCHAR(40) Nullable 
DT                              VARCHAR(10) Nullable 

Triggers on Table FCMB_ETATCOMPAGNON:
SET_NO_ETAT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : FCMB_ETB
===================================================================================================================
IDETB                           INTEGER Not Null 
LIBELLE                         VARCHAR(40) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_121:
  Primary key (IDETB)

Triggers on Table FCMB_ETB:
SET_IDETB, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : FCMB_METIER
===================================================================================================================
IDMETIER                        INTEGER Not Null 
LIBELLE                         VARCHAR(40) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_118:
  Primary key (IDMETIER)

Triggers on Table FCMB_METIER:
SET_IDMETIER, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : FCMB_OBJ_COMPAGNON
===================================================================================================================
NO_OBJ_COMP                     INTEGER Nullable 
OBJ_COMPAGNON                   VARCHAR(40) Nullable 

Triggers on Table FCMB_OBJ_COMPAGNON:
SET_NOOBJCOMP, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : FCMB_OBJ_FORMATION
===================================================================================================================
NO_OBJ_FORM                     INTEGER Nullable 
OBJ_FORMATION                   VARCHAR(40) Nullable 

Triggers on Table FCMB_OBJ_FORMATION:
SET_NOOBJFORM, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : FCMB_RECRUT_INITIAL
===================================================================================================================
IDRECRUT                        INTEGER Not Null 
LIBELLE                         VARCHAR(40) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_123:
  Primary key (IDRECRUT)

Triggers on Table FCMB_RECRUT_INITIAL:
SET_IDRECRUT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : FCMB_SOCIETE
===================================================================================================================
IDSOCIETE                       INTEGER Not Null 
LIBELLE                         VARCHAR(80) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_117:
  Primary key (IDSOCIETE)

Triggers on Table FCMB_SOCIETE:
SET_IDSOCIETE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : FCMB_VILLE
===================================================================================================================
IDVILLE                         INTEGER Not Null 
LIBELLE                         VARCHAR(40) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_119:
  Primary key (IDVILLE)

Triggers on Table FCMB_VILLE:
SET_IDVILLE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : FORMATIONS
===================================================================================================================
NO_FORMATION                    INTEGER Nullable 
LIBELLE                         VARCHAR(50) Nullable 
LIB                             VARCHAR(255) Nullable 
CD_AN                           INTEGER Nullable 
SAISIE                          VARCHAR(1) Nullable 
CODE                            VARCHAR(1) Nullable 
FAMILLE                         VARCHAR(1) Nullable 

Triggers on Table FORMATIONS:
SET_NO_FORMATION, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : HAJPUBLICLOGES
===================================================================================================================
IDPUBLICLOGES                   INTEGER Not Null 
IDETABL                         INTEGER Nullable 
IDSEJOUR                        INTEGER Not Null 
SEJOURTYPE                      INTEGER Nullable 
SEJOURCARACTERISTIQUEPT         INTEGER Nullable 
SEJOURCARACTERISTIQUEPL         VARCHAR(254) Nullable 
SEJOURSOUSLOCVF                 VARCHAR(1) Nullable 
SEJOURBAILGLISSANTVF            VARCHAR(1) Nullable 
SEJOURBAILGLISSANTDTBAIL        TIMESTAMP Nullable 
SEJOURNBPERSONNES               INTEGER Nullable 
SEJOURDUREEENNUIT               INTEGER Nullable 
SEJOURDUREEENCLASSES            INTEGER Nullable 
SEJOURNBPERIODES                INTEGER Nullable 
HAJINDIVPRINCIPALVF             VARCHAR(1) Nullable 
HAJINDIVTUTEURVF                VARCHAR(1) Nullable 
HAJINDIVGARANTVF                VARCHAR(1) Nullable 
RAISONRECHERLOGEMPT             INTEGER Nullable 
RAISONRECHERLOGEMPL             VARCHAR(254) Nullable 
RAISONCHOIXSOLUTIONHAJPT        INTEGER Nullable 
RAISONCHOIXSOLUTIONHAJPL        VARCHAR(254) Nullable 
SITUADEBUTSEJCLASSEAGE          INTEGER Nullable 
SITUADEBUTSEJTYPLOGEMACTUELPT   INTEGER Nullable 
SITUADEBUTSEJTYPLOGEMACTUELPL   VARCHAR(254) Nullable 
SITUADEBUTSEJLOCLOGEMACTUELPT   INTEGER Nullable 
SITUADEBUTSEJLOCLOGEMACTUELPL   VARCHAR(254) Nullable 
SITUADEBUTSEJACTIVITEPRINCPT    INTEGER Nullable 
SITUADEBUTSEJACTIVITEPRINCPL    VARCHAR(254) Nullable 
SITUADEBUTSEJAUTREACTIVITEPT    INTEGER Nullable 
SITUADEBUTSEJAUTREACTIVITEPL    VARCHAR(254) Nullable 
SITUADEBUTSEJMTPENSIONALIMENT   DOUBLE PRECISION Nullable 
SITUADEBUTSEJMTEMPRUNT          DOUBLE PRECISION Nullable 
SITUADEBUTSEJCLASSERESSTOTAL    INTEGER Nullable 
SITUADEBUTSEJMTRESSOURCETOTAL   DOUBLE PRECISION Nullable 
SITUADEBUTSEJRESSDETAILSALAIRE  DOUBLE PRECISION Nullable 
SITUADEBUTSEJRESSDETINDCHOMAGE  DOUBLE PRECISION Nullable 
SITUADEBUTSEJRESSDETAILBOURSE   DOUBLE PRECISION Nullable 
SITUADEBUTSEJRESSDETINDSTAGE    DOUBLE PRECISION Nullable 
SITUADEBUTSEJRESSDETSOUTFAMIL   DOUBLE PRECISION Nullable 
SITUADEBUTSEJRESSDETPENSALIM    DOUBLE PRECISION Nullable 
SITUADEBUTSEJRESSDETALLOCRSA    DOUBLE PRECISION Nullable 
SITUADEBUTSEJRESSDETALLOCPHAND  DOUBLE PRECISION Nullable 
SITUADEBUTSEJRESSDETAUTREPREC1  VARCHAR(254) Nullable 
SITUADEBUTSEJRESSDETAILAUTRE1   DOUBLE PRECISION Nullable 
SITUADEBUTSEJRESSDETAUTREPREC2  VARCHAR(254) Nullable 
SITUADEBUTSEJRESSDETAILAUTRE2   DOUBLE PRECISION Nullable 
SITUADEBUTSEJRESSDETAUTREPREC3  VARCHAR(254) Nullable 
SITUADEBUTSEJRESSDETAILAUTRE3   DOUBLE PRECISION Nullable 
SITUADEBUTSEJNIVEAUSCOLAIREPT   INTEGER Nullable 
SITUADEBUTSEJNIVEAUSCOLAIREPL   VARCHAR(254) Nullable 
SITUAFINSEJCLASSEAGE            INTEGER Nullable 
SITUAFINSEJTYPLOGEMACTUELPT     INTEGER Nullable 
SITUAFINSEJTYPLOGEMACTUELPL     VARCHAR(254) Nullable 
SITUAFINSEJLOCLOGEMACTUELPT     INTEGER Nullable 
SITUAFINSEJLOCLOGEMACTUELPL     VARCHAR(254) Nullable 
SITUAFINSEJACTIVITEPRINCIPALPT  INTEGER Nullable 
SITUAFINSEJACTIVITEPRINCIPALPL  VARCHAR(254) Nullable 
SITUAFINSEJAUTREACTIVITEPT      INTEGER Nullable 
SITUAFINSEJAUTREACTIVITEPL      VARCHAR(254) Nullable 
SITUAFINSEJMTPENSIONALIMENTAIRE DOUBLE PRECISION Nullable 
SITUAFINSEJMTEMPRUNT            DOUBLE PRECISION Nullable 
SITUAFINSEJCLASSERESSOURCETOTAL INTEGER Nullable 
SITUAFINSEJMTRESSOURCETOTAL     DOUBLE PRECISION Nullable 
SITUAFINSEJRESSDETAILSALAIRE    DOUBLE PRECISION Nullable 
SITUAFINSEJRESSDETINDCHOMAGE    DOUBLE PRECISION Nullable 
SITUAFINSEJRESSDETAILBOURSE     DOUBLE PRECISION Nullable 
SITUAFINSEJRESSDETINDSTAGE      DOUBLE PRECISION Nullable 
SITUAFINSEJRESSDETSOUTFAMIL     DOUBLE PRECISION Nullable 
SITUAFINSEJRESSDETPENSALIM      DOUBLE PRECISION Nullable 
SITUAFINSEJRESSDETALLOCRSA      DOUBLE PRECISION Nullable 
SITUAFINSEJRESSDETALLOCPHAND    DOUBLE PRECISION Nullable 
SITUAFINSEJRESSDETAUTREPREC1    VARCHAR(254) Nullable 
SITUAFINSEJRESSDETAILAUTRE1     DOUBLE PRECISION Nullable 
SITUAFINSEJRESSDETAUTREPREC2    VARCHAR(254) Nullable 
SITUAFINSEJRESSDETAILAUTRE2     DOUBLE PRECISION Nullable 
SITUAFINSEJRESSDETAUTREPREC3    VARCHAR(254) Nullable 
SITUAFINSEJRESSDETAILAUTRE3     DOUBLE PRECISION Nullable 
SITUAFINSEJNIVEAUSCOLAIREPT     INTEGER Nullable 
SITUAFINSEJNIVEAUSCOLAIREPL     VARCHAR(254) Nullable 
PRINCIPALRAISONDEPART           INTEGER Nullable 
PRINCIPALRAISONDEPARTPL         VARCHAR(254) Nullable 
CONTEXTEACCOMPAGNEMENT          INTEGER Nullable 
CONTEXTEACCOMPAGNEMENTPL        VARCHAR(254) Nullable 
SUITESEJOUR                     VARCHAR(254) Nullable 
SUITESEJOURPL                   VARCHAR(254) Nullable 
ORIENTATION                     VARCHAR(254) Nullable 
ORIENTATIONPL                   VARCHAR(254) Nullable 
AIDEMOBILISEE                   VARCHAR(254) Nullable 
AIDEMOBILISEEPL                 VARCHAR(254) Nullable 
GARANTIEMISEENOEUVRE            VARCHAR(254) Nullable 
GARANTIEMISEENOEUVREPL          VARCHAR(254) Nullable 
LOGEMENTTROUVESOLUTION          INTEGER Nullable 
LOGEMENTTROUVESOLUTIONPL        VARCHAR(254) Nullable 
LOGEMENTTROUVECONTINGENTSPEC    VARCHAR(254) Nullable 
LOGEMENTTROUVECONTINGENTSPECPL  VARCHAR(254) Nullable 
LOGEMENTTROUVETYPE              INTEGER Nullable 
LOGEMENTTROUVETYPEPL            VARCHAR(254) Nullable 
LOGEMENTTROUVELOCALISATION      INTEGER Nullable 
LOGEMENTTROUVELOCALISATIONPL    VARCHAR(254) Nullable 
SITUAENCOURSSEJDATE             TIMESTAMP Nullable 
SITUAENCOURSSEJCLASSEAGE        INTEGER Nullable 
SITUAENCOURSSEJTYPLOGEMACTUELPT INTEGER Nullable 
SITUAENCOURSSEJTYPLOGEMACTUELPL VARCHAR(254) Nullable 
SITUAENCOURSSEJLOCLOGEMACTUELPT INTEGER Nullable 
SITUAENCOURSSEJLOCLOGEMACTUELPL VARCHAR(254) Nullable 
SITUAENCOURSSEJACTIVITEPRINCPT  INTEGER Nullable 
SITUAENCOURSSEJACTIVITEPRINCPL  VARCHAR(254) Nullable 
SITUAENCOURSSEJAUTREACTIVITEPT  INTEGER Nullable 
SITUAENCOURSSEJAUTREACTIVITEPL  VARCHAR(254) Nullable 
SITUAENCOURSSEJMTPENSIONALIM    DOUBLE PRECISION Nullable 
SITUAENCOURSSEJMTEMPRUNT        DOUBLE PRECISION Nullable 
SITUAENCOURSSEJMTRESSOURCETOTAL DOUBLE PRECISION Nullable 
SITUAENCOURSSEJRESSDETSALAIRE   DOUBLE PRECISION Nullable 
SITUAENCOURSSEJRESSDETINDCHOM   DOUBLE PRECISION Nullable 
SITUAENCOURSSEJRESSDETAILBOURSE DOUBLE PRECISION Nullable 
SITUAENCOURSSEJRESSDETINDSTAGE  DOUBLE PRECISION Nullable 
SITUAENCOURSSEJRESSDETSOUTFAMIL DOUBLE PRECISION Nullable 
SITUAENCOURSSEJRESSDETPENSALIM  DOUBLE PRECISION Nullable 
SITUAENCOURSSEJRESSDETALLOCRSA  DOUBLE PRECISION Nullable 
SITUAENCOURSSEJRESSDETALLOCPH   DOUBLE PRECISION Nullable 
SITUAENCOURSSEJRESSDETAUTRPREC1 VARCHAR(254) Nullable 
SITUAENCOURSSEJRESSDETAILAUTRE1 DOUBLE PRECISION Nullable 
SITUAENCOURSSEJRESSDETAUTRPREC2 VARCHAR(254) Nullable 
SITUAENCOURSSEJRESSDETAILAUTRE2 DOUBLE PRECISION Nullable 
SITUAENCOURSSEJRESSDETAUTRPREC3 VARCHAR(254) Nullable 
SITUAENCOURSSEJRESSDETAILAUTRE3 DOUBLE PRECISION Nullable 
SITUAENCOURSSEJNIVEAUSCOLAIREPT INTEGER Nullable 
SITUAENCOURSSEJNIVEAUSCOLAIREPL VARCHAR(254) Nullable 
CONSTRAINT INTEG_36:
  Primary key (IDPUBLICLOGES)

Triggers on Table HAJPUBLICLOGES:
SET_IDPUBLICLOGES, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : HISTO_SAISIE
===================================================================================================================
NO_HISTO_SAISIE                 INTEGER Nullable 
DATE_SAISIE                     TIMESTAMP Nullable 
NO_RESIDENT                     INTEGER Nullable 
CHAMP_MODIFIE                   VARCHAR(50) Nullable 
VALEUR_SAISIE                   VARCHAR(50) Nullable 
SAISIE_PAR                      VARCHAR(10) Nullable 
VALEUR_PREC                     VARCHAR(50) Nullable 

Triggers on Table HISTO_SAISIE:
SET_HISTO_SAISIE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : INDISPO
===================================================================================================================
ID_INDISPO                      INTEGER Not Null 
NO_LOGEMENT                     VARCHAR(10) Not Null 
NO_ETABL                        INTEGER Not Null 
DATE_DEB                        TIMESTAMP Nullable 
DATE_FIN                        TIMESTAMP Nullable 
MOTIF                           BLOB segment 80, subtype TEXT Nullable 
NO_LOGT                         INTEGER Nullable 
CONSTRAINT INTEG_18:
  Primary key (ID_INDISPO)

Triggers on Table INDISPO:
BI_INDISPO_ID_INDISPO, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : INDUS
===================================================================================================================
NO_INDU                         INTEGER Nullable 
NO_AIDE                         INTEGER Nullable 
NO_RESIDENT                     INTEGER Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
MONNAIE                         VARCHAR(1) Nullable 
NO_TIERS_V                      INTEGER Nullable 
DATE_DEB                        TIMESTAMP Nullable 
DATE_FIN                        TIMESTAMP Nullable 
COMPTA_LE                       TIMESTAMP Nullable 
DATE_INDU                       TIMESTAMP Nullable 
REFERENCE                       VARCHAR(20) Nullable 
MT_DIFF                         DOUBLE PRECISION Nullable 
MT_RBT                          DOUBLE PRECISION Nullable 
REMB_LE                         TIMESTAMP Nullable 
REMB_MODE                       INTEGER Nullable 
REMB_CPTA_LE                    TIMESTAMP Nullable 
NUM_RBT                         INTEGER Nullable 

Triggers on Table INDUS:
SET_NO_INDU, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : INFOS
===================================================================================================================
NO_INFO                         INTEGER Not Null 
IDENTIFIANT                     VARCHAR(5) Nullable 
DATE_ACTION                     TIMESTAMP Nullable 
INFOS                           VARCHAR(255) Nullable 
TEXTE                           BLOB segment 80, subtype TEXT Nullable 
SOURCE                          VARCHAR(50) Nullable 
CONSTRAINT INTEG_30:
  Primary key (NO_INFO)

Triggers on Table INFOS:
BI_INFOS, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : INTERVENANTS
===================================================================================================================
NO_INTERVENANT                  INTEGER Nullable 
NOM                             VARCHAR(40) Nullable 
PRENOM                          VARCHAR(40) Nullable 
NO_ETABL                        INTEGER Nullable 
TEL                             VARCHAR(15) Nullable 
FAX                             VARCHAR(15) Nullable 
TYPE_INTERV                     INTEGER Nullable 
E_MAIL                          VARCHAR(60) Nullable 

Triggers on Table INTERVENANTS:
SET_NO_INTERVENANT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : INTERVENTIONS
===================================================================================================================
NO_INTERVENTION                 INTEGER Nullable 
NO_INTERVENANT                  INTEGER Nullable 
NO_RESIDENT                     INTEGER Nullable 
DATE_INTERV                     TIMESTAMP Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
MONNAIE                         VARCHAR(1) Nullable 
MOTIF                           BLOB segment 80, subtype TEXT Nullable 
PAYEE                           VARCHAR(1) Nullable 
PAYEE_LE                        TIMESTAMP Nullable 
NO_FACTURE                      VARCHAR(15) Nullable 
EDIT_R                          TIMESTAMP Nullable 
EDIT_I                          TIMESTAMP Nullable 
DUREE                           DOUBLE PRECISION Nullable 
NIVEAU                          VARCHAR(10) Nullable 
NO_TYPE                         INTEGER Nullable 
DUREE_UNITE                     VARCHAR(10) Nullable 

Triggers on Table INTERVENTIONS:
SET_NO_INTERVENTION, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : INTERV_MAT
===================================================================================================================
NO_INTERV_MAT                   INTEGER Nullable 
LIEU                            VARCHAR(20) Nullable 
LIBELLE                         BLOB segment 80, subtype TEXT Nullable 
DATE_INTERV_MAXI                TIMESTAMP Nullable 
DATE_INTERV                     TIMESTAMP Nullable 
NO_ETABL                        INTEGER Nullable 

Triggers on Table INTERV_MAT:
GEN_INTERV_MAT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : INTERV_TIERS
===================================================================================================================
NO_INTERV_TIERS                 INTEGER Nullable 
NO_TIERS                        INTEGER Nullable 
DAT_                            TIMESTAMP Nullable 
MEMO                            BLOB segment 80, subtype TEXT Nullable 
COUT                            DOUBLE PRECISION Nullable 
MONNAIE                         VARCHAR(1) Nullable 

Triggers on Table INTERV_TIERS:
SET_NO_INTERV_TIERS, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : INVENTAIRE
===================================================================================================================
NO_INVENTAIRE                   INTEGER Nullable 
NO_LOGEMENT                     VARCHAR(10) Nullable 
LIBELLE                         VARCHAR(40) Nullable 
DATE_ACHAT                      TIMESTAMP Nullable 
VALEUR_ACHAT                    DOUBLE PRECISION Nullable 
MONNAIE                         VARCHAR(1) Nullable 
NO_ETABL                        INTEGER Nullable 
QUANTITE                        INTEGER Nullable 
LIEU                            VARCHAR(30) Nullable 
LE_TYPE                         VARCHAR(30) Nullable 
ETAT                            VARCHAR(20) Nullable 
MEMO                            BLOB segment 80, subtype TEXT Nullable 

Triggers on Table INVENTAIRE:
SET_NO_INVENTAIRE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ITI_CONTRAT
===================================================================================================================
IDCONTRAT                       INTEGER Not Null 
LIBELLE                         VARCHAR(40) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_125:
  Primary key (IDCONTRAT)

Triggers on Table ITI_CONTRAT:
SET_IDCONTRAT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ITI_DISCIPLINE
===================================================================================================================
IDDISCIPLINE                    INTEGER Not Null 
LIBELLE                         VARCHAR(40) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_127:
  Primary key (IDDISCIPLINE)

Triggers on Table ITI_DISCIPLINE:
SET_IDDISCIPLINE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ITI_LANGUE
===================================================================================================================
IDLANGUE                        INTEGER Not Null 
LIBELLE                         VARCHAR(40) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_129:
  Primary key (IDLANGUE)

Triggers on Table ITI_LANGUE:
SET_IDLANGUE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ITI_LOCOMOTION
===================================================================================================================
IDLOCOMOTION                    INTEGER Not Null 
LIBELLE                         VARCHAR(40) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_124:
  Primary key (IDLOCOMOTION)

Triggers on Table ITI_LOCOMOTION:
SET_IDLOCOMOTION, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ITI_NIVEAU
===================================================================================================================
IDNIVEAU                        INTEGER Not Null 
LIBELLE                         VARCHAR(40) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_128:
  Primary key (IDNIVEAU)

Triggers on Table ITI_NIVEAU:
SET_IDNIVEAU, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ITI_TITRE
===================================================================================================================
IDTITRE                         INTEGER Not Null 
LIBELLE                         VARCHAR(40) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_126:
  Primary key (IDTITRE)

Triggers on Table ITI_TITRE:
SET_IDTITRE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIGNES_F
===================================================================================================================
REF_LIGNE                       DOUBLE PRECISION Nullable 
NO_FACTURE                      VARCHAR(15) Nullable 
NO_LIGNE                        SMALLINT Nullable 
LIBELLE                         BLOB segment 80, subtype TEXT Nullable 
LIBELLE_C                       VARCHAR(40) Nullable 
PU_HT                           DOUBLE PRECISION Nullable 
PU_TTC                          DOUBLE PRECISION Nullable 
CODE_TVA                        SMALLINT Nullable 
TAUX_TVA                        DOUBLE PRECISION Nullable 
QTE                             DOUBLE PRECISION Nullable 
TOTAL_HT                        DOUBLE PRECISION Nullable 
TOTAL_TTC                       DOUBLE PRECISION Nullable 
DATE_DEB                        TIMESTAMP Nullable 
DATE_FIN                        TIMESTAMP Nullable 
PAR_BAREME                      VARCHAR(1) Nullable 
ESTIMATION                      VARCHAR(1) Nullable 
NO_TYPE_AIDE                    INTEGER Nullable 
AIDE_VERSEE                     CHAR(1) Nullable 
AIDE_VERSEE_LE                  TIMESTAMP Nullable 
NO_AVOIR                        VARCHAR(15) Nullable 
PASSE_AVOIR                     DOUBLE PRECISION Nullable 
AIDES_PERMANENTE                VARCHAR(1) Nullable 
AVANCE                          VARCHAR(1) Nullable 
COMPTA_LE                       TIMESTAMP Nullable 
NO_TYPE_AFF                     INTEGER Nullable 
LIEN_SELF                       VARCHAR(1) Nullable 
ENVOI_SELF_LE                   TIMESTAMP Nullable 
NOM_RES                         VARCHAR(70) Nullable 
NO_RES                          INTEGER Nullable 
TYPE_LIEN_SELF                  INTEGER Nullable 
PARAM_CPTA                      VARCHAR(1) Nullable 
JL                              VARCHAR(10) Nullable 
COMPTE                          VARCHAR(15) Nullable 
COMPTE_ANA                      VARCHAR(15) Nullable 
COMPTE_TVA                      VARCHAR(15) Nullable 
NO_RESIDENT                     INTEGER Nullable 
NOM_RESIDENT                    VARCHAR(60) Nullable 
NO_SOLDE_CAUTION                INTEGER Nullable 
DATE_SOLDE_CAUTION              TIMESTAMP Nullable 
T_TYPE                          VARCHAR(1) Nullable 
SUR_QUITTANCE                   VARCHAR(1) Nullable 
NO_COMMENT                      INTEGER Nullable 
MONTANT_CALC                    DOUBLE PRECISION Nullable 
NO_ARTICLE_SELF                 INTEGER Nullable 
NO_TARIF_SELF                   INTEGER Nullable 
TYPE_CPTA_SELF                  VARCHAR(1) Nullable 
QTE_OFFERT                      DOUBLE PRECISION Nullable 
NO_PRESTATION                   INTEGER Nullable 
NO_AIDE                         INTEGER Nullable 
NO_CAUTION                      INTEGER Nullable 
NO_PRESTA_RESIDENTS             INTEGER Nullable 

Triggers on Table LIGNES_F:
SET_REF_LIGNE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LISTE_ETATS
===================================================================================================================
NO_LISTE_ETAT                   INTEGER Nullable 
LIBELLE                         CHAR(30) Nullable 
NO_ETABL                        INTEGER Nullable 
SOUS_REP                        CHAR(80) Nullable 
ETAT                            CHAR(80) Nullable 

Triggers on Table LISTE_ETATS:
SET_NO_LISTE_ETAT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_ACTIVITEPRINCIPALE
===================================================================================================================
IDACTIVITEPRINC                 INTEGER Not Null 
LIBELLE                         VARCHAR(96) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_58:
  Primary key (IDACTIVITEPRINC)

Triggers on Table LIST_ACTIVITEPRINCIPALE:
SET_IDACTIVITEPRINC, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_AGREMENTCASF
===================================================================================================================
IDAGREMENTCASF                  INTEGER Not Null 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_90:
  Primary key (IDAGREMENTCASF)

Triggers on Table LIST_AGREMENTCASF:
SET_IDAGREMENTCASF, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_AGREMENTCCH
===================================================================================================================
IDAGREMENTCCH                   INTEGER Not Null 
LIBELLE                         VARCHAR(96) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_88:
  Primary key (IDAGREMENTCCH)

Triggers on Table LIST_AGREMENTCCH:
SET_IDAGREMENTCCH, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_AIDESETGARANTIEMOBILISEES
===================================================================================================================
IDAIDESEGM                      INTEGER Not Null 
LIBELLE                         VARCHAR(96) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_72:
  Primary key (IDAIDESEGM)

Triggers on Table LIST_AIDESETGARANTIEMOBILISEES:
SET_IDAIDESEGM, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_CLASSEAGE
===================================================================================================================
IDCLASSEAGE                     INTEGER Not Null 
LIBELLE                         VARCHAR(48) Nullable 
DEBUT                           INTEGER Nullable 
FIN                             INTEGER Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_110:
  Primary key (IDCLASSEAGE)

Triggers on Table LIST_CLASSEAGE:
SET_IDAGEDOFOECSEJ, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_CLASSEDUREESEJOUR
===================================================================================================================
IDCLASSEDUREESEJ                INTEGER Not Null 
LIBELLE                         VARCHAR(48) Nullable 
DEBUT                           INTEGER Nullable 
FIN                             INTEGER Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_108:
  Primary key (IDCLASSEDUREESEJ)

Triggers on Table LIST_CLASSEDUREESEJOUR:
SET_IDDUREESEJ, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_CLASSERESSOURCES
===================================================================================================================
IDCLASSERESSOURCES              INTEGER Not Null 
LIBELLE                         VARCHAR(48) Nullable 
DEBUT                           INTEGER Nullable 
FIN                             INTEGER Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_112:
  Primary key (IDCLASSERESSOURCES)

Triggers on Table LIST_CLASSERESSOURCES:
SET_IDRESSOURCESDEFEEC, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_COLLECTIFVSDIFFUS
===================================================================================================================
IDCOLLECTIFVSD                  INTEGER Not Null 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_86:
  Primary key (IDCOLLECTIFVSD)

Triggers on Table LIST_COLLECTIFVSDIFFUS:
SET_IDCOLLECTIFVSD, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_COMMENTAVEZVOUSEUIDEESDE
===================================================================================================================
IDCOMMENTAVEIDVAAN              INTEGER Not Null 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_98:
  Primary key (IDCOMMENTAVEIDVAAN)

Triggers on Table LIST_COMMENTAVEZVOUSEUIDEESDE:
SET_IDCOMMENTAVEIDVAAN, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_CONTEXTEACCOMPAGNEMENT
===================================================================================================================
IDCONTEXTEACC                   INTEGER Not Null 
LIBELLE                         VARCHAR(96) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_66:
  Primary key (IDCONTEXTEACC)

Triggers on Table LIST_CONTEXTEACCOMPAGNEMENT:
SET_IDCONTEXTEACC, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_CONTINGENTSPECIFIQUE
===================================================================================================================
IDCONTINGENTSPEC                INTEGER Not Null 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_80:
  Primary key (IDCONTINGENTSPEC)

Triggers on Table LIST_CONTINGENTSPECIFIQUE:
SET_IDCONTINGENTSPEC, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_CONVENTIONSLIEESAULOGT
===================================================================================================================
IDCONVENTIONSLAL                INTEGER Not Null 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_92:
  Primary key (IDCONVENTIONSLAL)

Triggers on Table LIST_CONVENTIONSLIEESAULOGT:
SET_IDCONVENTIONSLAL, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_DEPT
===================================================================================================================
IDDEPT                          INTEGER Not Null 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_40:
  Primary key (IDDEPT)

Triggers on Table LIST_DEPT:
SET_IDDEPT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_FINANCEPAR
===================================================================================================================
IDFINANCEP                      INTEGER Not Null 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_106:
  Primary key (IDFINANCEP)

Triggers on Table LIST_FINANCEPAR:
SET_IDFINANCEP, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_GARANTIEMISEENOEUVRE
===================================================================================================================
IDGARANTIEMEO                   INTEGER Not Null 
LIBELLE                         VARCHAR(96) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_76:
  Primary key (IDGARANTIEMEO)

Triggers on Table LIST_GARANTIEMISEENOEUVRE:
SET_IDGARANTIEMEO, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_ISSUESETSUITESSEJOUACC
===================================================================================================================
IDISSUESETSUITESSOA             INTEGER Not Null 
LIBELLE                         VARCHAR(96) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_70:
  Primary key (IDISSUESETSUITESSOA)

Triggers on Table LIST_ISSUESETSUITESSEJOUACC:
SET_IDISSUESETSUITESSOA, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_LOCALISATIONLOGTAVSEJOUR
===================================================================================================================
IDLOCLOGTAVS                    INTEGER Not Null 
LIBELLE                         VARCHAR(96) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_56:
  Primary key (IDLOCLOGTAVS)

Triggers on Table LIST_LOCALISATIONLOGTAVSEJOUR:
SET_IDLOCLOGTAVS, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_MOTIVATIONNONATTRIBUTION
===================================================================================================================
IDMOTIVATIONNA                  INTEGER Not Null 
LIBELLE                         VARCHAR(96) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_100:
  Primary key (IDMOTIVATIONNA)

Triggers on Table LIST_MOTIVATIONNONATTRIBUTION:
SET_IDMOTIVATIONNA, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_NIVEAUSCOLAIRE
===================================================================================================================
IDNIVEAUSCOLAIRE                INTEGER Not Null 
LIBELLE                         VARCHAR(96) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_62:
  Primary key (IDNIVEAUSCOLAIRE)

Triggers on Table LIST_NIVEAUSCOLAIRE:
SET_IDNIVEAUSCOLAIRE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_ORIENTATIONPROPOSE
===================================================================================================================
IDORIENTATIONP                  INTEGER Not Null 
LIBELLE                         VARCHAR(96) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_68:
  Primary key (IDORIENTATIONP)

Triggers on Table LIST_ORIENTATIONPROPOSE:
SET_IDORIENTATIONP, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_QUELLESONTACTUELACTIVITES
===================================================================================================================
IDQUELLESAVA                    INTEGER Not Null 
LIBELLE                         VARCHAR(96) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_102:
  Primary key (IDQUELLESAVA)

Triggers on Table LIST_QUELLESONTACTUELACTIVITES:
SET_IDQUELLESAVA, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_RAISONCHOIXSOLUTIONHAJ
===================================================================================================================
IDRAISONCSHAJ                   INTEGER Not Null 
LIBELLE                         VARCHAR(96) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_48:
  Primary key (IDRAISONCSHAJ)

Triggers on Table LIST_RAISONCHOIXSOLUTIONHAJ:
SET_IDRAISONCSHAJ, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_RAISONRECHERCHELOGT
===================================================================================================================
IDRAISONRL                      INTEGER Not Null 
LIBELLE                         VARCHAR(96) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_46:
  Primary key (IDRAISONRL)

Triggers on Table LIST_RAISONRECHERCHELOGT:
SET_IDRAISONRL, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_REGION
===================================================================================================================
IDREGION                        INTEGER Not Null 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_38:
  Primary key (IDREGION)

Triggers on Table LIST_REGION:
SET_IDREGION, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_SEJOURCARACTERISTIQUE
===================================================================================================================
IDSEJCARACT                     INTEGER Not Null 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_44:
  Primary key (IDSEJCARACT)

Triggers on Table LIST_SEJOURCARACTERISTIQUE:
SET_IDSEJCARACT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_SEJOURTYPE
===================================================================================================================
IDSEJTYPE                       INTEGER Not Null 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_42:
  Primary key (IDSEJTYPE)

Triggers on Table LIST_SEJOURTYPE:
SET_IDSEJTYPE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_SOLUTIONHBGTTROUVEE
===================================================================================================================
IDSOLUTIONHT                    INTEGER Not Null 
LIBELLE                         VARCHAR(96) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_78:
  Primary key (IDSOLUTIONHT)

Triggers on Table LIST_SOLUTIONHBGTTROUVEE:
SET_IDSOLUTIONHT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_STATUTDEMANDE
===================================================================================================================
IDSTATUTDEMANDE                 INTEGER Not Null 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_96:
  Primary key (IDSTATUTDEMANDE)

Triggers on Table LIST_STATUTDEMANDE:
SET_IDSTATUTDEMANDE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_STATUTGESTIONNAIRE
===================================================================================================================
IDSTATUTG                       INTEGER Not Null 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_82:
  Primary key (IDSTATUTG)

Triggers on Table LIST_STATUTGESTIONNAIRE:
SET_IDSTATUTG, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_TYPELOGEMENT
===================================================================================================================
IDTYPELOGT                      INTEGER Not Null 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_84:
  Primary key (IDTYPELOGT)

Triggers on Table LIST_TYPELOGEMENT:
SET_IDTYPELOGT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_TYPELOGTDEBUTSEJOUR
===================================================================================================================
IDTYPELOGTDS                    INTEGER Not Null 
LIBELLE                         VARCHAR(96) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_52:
  Primary key (IDTYPELOGTDS)

Triggers on Table LIST_TYPELOGTDEBUTSEJOUR:
SET_IDTYPELOGTDS, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_TYPELOGTFINSEJOUR
===================================================================================================================
IDTYPELOGTFS                    INTEGER Not Null 
LIBELLE                         VARCHAR(96) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_54:
  Primary key (IDTYPELOGTFS)

Triggers on Table LIST_TYPELOGTFINSEJOUR:
SET_IDTYPELOGTFS, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_TYPEPROPRIETAIRE
===================================================================================================================
IDTYPEPROPRIO                   INTEGER Not Null 
LIBELLE                         VARCHAR(96) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_50:
  Primary key (IDTYPEPROPRIO)

Triggers on Table LIST_TYPEPROPRIETAIRE:
SET_IDTYPEPROPRIO, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LIST_TYPERDV
===================================================================================================================
IDTYPERDV                       INTEGER Not Null 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 
CONSTRAINT INTEG_104:
  Primary key (IDTYPERDV)

Triggers on Table LIST_TYPERDV:
SET_IDTYPERDV, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : LOGEMENTS
===================================================================================================================
NO_LOGT                         INTEGER Nullable 
NO_LOGEMENT                     VARCHAR(10) Nullable 
IMAGE                           BLOB segment 80, subtype BINARY Nullable 
SURFACE                         DOUBLE PRECISION Nullable 
NBRE_LITS                       DOUBLE PRECISION Nullable 
TELEPHONE                       VARCHAR(14) Nullable 
REF_CLEF                        VARCHAR(10) Nullable 
ETAGE                           DOUBLE PRECISION Nullable 
DIVERS                          BLOB segment 80, subtype TEXT Nullable 
NO_ETABL                        INTEGER Nullable 
NO_BATIMENT                     INTEGER Nullable 
BATIMENT                        INTEGER Nullable 
MEUBLE                          VARCHAR(1) Nullable 
PARKING                         VARCHAR(1) Nullable 
CAVE                            VARCHAR(1) Nullable 
INTERPHONE                      VARCHAR(1) Nullable 
DIGICODE                        VARCHAR(1) Nullable 
ENTREE                          VARCHAR(1) Nullable 
SDB                             VARCHAR(1) Nullable 
SDB_EQP                         VARCHAR(48) Nullable 
WC                              VARCHAR(1) Nullable 
CHAUFFAGE                       VARCHAR(1) Nullable 
CHAUFFAGE_EQP                   VARCHAR(48) Nullable 
CUISINE                         VARCHAR(1) Nullable 
CUISINE_EQP                     VARCHAR(48) Nullable 
CONSO_ENERGIE                   VARCHAR(1) Nullable 
LOYER_DEM                       DOUBLE PRECISION Nullable 
CHARGES_DEM                     DOUBLE PRECISION Nullable 
CH_EAU                          DOUBLE PRECISION Nullable 
CH_ELECT                        DOUBLE PRECISION Nullable 
CH_CHAUFFAGE                    DOUBLE PRECISION Nullable 
CH_ORDURES                      DOUBLE PRECISION Nullable 
CH_ENTRETIEN                    DOUBLE PRECISION Nullable 
CH_AUTRES                       DOUBLE PRECISION Nullable 
FRAIS                           DOUBLE PRECISION Nullable 
DG_DEM                          DOUBLE PRECISION Nullable 
NO_TIERS                        INTEGER Nullable 
ADRESSE                         INTEGER Nullable 
T_TYPE                          INTEGER Nullable 
CONSO_KWH                       DOUBLE PRECISION Nullable 
LIEU_LOGT                       INTEGER Nullable 
NON_LOGT                        VARCHAR(1) Nullable 

Triggers on Table LOGEMENTS:
SET_NO_LOGT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : MOTIFS_DEP
===================================================================================================================
NO_MOTIF                        INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
LIB                             VARCHAR(255) Nullable 
CD_AN                           INTEGER Nullable 
SAISIE                          VARCHAR(1) Nullable 

Triggers on Table MOTIFS_DEP:
SET_NO_MOTIF, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : MOTIVATIONS
===================================================================================================================
NO_MOTIVATION                   INTEGER Nullable 
LIBELLE                         VARCHAR(50) Nullable 
LIB                             VARCHAR(255) Nullable 
CD_AN                           INTEGER Nullable 
SAISIE                          VARCHAR(1) Nullable 
CODE                            VARCHAR(1) Nullable 
FAMILLE                         VARCHAR(1) Nullable 

Triggers on Table MOTIVATIONS:
SET_NO_MOTIVATION, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : MT_REVENUS
===================================================================================================================
NO_MT_REVENUS                   INTEGER Nullable 
LIBELLE                         VARCHAR(50) Nullable 
MT_MIN                          DOUBLE PRECISION Nullable 
MT_MAX                          DOUBLE PRECISION Nullable 
CD_AN                           INTEGER Nullable 
SAISIE                          VARCHAR(1) Nullable 

Triggers on Table MT_REVENUS:
SET_NO_MT_REVENUS, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : MVT_CAISSE
===================================================================================================================
NO_MVT_CAISSE                   INTEGER Nullable 
LIBELLE                         VARCHAR(60) Nullable 
NO_TYPE_REG                     INTEGER Nullable 
NO_BANQUE                       INTEGER Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
MONNAIE                         VARCHAR(1) Nullable 
DATE_SOLDE                      TIMESTAMP Nullable 
COMPTA_LE                       TIMESTAMP Nullable 
COMPTE                          VARCHAR(15) Nullable 
NO_ETABL                        INTEGER Nullable 
COMPTE_ANA                      VARCHAR(15) Nullable 
SAISI_PAR                       VARCHAR(10) Nullable 
CODE_ANA                        VARCHAR(15) Nullable 

Triggers on Table MVT_CAISSE:
SET_NO_MVT_CAISSE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : MVT_CPTES
===================================================================================================================
NO_MVT_CPTE                     INTEGER Nullable 
NO_RESIDENT                     INTEGER Nullable 
NO_TIERS                        INTEGER Nullable 
LIBELLE                         VARCHAR(80) Nullable 
DATE_MVT                        TIMESTAMP Nullable 
COMPTA_LE                       TIMESTAMP Nullable 
COMPTE                          VARCHAR(15) Nullable 
JL                              VARCHAR(10) Nullable 
DEBIT                           DOUBLE PRECISION Nullable 
CREDIT                          DOUBLE PRECISION Nullable 

Triggers on Table MVT_CPTES:
SET_NO_MVT_CPTE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : MVT_SEJOURS
===================================================================================================================
NO_MVT_SEJOUR                   INTEGER Nullable 
DATE_DEB                        TIMESTAMP Nullable 
DATE_FIN                        TIMESTAMP Nullable 
NO_LOGEMENT                     VARCHAR(10) Nullable 
NO_SEJOUR                       INTEGER Nullable 
NO_ETABL                        INTEGER Nullable 
NOM_RES                         VARCHAR(60) Nullable 
BADGCLI                         VARCHAR(15) Nullable 
NO_LIT                          INTEGER Nullable 
NO_LOGT                         INTEGER Nullable 

Triggers on Table MVT_SEJOURS:
SET_NO_MVT_SEJOUR, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : NATIONALITES
===================================================================================================================
NO_NATION                       INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
CODE_NATION                     VARCHAR(10) Nullable 

Triggers on Table NATIONALITES:
SET_NO_NATION, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : NATURE_PRESTA
===================================================================================================================
NO_NATURE_PRESTA                INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 

Triggers on Table NATURE_PRESTA:
SET_NO_NATURE_PRESTA, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : NE_A
===================================================================================================================
NO_NE_A                         INTEGER Nullable 
LIBELLE                         VARCHAR(50) Nullable 
LIB                             VARCHAR(255) Nullable 
CD_AN                           INTEGER Nullable 
SAISIE                          VARCHAR(1) Nullable 
CODE                            VARCHAR(1) Nullable 
FAMILLE                         VARCHAR(1) Nullable 

Triggers on Table NE_A:
SET_NO_NE_A, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : ORDINATEURS
===================================================================================================================
NOM_ORDINATEUR                  VARCHAR(255) Nullable 
NOM_UTILISATEUR                 VARCHAR(255) Nullable 
CHEMIN_PROG_LOCAL               VARCHAR(255) Nullable 
CHEMIN_PROG_SERVEUR             VARCHAR(255) Nullable 
VERSION                         VARCHAR(20) Nullable 
PROCESSEUR                      VARCHAR(60) Nullable 
RESOLUTION                      VARCHAR(20) Nullable 
OS                              VARCHAR(50) Nullable 
IP                              VARCHAR(20) Nullable 
CAISSE_SELF                     VARCHAR(3) Nullable 
NO_CAISSE                       INTEGER Nullable 
PRINT                           VARCHAR(7) Nullable 
RAM                             VARCHAR(15) Nullable 
RAM_DISPO                       VARCHAR(15) Nullable 
DISPO_C                         VARCHAR(15) Nullable 
FREQUENCE                       VARCHAR(15) Nullable 
MAJ                             VARCHAR(30) Nullable 
===================================================================================================================
Table : ORG_GEO
===================================================================================================================
NO_ORG                          INTEGER Nullable 
LIBELLE                         VARCHAR(50) Nullable 
LIB                             VARCHAR(255) Nullable 
CD_AN                           INTEGER Nullable 
SAISIE                          VARCHAR(1) Nullable 
CODE                            VARCHAR(1) Nullable 
FAMILLE                         VARCHAR(1) Nullable 

Triggers on Table ORG_GEO:
SET_NO_ORG, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : PARAM_SAISIE_RES
===================================================================================================================
NO_ID                           INTEGER Nullable 
NO_ETABL                        INTEGER Nullable 
NOM_COMPOSANT                   VARCHAR(20) Nullable 
VISIBLE                         VARCHAR(1) Nullable 
RANG_AFF                        INTEGER Nullable 
DESCRIPTIF                      VARCHAR(80) Nullable 
IMPRIMABLE                      VARCHAR(1) Nullable 

Triggers on Table PARAM_SAISIE_RES:
SET_PARAM_SAISIE_RES, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : PERS_1L
===================================================================================================================
NO_PERS_1L                      INTEGER Nullable 
NO_RESIDENT                     INTEGER Nullable 
RUBRIQUE                        VARCHAR(20) Nullable 
DU                              TIMESTAMP Nullable 
AU                              TIMESTAMP Nullable 
LIBELLE                         VARCHAR(60) Nullable 

Triggers on Table PERS_1L:
SET_NO_PERS_1L, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : PRESTATIONS
===================================================================================================================
NO_PRESTATION                   INTEGER Not Null 
LIBELLE                         VARCHAR(80) Nullable 
PAR_BAREME                      VARCHAR(1) Nullable 
ESTIMATION                      VARCHAR(1) Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
DECOUPAGE_CPTA                  VARCHAR(1) Nullable 
DECOUPE_MONTANT                 VARCHAR(1) Nullable 
CODE_CATEGORIE                  VARCHAR(10) Nullable 
MONNAIE                         VARCHAR(1) Nullable 
FREQUENCE                       SMALLINT Nullable 
FACTURABLE                      VARCHAR(1) Nullable 
EFFET_PLANNING                  VARCHAR(1) Nullable 
NO_ETABL                        INTEGER Nullable 
JL                              VARCHAR(10) Nullable 
COMPTE_TVA                      VARCHAR(15) Nullable 
AVANCE                          VARCHAR(1) Nullable 
TYPE_LIEN_SELF                  INTEGER Nullable 
SUR_QUITTANCE                   VARCHAR(1) Nullable 
PR_RESIDENT                     VARCHAR(1) Nullable 
PR_VTE_DIRECTE                  VARCHAR(1) Nullable 
PR_GROUPE                       VARCHAR(1) Nullable 
PR_SELF                         VARCHAR(1) Nullable 
GR_SELF                         VARCHAR(1) Nullable 
NO_TYPE_PRESTA                  INTEGER Nullable 
TAUX_TVA                        INTEGER Nullable 
EXPORTWEB                       VARCHAR(1) Nullable 
NO_NATURE_PRESTA                INTEGER Nullable 
CONSTRAINT INTEG_27:
  Primary key (NO_PRESTATION)

Triggers on Table PRESTATIONS:
SET_NO_PRESTATION, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : PRESTA_RESIDENTS
===================================================================================================================
NO_PRESTA_RESIDENTS             INTEGER Nullable 
NO_RESIDENT                     INTEGER Nullable 
NO_PRESTA                       INTEGER Nullable 
QTE                             DOUBLE PRECISION Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
MONNAIE                         VARCHAR(1) Nullable 
FREQUENCE                       SMALLINT Nullable 
REGULIER                        VARCHAR(1) Nullable 
FACTURE                         VARCHAR(1) Nullable 
FACTURE_LE                      TIMESTAMP Nullable 
LIBELLE                         VARCHAR(80) Nullable 
LAST_FACT                       TIMESTAMP Nullable 
NO_SEJOUR                       INTEGER Nullable 
DATE_DEB                        TIMESTAMP Nullable 
DATE_FIN                        TIMESTAMP Nullable 
EFFET_PLANNING                  VARCHAR(1) Nullable 
FACTURABLE                      VARCHAR(1) Nullable 
AVANCE                          VARCHAR(1) Nullable 
TYPE_LIEN_SELF                  INTEGER Nullable 
PARAM_CPTA                      VARCHAR(1) Nullable 
JL                              VARCHAR(10) Nullable 
COMPTE                          VARCHAR(15) Nullable 
COMPTE_ANA                      VARCHAR(15) Nullable 
COMPTE_TVA                      VARCHAR(15) Nullable 
TAUX_TVA                        INTEGER Nullable 
BAREME                          VARCHAR(1) Nullable 
PR_SELF                         VARCHAR(1) Nullable 
NO_ARTICLE_SELF                 INTEGER Nullable 
NO_TARIF_SELF                   INTEGER Nullable 
TYPE_CPTA_SELF                  VARCHAR(1) Nullable 
NO_SERVICE                      INTEGER Nullable 
DATE_PASSAGE                    TIMESTAMP Nullable 
NOM_TRANSFERT                   VARCHAR(20) Nullable 
QTE_OFFERT                      DOUBLE PRECISION Nullable 
NO_PLANNING                     INTEGER Nullable 
NO_FAMILLE                      INTEGER Nullable 
NO_TIERS                        INTEGER Nullable 
ID_PP                           INTEGER Nullable 
ID_PPR                          INTEGER Nullable 
SAL_NO_RESA                     INTEGER Nullable 
CONSTRAINT FK_PRESTA_RESIDENTS:
  Foreign key (ID_PPR)    References ALT_PLANNING_PRESTA_RES (ID_PPR) On Update Cascade On Delete Cascade

Triggers on Table PRESTA_RESIDENTS:
SET_NO_PRESTA_RESIDENTS, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : PRISES_CHARGES
===================================================================================================================
NO_PRISE                        INTEGER Nullable 
NO_RESIDENT                     INTEGER Nullable 
NO_TIERS                        INTEGER Nullable 
LIBELLE                         VARCHAR(80) Nullable 
DATE_DEB                        TIMESTAMP Nullable 
DATE_FIN                        TIMESTAMP Nullable 
REF_DOSSIER                     VARCHAR(40) Nullable 
CONTACT                         VARCHAR(80) Nullable 

Triggers on Table PRISES_CHARGES:
SET_NO_PRISE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : PROFESSION_RES
===================================================================================================================
NO_PROF_RES                     INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 

Triggers on Table PROFESSION_RES:
SET_NO_PROF_RES, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : PROFIL
===================================================================================================================
NO_PROFIL                       INTEGER Nullable 
NOM_PROFIL                      VARCHAR(24) Nullable 

Triggers on Table PROFIL:
SET_NO_PROFIL, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : PROF_PARENTS
===================================================================================================================
NO_PROF                         INTEGER Nullable 
LIBELLE                         VARCHAR(50) Nullable 
LIB                             VARCHAR(255) Nullable 
CD_AN                           INTEGER Nullable 
SAISIE                          VARCHAR(1) Nullable 
CODE                            VARCHAR(1) Nullable 
FAMILLE                         VARCHAR(1) Nullable 

Triggers on Table PROF_PARENTS:
SET_NO_PROF, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : PROSPECT_STATUT
===================================================================================================================
NO_PROSPECT_STATUT              INTEGER Nullable 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 
MASK                            VARCHAR(1) Nullable 

Triggers on Table PROSPECT_STATUT:
SET_NOPROSPECTSTATUT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : RECUS
===================================================================================================================
NO_RECU                         INTEGER Nullable 
NO_REGLT                        INTEGER Nullable 
NO_REMBT                        INTEGER Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
MONNAIE                         VARCHAR(1) Nullable 
TEXTE                           BLOB segment 80, subtype TEXT Nullable 
DATE_RECU                       TIMESTAMP Nullable 
NO_ETABL                        INTEGER Nullable 
NO_RESIDENT                     INTEGER Nullable 
NO_TIERS                        INTEGER Nullable 
TEXTE_INFOS                     BLOB segment 80, subtype TEXT Nullable 
MT_DIFF                         DOUBLE PRECISION Nullable 
ANNULE                          VARCHAR(1) Nullable 
NO_CAUTION                      INTEGER Nullable 
NUMERO                          VARCHAR(21) Nullable 
NO_OPERATION_VENTE_DIRECTE      INTEGER Nullable 

Triggers on Table RECUS:
SET_NO_RECU, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : REGIMES
===================================================================================================================
NO_REGIME                       INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 

Triggers on Table REGIMES:
SET_NO_REGIME, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : REGLT
===================================================================================================================
NO_REGLT                        INTEGER Nullable 
NO_RESIDENT                     INTEGER Nullable 
NO_TIERS                        INTEGER Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
MONNAIE                         VARCHAR(1) Nullable 
DEJA_UTILISE                    DOUBLE PRECISION Nullable 
DAT_                            TIMESTAMP Nullable 
PAYE_PAR                        INTEGER Nullable 
REFERENCE                       VARCHAR(40) Nullable 
COMPTA                          VARCHAR(1) Nullable 
COMPTA_LE                       TIMESTAMP Nullable 
MODIFIABLE                      VARCHAR(1) Nullable 
VENTILE                         VARCHAR(1) Nullable 
NO_ETABL                        INTEGER Nullable 
NO_BANQUE                       INTEGER Nullable 
DATE_BORDEREAU                  TIMESTAMP Nullable 
TIREUR                          VARCHAR(40) Nullable 
REMBOURSE                       DOUBLE PRECISION Nullable 
SOLDE_IMPAYE                    DOUBLE PRECISION Nullable 
IMPAYE                          VARCHAR(1) Nullable 
NO_IMPAYE                       INTEGER Nullable 
PARAM_CPTA                      VARCHAR(1) Nullable 
JL                              VARCHAR(10) Nullable 
COMPTE                          VARCHAR(15) Nullable 
COMPTE_ANA                      VARCHAR(15) Nullable 
COMPTE_TVA                      VARCHAR(15) Nullable 
PAR_PRLVTS                      VARCHAR(1) Nullable 
COORDONNEES                     VARCHAR(23) Nullable 
EMIS_LE                         TIMESTAMP Nullable 
PRELEVE_LE                      TIMESTAMP Nullable 
NO_LOT                          INTEGER Nullable 
MT_DIFF                         DOUBLE PRECISION Nullable 
SAISI_PAR                       VARCHAR(10) Nullable 
NO_SEJOUR                       INTEGER Nullable 
NO_OPERATION_VENTE_DIRECTE      INTEGER Nullable 
TP_NO_ROLE                      INTEGER Nullable 
TP_TITRE                        INTEGER Nullable 
TP_EXERCICE                     INTEGER Nullable 
TP_IMP_CPTA                     VARCHAR(10) Nullable 
TP_EMIS_LE                      TIMESTAMP Nullable 
BQ_EMETTRICE                    VARCHAR(36) Nullable 
NO_CHEQUE                       VARCHAR(12) Nullable 

Triggers on Table REGLT:
SET_NO_REGLT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : REMBOURSEMENTS
===================================================================================================================
NO_REMB                         INTEGER Nullable 
NO_AIDE                         INTEGER Nullable 
NO_REGLT                        INTEGER Nullable 
NO_TIERS                        INTEGER Nullable 
NO_RESIDENT                     INTEGER Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
MONNAIE                         VARCHAR(1) Nullable 
DATE_REMB                       TIMESTAMP Nullable 
INFOS                           BLOB segment 80, subtype TEXT Nullable 
COMPTA_LE                       TIMESTAMP Nullable 
NO_TYPE_REG                     INTEGER Nullable 
NO_BANQUE                       INTEGER Nullable 
NO_ETABL                        INTEGER Nullable 
NO_CAUTION                      INTEGER Nullable 
PARAM_CPTA                      VARCHAR(1) Nullable 
JL                              VARCHAR(10) Nullable 
COMPTE                          VARCHAR(15) Nullable 
COMPTE_ANA                      VARCHAR(15) Nullable 
COMPTE_TVA                      VARCHAR(15) Nullable 
SAISI_PAR                       VARCHAR(10) Nullable 
INDU                            VARCHAR(1) Nullable 
LOT                             INTEGER Nullable 
DT_VRM                          TIMESTAMP Nullable 

Triggers on Table REMBOURSEMENTS:
SET_NO_REMB, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : RESIDENTS
===================================================================================================================
NO_RESIDENT                     INTEGER Not Null 
NOM                             VARCHAR(40) Nullable 
PRENOM                          VARCHAR(40) Nullable 
NE_LE                           TIMESTAMP Nullable 
NE_A                            VARCHAR(40) Nullable 
IMAGE                           BLOB segment 80, subtype BINARY Nullable 
ADRESSE_ET                      VARCHAR(1) Nullable 
SEXE                            VARCHAR(1) Nullable 
DIVERS                          BLOB segment 80, subtype TEXT Nullable 
COMPTE_ACP                      VARCHAR(15) Nullable 
NO_GROUPE                       VARCHAR(1) Nullable 
NO_SECU                         VARCHAR(21) Nullable 
NOM_JF                          VARCHAR(40) Nullable 
SUIVI_SOCIAL                    VARCHAR(1) Nullable 
VIREMENT                        VARCHAR(1) Nullable 
ETABL                           VARCHAR(5) Nullable 
GUICHET                         VARCHAR(5) Nullable 
REF_COMPTE                      VARCHAR(11) Nullable 
CLE_RICE                        VARCHAR(2) Nullable 
DOMICILIATION                   VARCHAR(40) Nullable 
TYPE_TITRE                      VARCHAR(40) Nullable 
DELIVRE_PAR                     VARCHAR(40) Nullable 
DELIVRE_LE                      TIMESTAMP Nullable 
DATE_FIN_VALIDITE               TIMESTAMP Nullable 
PROROGATION                     TIMESTAMP Nullable 
NO_ALLOCATAIRE                  VARCHAR(15) Nullable 
PROFESSION                      VARCHAR(40) Nullable 
EVALUATION_SOCIALE              VARCHAR(1) Nullable 
FIN_RC                          TIMESTAMP Nullable 
REFERENCE_RC                    VARCHAR(80) Nullable 
COLOCATAIRE                     VARCHAR(40) Nullable 
NOMP_SUIVI                      VARCHAR(40) Nullable 
IMMAT                           VARCHAR(12) Nullable 
VEHICULE                        VARCHAR(40) Nullable 
REF_TITRE                       VARCHAR(40) Nullable 
SDAPL                           VARCHAR(1) Nullable 
DATE_SDAPL                      TIMESTAMP Nullable 
DATE2_SDAPL                     TIMESTAMP Nullable 
NOM_PA                          VARCHAR(40) Nullable 
MEMO_PA                         BLOB segment 80, subtype TEXT Nullable 
CIVILITE                        VARCHAR(12) Nullable 
STATUS                          DOUBLE PRECISION Nullable 
DELAI_PAIE                      DOUBLE PRECISION Nullable 
NOM_AD                          VARCHAR(40) Nullable 
MEMO_AD                         BLOB segment 80, subtype TEXT Nullable 
ENTREE_CR                       TIMESTAMP Nullable 
SORTIE_CR                       TIMESTAMP Nullable 
STATUS_CR                       VARCHAR(1) Nullable 
NOM_CR                          VARCHAR(40) Nullable 
NOM_JF_CR                       VARCHAR(40) Nullable 
PRENOM_CR                       VARCHAR(40) Nullable 
NE_LE_CR                        TIMESTAMP Nullable 
NE_A_CR                         VARCHAR(40) Nullable 
SEXE_CR                         VARCHAR(1) Nullable 
DIVERS_CR                       BLOB segment 80, subtype TEXT Nullable 
NO_TIERS_FACTURE                INTEGER Nullable 
NO_ETABL                        INTEGER Nullable 
NOM_CAU                         VARCHAR(40) Nullable 
MONNAIE_CAU                     VARCHAR(1) Nullable 
INFOS_RES                       BLOB segment 80, subtype TEXT Nullable 
TEL                             VARCHAR(15) Nullable 
TEL_PA                          VARCHAR(15) Nullable 
TEL_AD                          VARCHAR(15) Nullable 
TEL_CAU                         VARCHAR(15) Nullable 
FAX_CAU                         VARCHAR(15) Nullable 
NO_CAT_RES                      INTEGER Nullable 
DEPT_NE                         VARCHAR(3) Nullable 
BADGCLI                         VARCHAR(15) Nullable 
LIEU_N                          INTEGER Nullable 
RESSOURCES                      DOUBLE PRECISION Nullable 
TYPE_RESSOURCES                 INTEGER Nullable 
COMPTE_INDU                     VARCHAR(15) Nullable 
INSCRIT_LE                      TIMESTAMP Nullable 
ANNULE_LE                       TIMESTAMP Nullable 
RELANCE_LE                      TIMESTAMP Nullable 
TUTEUR_PA1                      VARCHAR(1) Nullable 
CHAMBRE_OCCUPEE                 VARCHAR(50) Nullable 
RES_PRESENT                     VARCHAR(1) Nullable 
PUBLIC                          VARCHAR(1) Nullable 
NBPERSONNE                      INTEGER Nullable 
RESPONSABLE                     VARCHAR(40) Nullable 
MONTANT_CAU                     DOUBLE PRECISION Nullable 
SQL1_V40                        VARCHAR(40) Nullable 
SQL2_V40                        VARCHAR(40) Nullable 
COMPTE                          VARCHAR(20) Nullable 
TEL_RESP                        VARCHAR(15) Nullable 
TEL_MOBILE                      VARCHAR(15) Nullable 
NO_LOCATAIRE_CAF                VARCHAR(13) Nullable 
REPRISE_SUR_REVISION            VARCHAR(1) Nullable 
PLAN_APUREMENT                  INTEGER Nullable 
DATE_PLAN_APUREMENT             TIMESTAMP Nullable 
COMPTE_DG                       VARCHAR(15) Nullable 
CHB_OCC                         VARCHAR(10) Nullable 
MODIF_RESA_SELF                 VARCHAR(1) Nullable 
SALAIRE                         DOUBLE PRECISION Nullable 
PERS_2MA                        BLOB segment 80, subtype TEXT Nullable 
PERS_3MB                        BLOB segment 80, subtype TEXT Nullable 
PERS_4TA                        VARCHAR(40) Nullable 
PERS_5TB                        VARCHAR(40) Nullable 
PERS_6TC                        VARCHAR(40) Nullable 
PERS_7TD                        VARCHAR(40) Nullable 
PERS_8TE                        VARCHAR(40) Nullable 
PERS_9TF                        VARCHAR(40) Nullable 
PERS_10NA                       DOUBLE PRECISION Nullable 
PERS_11NB                       DOUBLE PRECISION Nullable 
PERS_12NC                       DOUBLE PRECISION Nullable 
NBRE_ENFANTS                    INTEGER Nullable 
NBRE_ENFANTS_CHARGE             INTEGER Nullable 
NBRE_ENFANTS_PAYS               INTEGER Nullable 
NO_CSP                          INTEGER Nullable 
NO_DEPART_CR                    INTEGER Nullable 
NO_NATIONALITE                  INTEGER Nullable 
NO_NATION_CR                    INTEGER Nullable 
NO_REGIME                       INTEGER Nullable 
NO_SECTEUR                      INTEGER Nullable 
NO_TYPE                         INTEGER Nullable 
SITUATION_F                     INTEGER Nullable 
NO_RANG                         INTEGER Nullable 
REINIT_REVENU                   VARCHAR(1) Nullable 
NO_FAMILLE_RES                  INTEGER Nullable 
FACT_PERS_CAUTION               VARCHAR(1) Nullable 
NO_PROFESSION_RES               INTEGER Nullable 
PERMIS_CAT                      VARCHAR(12) Nullable 
FCMB_LANGUES_ETR                BLOB segment 80, subtype TEXT Nullable 
FCMB_CONNAISS_INFO              BLOB segment 80, subtype TEXT Nullable 
FCMB_CONNAISS_AUTRES            BLOB segment 80, subtype TEXT Nullable 
FCMB_ENTREETDF                  TIMESTAMP Nullable 
FCMB_ENTREEVILLE                TIMESTAMP Nullable 
FCMB_PROJET_N1                  BLOB segment 80, subtype TEXT Nullable 
PERMIS_NB_PLACE                 INTEGER Nullable 
FCMB_OBJ_COMPAGNON              INTEGER Nullable 
FCMB_OBJ_FORMATION              INTEGER Nullable 
FCMB_OBJ_FOR_PER                VARCHAR(7) Nullable 
FCMB_OBJ_AUTRES                 VARCHAR(120) Nullable 
COT_NATION                      VARCHAR(1) Nullable 
COT_REGION                      VARCHAR(1) Nullable 
FCMB_ID_NAT                     VARCHAR(14) Nullable 
FCMB_RECRUT_INIT                INTEGER Nullable 
IDNET                           VARCHAR(50) Nullable 
CLECPTWEB                       VARCHAR(50) Nullable 
NOPROSPECT                      INTEGER Nullable 
PROSPECTDTACC                   TIMESTAMP Nullable 
PROSPECTDTBASC                  TIMESTAMP Nullable 
PROSPECTETBACC                  VARCHAR(44) Nullable 
SL_INFO_JEUNE_RELOGE            VARCHAR(1) Nullable 
SL_PARC_LOGT_RELOGE             VARCHAR(24) Nullable 
SL_TYP_LOGT_RELOGE              VARCHAR(24) Nullable 
SL_REFERENT                     VARCHAR(24) Nullable 
SL_NO_REF_PRESCRIPTEUR          INTEGER Nullable 
COMPTE_ANA                      VARCHAR(20) Nullable 
SL_ORIENTE_PAR                  VARCHAR(96) Nullable 
PRARCHIVE                       VARCHAR(1) Nullable 
NO_LOGT                         INTEGER Nullable 
SEPA_RUM                        VARCHAR(35) Nullable 
SEPA_DTSIGN                     TIMESTAMP Nullable 
SEPA_TYPMT                      VARCHAR(1) Nullable 
IBAN                            VARCHAR(34) Nullable 
BIC                             VARCHAR(11) Nullable 
CDPAYS                          VARCHAR(2) Nullable 
SEPA_FIRST                      VARCHAR(1) Nullable 
DAL                             VARCHAR(1) Nullable 
NO_PROSPECT_STATUT              INTEGER Nullable 
E_MAIL                          VARCHAR(80) Nullable 
ITI_IDLOCOMOTION                INTEGER Nullable 
ITI_DTCREATION                  TIMESTAMP Nullable 
ITI_DTSAISIE                    TIMESTAMP Nullable 
ITI_IDSOCIETE                   INTEGER Nullable 
ITI_IDMETIER                    INTEGER Nullable 
ITI_IDVILLE                     INTEGER Nullable 
ITI_IDCAMPAGNE                  INTEGER Nullable 
ITI_TERMINALEIEF                VARCHAR(1) Nullable 
ITI_JEUNE1EREVILLE              VARCHAR(1) Nullable 
ITI_DTENTREETDF                 TIMESTAMP Nullable 
ITI_DTDELIVRCARNET              TIMESTAMP Nullable 
ITI_IDVILLESUIV                 INTEGER Nullable 
ITI_RECTRSFT_IDSOCIETE          INTEGER Nullable 
ITI_RECTRSFT_IDRECRUT           INTEGER Nullable 
ITI_RECTRSFT_IDVILLE_RECRUT     INTEGER Nullable 
ITI_RECTRSFT_IDVILLE_VOEU1      INTEGER Nullable 
ITI_RECTRSFT_IDVILLE_VOEU2      INTEGER Nullable 
ITI_RECTRSFT_IDVILLE_VOEU3      INTEGER Nullable 
ITI_RECTRSFT_IDVILLE_SUIV       INTEGER Nullable 
ITI_RECTRSFT_IDCAMPAGNE         INTEGER Nullable 
ITI_RECTRSFT_DT_ARRIVPREVUE     TIMESTAMP Nullable 
ITI_ETATCOMP_JEUNE              VARCHAR(4) Nullable 
ITI_ETATCOMP_ASPAFFJH           VARCHAR(4) Nullable 
ITI_ETATCOMP_COMPAGNON          VARCHAR(4) Nullable 
ITI_NOM_COMPAGNON               VARCHAR(80) Nullable 
ITI_CONVCOLL                    INTEGER Nullable 
ITI_COMPETENCESPRO              BLOB segment 80, subtype TEXT Nullable 
ITI_COMPRO_LANGUE1              INTEGER Nullable 
ITI_COMPRO_LANGUE2              INTEGER Nullable 
ITI_COMPRO_LANGUE3              INTEGER Nullable 
ITI_COMPRO_LANGUE4              INTEGER Nullable 
ITI_COMPRO_INFOB2I              VARCHAR(1) Nullable 
ITI_COMPRO_INFOWORD             INTEGER Nullable 
ITI_COMPRO_INFOEXCEL            INTEGER Nullable 
ITI_COMPRO_INFOPPOINT           INTEGER Nullable 
ITI_COMPRO_INFOOUTLOOK          INTEGER Nullable 
ITI_COMPRO_INFOAUTOCAD          INTEGER Nullable 
ITI_COMPRO_INFOCADWORK          INTEGER Nullable 
ITI_COMPRO_INFOSEMA             INTEGER Nullable 
ITI_COMPRO_INFOPAINT            INTEGER Nullable 
ITI_COMPRO_INFOSKETCHUP         INTEGER Nullable 
ITI_COMPRO_INFOPHOTOSHOP        INTEGER Nullable 
ITI_COMPRO_INFOILLUSTRATOR      INTEGER Nullable 
ITI_COMPRO_INFOAUTRES           VARCHAR(20) Nullable 
ITI_COMPETENCESAUTRES           BLOB segment 80, subtype TEXT Nullable 
ITI_OBJ_PLANCARRIERE            BLOB segment 80, subtype TEXT Nullable 
ITI_OBJ_FORMATION               BLOB segment 80, subtype TEXT Nullable 
ITI_OBJ_COMPAGNONNIQUE          BLOB segment 80, subtype TEXT Nullable 
ITI_OBJ_AUTRES                  BLOB segment 80, subtype TEXT Nullable 
ITI_DEPTRESIDORIGINE            VARCHAR(3) Nullable 
ITI_RECTRSFT_DEPT               VARCHAR(3) Nullable 
ITI_SAISON                      VARCHAR(9) Nullable 
EMAIL_CAU                       VARCHAR(80) Nullable 
CONSTRAINT INTEG_26:
  Primary key (NO_RESIDENT)

Triggers on Table RESIDENTS:
SET_NO_RESIDENT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : RESSOURCES
===================================================================================================================
NO_RESSOURCES                   INTEGER Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
DATE_DEB                        TIMESTAMP Nullable 
DATE_FIN                        TIMESTAMP Nullable 
ORGANISME                       VARCHAR(40) Nullable 
NO_TIERS                        INTEGER Nullable 
CONSERVE                        DOUBLE PRECISION Nullable 
PERIODE                         SMALLINT Nullable 
DANS_CALCULS                    VARCHAR(1) Nullable 
NO_RESIDENT                     INTEGER Nullable 

Triggers on Table RESSOURCES:
SET_NO_RESSOURCES, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : RNI
===================================================================================================================
NO_RNI                          INTEGER Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
CO_RESIDENT                     VARCHAR(1) Nullable 
ANNEE                           SMALLINT Nullable 
NO_RESIDENT                     INTEGER Nullable 

Triggers on Table RNI:
SET_NO_RNI, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SAL_CLIENTS
===================================================================================================================
NO_CLIENTS                      INTEGER Nullable 
NOM                             VARCHAR(35) Nullable 
PRENOM                          VARCHAR(30) Nullable 
ADRESSE1                        VARCHAR(30) Nullable 
ADRESSE2                        VARCHAR(30) Nullable 
CP                              VARCHAR(5) Nullable 
VILLE                           VARCHAR(30) Nullable 
TEL                             VARCHAR(15) Nullable 
FAX                             VARCHAR(15) Nullable 
EMAIL                           VARCHAR(30) Nullable 

Triggers on Table SAL_CLIENTS:
SET_NO_CLIENTS, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SAL_DISPOSITIONS
===================================================================================================================
NO_DISPOSITION                  INTEGER Nullable 
LIBELLE                         VARCHAR(96) Nullable 
===================================================================================================================
Table : SAL_PARAMS
===================================================================================================================
LAST_NO_RESA                    INTEGER Nullable 
COLOR_OCCUPE                    VARCHAR(10) Nullable 
COLOR_RESERVE                   VARCHAR(10) Nullable 
COLOR_DISPO                     VARCHAR(10) Nullable 
COLOR_ANNULE                    VARCHAR(10) Nullable 
LAST_NO_GROUP_MATERIEL          INTEGER Nullable 
LIEN_FOYER                      VARCHAR(1) Nullable 
LEGENDE_FIXE                    VARCHAR(1) Nullable 
NO_ETABL                        INTEGER Nullable 
===================================================================================================================
Table : SAL_PLANNING
===================================================================================================================
NO_PLANNING                     INTEGER Nullable 
NO_RESA                         INTEGER Nullable 
NO_SALLE                        INTEGER Nullable 
VALEUR                          VARCHAR(1) Nullable 
NO_CLIENT                       INTEGER Nullable 
DATE_DEB                        TIMESTAMP Nullable 
TRANCHE_DEB                     INTEGER Nullable 
DATE_FIN                        TIMESTAMP Nullable 
TRANCHE_FIN                     DOUBLE PRECISION Nullable 
MOTIF_ANNULE                    VARCHAR(200) Nullable 
NO_RESIDENT                     INTEGER Nullable 
COMMENTAIRE                     BLOB segment 80, subtype TEXT Nullable 
TYPE_TARIF                      VARCHAR(1) Nullable 
INFOSPRESTA                     BLOB segment 80, subtype TEXT Nullable 
NO_DISPOSITION                  INTEGER Nullable 

Triggers on Table SAL_PLANNING:
SET_NO_PLANNING, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SAL_SALLES
===================================================================================================================
NO_SALLE                        INTEGER Nullable 
LIBELLE                         VARCHAR(60) Nullable 
SUPERFICIE                      DOUBLE PRECISION Nullable 
NB_PERS_REUNION                 INTEGER Nullable 
NB_PERS_CONF                    INTEGER Nullable 
TARIF_REUNION                   DOUBLE PRECISION Nullable 
TARIF_CONF                      DOUBLE PRECISION Nullable 
NO_SALLE_JOINT                  INTEGER Nullable 
TYP                             VARCHAR(1) Nullable 
NO_GROUP_MATERIEL               INTEGER Nullable 
UTILISER                        VARCHAR(1) Nullable 
NO_SAL_TYPE                     INTEGER Nullable 

Triggers on Table SAL_SALLES:
SET_NO_SALLE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SAL_TRANCHES
===================================================================================================================
NO_TRANCHE                      INTEGER Nullable 
LIBELLE                         VARCHAR(20) Nullable 
===================================================================================================================
Table : SAL_TYPES
===================================================================================================================
NO_SAL_TYPE                     INTEGER Nullable 
LIBELLE                         VARCHAR(96) Nullable 
NO_PRESTA                       INTEGER Nullable 

Triggers on Table SAL_TYPES:
SET_NO_SAL_TYPE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SAL_VF
===================================================================================================================
VALEUR                          VARCHAR(1) Nullable 
TEXTE                           VARCHAR(10) Nullable 
===================================================================================================================
Table : SECTEURS
===================================================================================================================
NO_SECTEUR                      INTEGER Nullable 
LIBELLE                         VARCHAR(80) Nullable 

Triggers on Table SECTEURS:
SET_NO_SECTEUR, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SEJOURS
===================================================================================================================
NO_SEJOUR                       INTEGER Nullable 
DATE_DEB                        TIMESTAMP Nullable 
DATE_FIN                        TIMESTAMP Nullable 
EN_COURS                        VARCHAR(1) Nullable 
CLOTUREE                        VARCHAR(1) Nullable 
DATE_SAISIE                     TIMESTAMP Nullable 
DATE_FIN_FACTURATION            TIMESTAMP Nullable 
DATE_FIN_PREVUE                 TIMESTAMP Nullable 
HEB_ANT                         INTEGER Nullable 
ORG_GEO                         INTEGER Nullable 
MOTIVATION                      INTEGER Nullable 
NB_CHANGEMENT                   INTEGER Nullable 
FORMATION                       INTEGER Nullable 
CHOMAGE                         VARCHAR(1) Nullable 
PROF_ENTREE                     INTEGER Nullable 
RESS_ENTREE                     INTEGER Nullable 
DIPLOME                         INTEGER Nullable 
MOTIF_DEP                       INTEGER Nullable 
LOGT_SORTIE                     INTEGER Nullable 
RESS_SORTIE                     INTEGER Nullable 
LIEU_DEPART                     INTEGER Nullable 
PROF_SORTIE                     INTEGER Nullable 
NOM_PR                          VARCHAR(40) Nullable 
SIGLE_PR                        VARCHAR(40) Nullable 
MEMO_PR                         BLOB segment 80, subtype TEXT Nullable 
SECTEUR_ACT                     INTEGER Nullable 
NOM_EMP                         VARCHAR(40) Nullable 
MEMO_EMP                        BLOB segment 80, subtype TEXT Nullable 
NOM_PA                          VARCHAR(40) Nullable 
MEMO_PA                         BLOB segment 80, subtype TEXT Nullable 
PROF_PERE                       INTEGER Nullable 
PROF_MERE                       INTEGER Nullable 
NBRE_ENFANTS                    INTEGER Nullable 
TYPE_RESS_E1                    INTEGER Nullable 
TYPE_RESS_E2                    INTEGER Nullable 
TYPE_RESS_E3                    INTEGER Nullable 
TYPE_RESS_S1                    INTEGER Nullable 
TYPE_RESS_S2                    INTEGER Nullable 
TYPE_RESS_S3                    INTEGER Nullable 
AUTRE_DIPLOME                   BLOB segment 80, subtype TEXT Nullable 
DATE_ARRIVEE                    TIMESTAMP Nullable 
TEL_PA2                         VARCHAR(15) Nullable 
TEL_PR                          VARCHAR(15) Nullable 
TEL_EMP                         VARCHAR(15) Nullable 
TEL_PA                          VARCHAR(15) Nullable 
TUTEUR_PA1                      VARCHAR(1) Nullable 
TUTEUR_PA2                      VARCHAR(1) Nullable 
PASSAGER                        VARCHAR(1) Nullable 
DATE_ANNULATION                 TIMESTAMP Nullable 
MOTIF_ANNULATION                BLOB segment 80, subtype TEXT Nullable 
DATE_LIMITE                     TIMESTAMP Nullable 
MT_RESS_E1                      DOUBLE PRECISION Nullable 
MT_RESS_E2                      DOUBLE PRECISION Nullable 
MT_RESS_E3                      DOUBLE PRECISION Nullable 
MT_RESS_S1                      DOUBLE PRECISION Nullable 
MT_RESS_S2                      DOUBLE PRECISION Nullable 
MT_RESS_S3                      DOUBLE PRECISION Nullable 
NOM_SEJOUR                      VARCHAR(40) Nullable 
CONFIRME                        VARCHAR(1) Nullable 
NB_PERSONNES                    INTEGER Nullable 
GR_FACTURE                      VARCHAR(1) Nullable 
COLOCATION                      VARCHAR(1) Nullable 
NO_MT_REVENUS                   INTEGER Nullable 
NO_CH_LOGT_FJT                  INTEGER Nullable 
NO_ACTIVITE_PROF_PERE           INTEGER Nullable 
NO_ACTIVITE_PROF_MERE           INTEGER Nullable 
NO_SEJ_REF                      INTEGER Nullable 
DT_FIN_FRACT                    TIMESTAMP Nullable 
SEJ_FRACT                       VARCHAR(1) Nullable 
NO_RESIDENT                     INTEGER Nullable 
NOM_PA2                         VARCHAR(40) Nullable 
NO_CONTRAT_AIDE                 INTEGER Nullable 
NO_SEJOUR_ALT                   INTEGER Nullable 
NO_RESA                         INTEGER Nullable 

Triggers on Table SEJOURS:
SET_NO_SEJOUR, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SITUATIONS_PROF
===================================================================================================================
NO_SITU                         INTEGER Nullable 
LIBELLE                         VARCHAR(50) Nullable 
LIB                             VARCHAR(255) Nullable 
ACTIVITE                        INTEGER Nullable 
CD_AN                           INTEGER Nullable 
SAISIE                          VARCHAR(1) Nullable 
CODE                            VARCHAR(1) Nullable 
FAMILLE                         VARCHAR(1) Nullable 

Triggers on Table SITUATIONS_PROF:
SET_NO_SITU, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_ACTIVITEFORM
===================================================================================================================
NO_ACTIVFORM                    INTEGER Nullable 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_ACTIVITEFORM:
SET_SLNOACTIVFORM, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_DEREXPPRO
===================================================================================================================
NO_DEREXPPRO                    INTEGER Nullable 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_DEREXPPRO:
SET_SLNODEREXPPRO, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_DURABILITE
===================================================================================================================
NO_DURABILITE                   INTEGER Nullable 
LIBELLE                         VARCHAR(24) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_DURABILITE:
SET_SLNODURABILITE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_EMPLOYEUR
===================================================================================================================
NO_SLEMPLOYEUR                  INTEGER Nullable 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_EMPLOYEUR:
SET_NOSLEMPLOYEUR, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_LIEU
===================================================================================================================
NO_LIEU                         INTEGER Nullable 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_LIEU:
SET_SLNOLIEU, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_MOTIVSITUA
===================================================================================================================
NO_MOTIVSITUA                   INTEGER Nullable 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_MOTIVSITUA:
SET_SLNOMOTIVSITUA, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_MOYDPLCT
===================================================================================================================
NO_MOYDPLCT                     INTEGER Nullable 
LIBELLE                         VARCHAR(24) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_MOYDPLCT:
SET_SLNOMOYDPLCT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_NATUREAIDE
===================================================================================================================
NO_NATUREAIDE                   INTEGER Nullable 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_NATUREAIDE:
SET_SLNONATUREAIDE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_NIVFORMATION
===================================================================================================================
NO_NIVFORM                      INTEGER Nullable 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_NIVFORMATION:
SET_SLNONIVFORMAT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_ORIGRESSOURCES
===================================================================================================================
NO_ORIGRESS                     INTEGER Nullable 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_ORIGRESSOURCES:
SET_SLNOORIGRESS, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_PARCLOGT
===================================================================================================================
NO_PARCLOGT                     INTEGER Nullable 
LIBELLE                         VARCHAR(24) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_PARCLOGT:
SET_SLNOPARCLOGT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_REVENUS
===================================================================================================================
NO_REVENU                       INTEGER Nullable 
LIBELLE                         VARCHAR(24) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_REVENUS:
SET_SLNOREVENU, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_SITUAFAMIL
===================================================================================================================
NO_SITUAFAM                     INTEGER Nullable 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_SITUAFAMIL:
SET_SLNOSITUAFAM, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_SOLUTION
===================================================================================================================
NO_SOLUTION                     INTEGER Nullable 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_SOLUTION:
SET_SLNOSOLUTION, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_SPECIFICITE
===================================================================================================================
NO_SPECIFICITE                  INTEGER Nullable 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_SPECIFICITE:
SET_SLNOSPECIFICITE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_STATUT
===================================================================================================================
NO_STATUT                       INTEGER Nullable 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_STATUT:
SET_SLNOSTATUT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_STATUTCPLT
===================================================================================================================
NO_STATUTCPLT                   INTEGER Nullable 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_STATUTCPLT:
SET_SLNOSTATUTCPLT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_TYPCONTACT
===================================================================================================================
NO_TYPCONTACT                   INTEGER Nullable 
LIBELLE                         VARCHAR(24) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_TYPCONTACT:
SET_SLNOTYPCONTACT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_TYPEAIDE
===================================================================================================================
NO_TYPEAIDE                     INTEGER Nullable 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 
FINANCIER                       VARCHAR(3) Nullable 

Triggers on Table SLCHL_TYPEAIDE:
SET_SLNOTYPEAIDE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_TYPLOGT
===================================================================================================================
NO_TYPLOGT                      INTEGER Nullable 
LIBELLE                         VARCHAR(24) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_TYPLOGT:
SET_SLNOTYPLOGT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SLCHL_TYPLOGTADEM
===================================================================================================================
NO_TYPLOGTADEM                  INTEGER Nullable 
LIBELLE                         VARCHAR(48) Nullable 
TRI                             INTEGER Nullable 

Triggers on Table SLCHL_TYPLOGTADEM:
SET_SLNOTYPLOGTADEM, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SL_REF_PRESCRIPTEUR
===================================================================================================================
NO_REF_PRESCRIPT                INTEGER Nullable 
NOM                             VARCHAR(48) Nullable 
TEL                             VARCHAR(15) Nullable 
MOB                             VARCHAR(15) Nullable 
CMPLT                           VARCHAR(48) Nullable 

Triggers on Table SL_REF_PRESCRIPTEUR:
SET_SLNOREFPRESCRIPT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SL_SITUA_JEUNE
===================================================================================================================
NO_SITUAJ                       INTEGER Nullable 
ID_JEUNE                        INTEGER Nullable 
DT_SITUA_ADM                    TIMESTAMP Nullable 
SITUA_FAMILIALE                 VARCHAR(48) Nullable 
NOM_CJ                          VARCHAR(48) Nullable 
PRENOM_CJ                       VARCHAR(48) Nullable 
DT_NAISS_CJ                     TIMESTAMP Nullable 
NB_ENFANTS                      INTEGER Nullable 
NIV_FORMATION                   VARCHAR(48) Nullable 
STATU_PRINCIPAL                 VARCHAR(48) Nullable 
TEMPS_PLEIN                     VARCHAR(1) Nullable 
DERN_EXP_PROF                   VARCHAR(48) Nullable 
ACTIVITEJ                       VARCHAR(48) Nullable 
REVENUS_MENSUEL                 VARCHAR(24) Nullable 
ORIG_RESSOURCES                 VARCHAR(48) Nullable 
TYP_RESIDENCE                   VARCHAR(48) Nullable 
COLOCATION_EC                   VARCHAR(1) Nullable 
EU_LOGT_AUTONOME                VARCHAR(1) Nullable 
TYP_LOGT_RECH                   VARCHAR(24) Nullable 
PARC_RECH                       VARCHAR(24) Nullable 
MOY_DPLCT                       VARCHAR(24) Nullable 
OBSERVATION                     BLOB segment 80, subtype TEXT Nullable 
REFERENT                        VARCHAR(24) Nullable 
NATURE_AIDE                     VARCHAR(48) Nullable 
CDI                             VARCHAR(1) Nullable 
TYP_LOGT_RECH2                  VARCHAR(24) Nullable 
PARC_RECH2                      VARCHAR(24) Nullable 
COLOC_RECH                      VARCHAR(1) Nullable 
REVENUS_MENAGE                  DOUBLE PRECISION Nullable 
MEUBLE_RECH                     VARCHAR(1) Nullable 
SITUACTIVE                      VARCHAR(1) Nullable 
NO_CONJOINT_RES                 INTEGER Nullable 
MOTIF                           VARCHAR(48) Nullable 
DURABILITE                      VARCHAR(24) Nullable 
RELOGTTYP                       VARCHAR(24) Nullable 
RELOGTPARC                      VARCHAR(24) Nullable 
NO_DUPLICATA                    INTEGER Nullable 
RELOGTINFO                      VARCHAR(1) Nullable 
LIEU_PROVENANCE                 VARCHAR(48) Nullable 
STATU_CPLT                      VARCHAR(48) Nullable 
ORIG_RESSOURCES2                VARCHAR(48) Nullable 
VIDE_RECH                       VARCHAR(1) Nullable 
LIEU_RECH1                      VARCHAR(48) Nullable 
LIEU_RECH2                      VARCHAR(48) Nullable 
LIEU_RECH3                      VARCHAR(48) Nullable 
RELOGTLIEU                      VARCHAR(48) Nullable 
RELOGTMEUBLE                    VARCHAR(1) Nullable 
RELOGTVIDE                      VARCHAR(1) Nullable 
RELOGTCOLOC                     VARCHAR(1) Nullable 
RELOGTSPEC                      VARCHAR(48) Nullable 
EMPLOYEUR_NOM                   VARCHAR(48) Nullable 
EMPLOYEUR_LG1                   VARCHAR(48) Nullable 
EMPLOYEUR_LG2                   VARCHAR(48) Nullable 
EMPLOYEUR_CDP                   VARCHAR(12) Nullable 
EMPLOYEUR_VIL                   VARCHAR(48) Nullable 

Triggers on Table SL_SITUA_JEUNE:
SET_SLNOSITUAJ, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SL_SS_SERVICE
===================================================================================================================
NO_SS_SERVICE                   INTEGER Nullable 
LIBELLE                         VARCHAR(96) Nullable 
TRI                             INTEGER Nullable 
SIGLE                           VARCHAR(24) Nullable 
DT_PER                          VARCHAR(15) Nullable 

Triggers on Table SL_SS_SERVICE:
SET_SLNOSSSERVICE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SL_SUIVI_JEUNE
===================================================================================================================
NO_SUIVIJ                       INTEGER Nullable 
NO_SS_SERVICE                   INTEGER Nullable 
ID_JEUNE                        INTEGER Nullable 
TYP_CONTACT                     VARCHAR(24) Nullable 
DUREE_RDV                       INTEGER Nullable 
DT_AFFECT                       TIMESTAMP Nullable 
DT_AFFECT_FIN                   TIMESTAMP Nullable 
REFERENT                        VARCHAR(24) Nullable 
ABSENT                          VARCHAR(1) Nullable 
ABS_EXCUSE                      VARCHAR(1) Nullable 
COMMENTAIRE                     BLOB segment 80, subtype TEXT Nullable 
MEMO_AFFICHE                    BLOB segment 80, subtype TEXT Nullable 
NATURE                          VARCHAR(1) Nullable 
MTDEMANDE                       DOUBLE PRECISION Nullable 
DTACCORD                        TIMESTAMP Nullable 
MTACCORDE                       DOUBLE PRECISION Nullable 
TYPE_AIDE                       VARCHAR(48) Nullable 
DTDEMANDE                       TIMESTAMP Nullable 
SOLUTION                        VARCHAR(48) Nullable 
SUIVACTIVE                      VARCHAR(1) Nullable 
NO_DUPLICATA                    INTEGER Nullable 
DTSOLUTION                      TIMESTAMP Nullable 

Triggers on Table SL_SUIVI_JEUNE:
SET_SLNOSUIVIJ, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : SOLDE_AIDES_CAF
===================================================================================================================
NO_SOLDE_AIDE                   INTEGER Nullable 
NOM                             VARCHAR(60) Nullable 
NO_FACTURE                      VARCHAR(15) Nullable 
NO_LIGNE                        INTEGER Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
MONNAIE                         VARCHAR(1) Nullable 
DATE_SOLDE                      TIMESTAMP Nullable 

Triggers on Table SOLDE_AIDES_CAF:
SET_NO_SOLDE_AIDE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : TAUX_TVA
===================================================================================================================
TAUX_TVA                        INTEGER Nullable 
VALEUR                          DOUBLE PRECISION Nullable 

Triggers on Table TAUX_TVA:
SET_TAUX_TVA, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : THEB_ANT
===================================================================================================================
NO_THEB                         INTEGER Nullable 
LIBELLE                         VARCHAR(50) Nullable 
LIB                             VARCHAR(255) Nullable 
CD_AN                           INTEGER Nullable 
SAISIE                          VARCHAR(1) Nullable 
CODE                            VARCHAR(1) Nullable 
FAMILLE                         VARCHAR(1) Nullable 

Triggers on Table THEB_ANT:
SET_NO_THEB, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : TIERS
===================================================================================================================
NO_TIERS                        INTEGER Nullable 
NOM                             VARCHAR(40) Nullable 
PRENOM                          VARCHAR(40) Nullable 
COMPTE_ACP                      VARCHAR(10) Nullable 
NOM_PP                          VARCHAR(40) Nullable 
PRENOM_PP                       VARCHAR(40) Nullable 
ADRESSE_DIFF                    VARCHAR(1) Nullable 
NO_ETABL                        INTEGER Nullable 
UNE_FACT_PR                     INTEGER Nullable 
TEL                             VARCHAR(15) Nullable 
TEL_PP                          VARCHAR(15) Nullable 
FAX                             VARCHAR(15) Nullable 
FAX_PP                          VARCHAR(15) Nullable 
DELAI_PAIE                      INTEGER Nullable 
SEXE                            VARCHAR(1) Nullable 
COMPTE_INDU                     VARCHAR(15) Nullable 
ETABL                           VARCHAR(5) Nullable 
GUICHET                         VARCHAR(5) Nullable 
REF_COMPTE                      VARCHAR(11) Nullable 
CLE_RICE                        VARCHAR(2) Nullable 
DOMICILIATION                   VARCHAR(40) Nullable 
VIREMENT                        VARCHAR(1) Nullable 
COMPTE                          VARCHAR(20) Nullable 
CRITERE_1                       VARCHAR(48) Nullable 
CRITERE_2                       VARCHAR(48) Nullable 
CRITERE_3                       VARCHAR(48) Nullable 
CRITERE_4                       VARCHAR(48) Nullable 
T_FAC                           VARCHAR(1) Nullable 
T_EMP                           VARCHAR(1) Nullable 
T_PARTENAIRE                    VARCHAR(1) Nullable 
T_COMPSED                       VARCHAR(1) Nullable 
T_CA                            VARCHAR(1) Nullable 
TEL_MOBILE                      VARCHAR(15) Nullable 
TEL_MOBILE_PP                   VARCHAR(15) Nullable 
FORUM                           VARCHAR(1) Nullable 
MEMO_PP                         BLOB segment 80, subtype TEXT Nullable 
APE_NAF                         VARCHAR(5) Nullable 
SIRET                           VARCHAR(17) Nullable 
METIER                          INTEGER Nullable 
TUTEUR                          VARCHAR(254) Nullable 
PARTACTIVITE                    VARCHAR(80) Nullable 
NB_SAL                          INTEGER Nullable 
TAXAPPR                         VARCHAR(1) Nullable 
TYPEPARTENAIRE                  INTEGER Nullable 
ABT_JOURNAL                     INTEGER Nullable 
COTISANT                        INTEGER Nullable 
COT_NATION                      VARCHAR(1) Nullable 
COT_REGION                      VARCHAR(1) Nullable 
IDNET                           VARCHAR(50) Nullable 
T_LOGT                          VARCHAR(1) Nullable 
COMPTE_ANA                      VARCHAR(20) Nullable 
CLECPTWEB                       VARCHAR(50) Nullable 
SEPA_RUM                        VARCHAR(35) Nullable 
SEPA_DTSIGN                     TIMESTAMP Nullable 
SEPA_TYPMT                      VARCHAR(1) Nullable 
IBAN                            VARCHAR(34) Nullable 
BIC                             VARCHAR(11) Nullable 
CDPAYS                          VARCHAR(2) Nullable 
TEL2                            VARCHAR(15) Nullable 
TEL_MOBILE2                     VARCHAR(15) Nullable 
TEL_MOBILE3                     VARCHAR(15) Nullable 
ACCORDDIFFNET                   VARCHAR(1) Nullable 
ACCORDPHOTONET                  VARCHAR(1) Nullable 
SEPA_FIRST                      VARCHAR(1) Nullable 
E_MAIL                          VARCHAR(80) Nullable 
E_MAIL_PP                       VARCHAR(80) Nullable 

Triggers on Table TIERS:
SET_NO_TIERS, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : TLOG_SORT
===================================================================================================================
NO_TLOGT                        INTEGER Nullable 
LIBELLE                         VARCHAR(50) Nullable 
LIB                             VARCHAR(255) Nullable 
CD_AN                           INTEGER Nullable 
SAISIE                          VARCHAR(1) Nullable 
CODE                            VARCHAR(1) Nullable 
FAMILLE                         VARCHAR(1) Nullable 

Triggers on Table TLOG_SORT:
SET_NO_TLOGT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : TYPES_ABS
===================================================================================================================
NO_MOTIF                        INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 

Triggers on Table TYPES_ABS:
SET_NO_MOTIF_ABS, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : TYPES_AIDES
===================================================================================================================
NO_TYPE_AIDE                    INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
PAR_MOIS                        VARCHAR(1) Nullable 
MOIS_COMPLET                    VARCHAR(1) Nullable 
PAR_BAREME                      VARCHAR(1) Nullable 
PREVISIONNELLE                  VARCHAR(1) Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
PAR_JOUR                        VARCHAR(1) Nullable 
DECALAGE                        SMALLINT Nullable 
DUREE                           SMALLINT Nullable 
ARRIVEE                         VARCHAR(1) Nullable 
AUTOMATIQUE                     VARCHAR(1) Nullable 
VERSE_PAR                       INTEGER Nullable 
RESTRICTIONS                    VARCHAR(1) Nullable 
GIR_1                           VARCHAR(1) Nullable 
GIR_2                           VARCHAR(1) Nullable 
GIR_3                           VARCHAR(1) Nullable 
GIR_4                           VARCHAR(1) Nullable 
GIR_5                           VARCHAR(1) Nullable 
GIR_6                           VARCHAR(1) Nullable 
SELON_GIR                       VARCHAR(1) Nullable 
VALIDE_AP                       VARCHAR(1) Nullable 
PERPETUEL                       VARCHAR(1) Nullable 
COMPTE                          VARCHAR(15) Nullable 
JL                              VARCHAR(10) Nullable 
COMPTE_REMB                     VARCHAR(15) Nullable 
JL_REMB                         VARCHAR(10) Nullable 
T_TYPE                          VARCHAR(1) Nullable 
TYPE_APL                        VARCHAR(20) Nullable 
COMPTE_ANA                      VARCHAR(20) Nullable 

Triggers on Table TYPES_AIDES:
SET_NO_TYPE_AIDE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : TYPES_CONTRAT
===================================================================================================================
NO_TYPE_CONTRAT                 INTEGER Nullable 
TYP_CONTRAT                     VARCHAR(40) Nullable 

Triggers on Table TYPES_CONTRAT:
SET_NOTYPECONTRAT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : TYPES_DG
===================================================================================================================
NO_TYPE_DG                      INTEGER Nullable 
COMPTE                          VARCHAR(15) Nullable 
LIBELLE                         VARCHAR(40) Nullable 
JL                              VARCHAR(10) Nullable 
COMPTE_ANA                      VARCHAR(20) Nullable 
NO_ETABL                        INTEGER Nullable 

Triggers on Table TYPES_DG:
SET_NO_TYPE_DG, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : TYPES_EVENEMENTS
===================================================================================================================
NO_TYPE_EVENEMENT               INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
NO_PRESTATION                   INTEGER Nullable 

Triggers on Table TYPES_EVENEMENTS:
SET_NO_TYPE_EVENEMENT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : TYPES_INTERV
===================================================================================================================
NO_TYPE                         INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 

Triggers on Table TYPES_INTERV:
SET_NO_TYPE_INTERV, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : TYPES_INTERVENTIONS
===================================================================================================================
NO_TYPE                         INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
MONTANT_DEF                     DOUBLE PRECISION Nullable 

Triggers on Table TYPES_INTERVENTIONS:
SET_NO_TYPE_INTERVENTIONS, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : TYPES_LOGT
===================================================================================================================
NO_TYPE                         INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
SURFACE                         DOUBLE PRECISION Nullable 

Triggers on Table TYPES_LOGT:
SET_NO_TYPES_LOGT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : TYPES_PRESTA
===================================================================================================================
NO_TYPE_PRESTA                  INTEGER Nullable 
LIBELLE                         VARCHAR(30) Nullable 

Triggers on Table TYPES_PRESTA:
SET_NO_TYPE_PRESTA, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : TYPES_REG
===================================================================================================================
NO_TYPE_REG                     INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
BANQUE_RATTACHEE                VARCHAR(40) Nullable 
CAISSE                          INTEGER Nullable 
INITIAL                         DOUBLE PRECISION Nullable 
GLOBALISE                       VARCHAR(1) Nullable 
TXT_SUR_RECU                    VARCHAR(150) Nullable 
TPE                             VARCHAR(1) Nullable 

Triggers on Table TYPES_REG:
SET_NO_TYPE_REG, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : TYPES_RES
===================================================================================================================
NO_TYPE                         INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 

Triggers on Table TYPES_RES:
SET_NO_TYPE_RES, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : TYPES_RESSOURCES
===================================================================================================================
NO_TYPE                         INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
LIB                             VARCHAR(255) Nullable 
CD_AN                           INTEGER Nullable 
SAISIE                          VARCHAR(1) Nullable 

Triggers on Table TYPES_RESSOURCES:
SET_NO_TYPE_RESSOURCES, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : TYPES_SITUATIONS
===================================================================================================================
NO_SITUATION                    INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 

Triggers on Table TYPES_SITUATIONS:
SET_NO_TYPE_SITUATION, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : USERS
===================================================================================================================
NO_USER                         INTEGER Not Null 
IDENTIFIANT                     VARCHAR(5) Nullable 
ID_CAISSE                       VARCHAR(5) Nullable 
ADMINCAISSE                     VARCHAR(1) Nullable 
NOM_USER                        VARCHAR(18) Nullable 
MOT_PASSE                       VARCHAR(18) Nullable 
CHANGEMDP                       VARCHAR(1) Nullable 
CONSTRAINT INTEG_28:
  Primary key (NO_USER)

Triggers on Table USERS:
SET_NO_USER, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : VENTE_DIRECTE
===================================================================================================================
NO_VENTE_DIRECTE                INTEGER Nullable 
LIBELLE_PRESTATION              VARCHAR(80) Nullable 
NO_PRESTATION                   INTEGER Nullable 
PU_HT                           DOUBLE PRECISION Nullable 
PU_TTC                          DOUBLE PRECISION Nullable 
TOTAL_HT                        DOUBLE PRECISION Nullable 
TOTAL_TTC                       DOUBLE PRECISION Nullable 
TVA                             DOUBLE PRECISION Nullable 
DATE_SAISIE                     TIMESTAMP Nullable 
COMPTA_LE                       TIMESTAMP Nullable 
MONNAIE                         VARCHAR(1) Nullable 
QUANTITE                        DOUBLE PRECISION Nullable 
NO_ETABL                        INTEGER Nullable 
NO_OPERATION_VENTE_DIRECTE      INTEGER Nullable 

Triggers on Table VENTE_DIRECTE:
SET_NO_VENTE_DIRECTE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : VENTILE_REGLT
===================================================================================================================
NO_VENTILE_R                    INTEGER Nullable 
MONNAIE                         CHAR(1) Nullable 
NO_FACTURE                      CHAR(15) Nullable 
NO_REMBT                        INTEGER Nullable 
COMPTA_LE                       TIMESTAMP Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
NO_PRESTATION                   INTEGER Nullable 
NO_CAUTION                      INTEGER Nullable 
NO_REGLT                        INTEGER Nullable 
NO_FACT                         INTEGER Nullable 
IMPAYE                          VARCHAR(1) Nullable 

Triggers on Table VENTILE_REGLT:
SET_NO_VENTILE_R, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : VENTILE_REMB
===================================================================================================================
NO_VENTILE_REMB                 INTEGER Nullable 
NO_FACTURE                      CHAR(15) Nullable 
NO_AIDE                         INTEGER Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
NO_REMB                         INTEGER Nullable 
NO_CAUTION                      INTEGER Nullable 
NO_REGLT                        INTEGER Nullable 
MONNAIE                         VARCHAR(1) Nullable 
NO_INDU                         INTEGER Nullable 
NO_FACT                         INTEGER Nullable 

Triggers on Table VENTILE_REMB:
SET_NO_VENTILE_REMB, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table : VERSION
===================================================================================================================
NO_VERSION                      INTEGER Nullable 
ACHETEUR                        VARCHAR(40) Nullable 
CODE                            VARCHAR(254) Nullable 
NBLITS                          INTEGER Nullable 
CONSULT                         INTEGER Nullable 
CDCLIENT                        VARCHAR(12) Nullable 
BLOCAGE                         VARCHAR(1) Nullable 
DTABO                           TIMESTAMP Nullable 
NBJRAV                          INTEGER Nullable 
NBJRBL                          INTEGER Nullable 
SUIVI_MTN                       VARCHAR(1) Nullable 
DE_VERSION                      INTEGER Nullable 
