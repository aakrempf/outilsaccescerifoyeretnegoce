===================================================================================================================
Liste des tables
===================================================================================================================
       ACCORDS                                ACCORDS_TIC                    
       ADHESIONS                              ARTICLES                       
       ARTICLES_TIC                           BANDEZ                         
       CAISSES                                CLAVIERS                       
       CLIENTS                                CLIENTS_SUPP                   
       CPTA_ART                               DET_ACCORD                     
       DET_ACCORD_TA                          ECRTACTILE                     
       INGREDIENTS                            LISTE_ARTICLES                 
       LISTE_TICKETS                          LST_CPTE_AVEC_HISTO            
       PARAMS                                 PLANNING                       
       RECETTES                               REGLT_TIC                      
       RESERVATIONS                           SERVICES                       
       TARIFS                                 TICKETS                        
       TOUCHES                                TRANSFERT                      
       TYPE_ADHESIONS                         TYPE_ARTICLES                  
       TYPE_CLIENTS                           TYPE_REGLT                     
       VERSION                                ZTBDETACC                      

===================================================================================================================
Table : ACCORDS
===================================================================================================================
NO_ACCORD                       INTEGER Nullable 
NOM                             VARCHAR(40) Nullable 
ACTIF                           VARCHAR(1) Nullable 

Triggers on Table ACCORDS:
SET_NO_ACCORD, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table ACCORDS_TIC
===================================================================================================================
NO_ACCORD_TIC                   INTEGER Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
NO_DET_ACCORD                   INTEGER Nullable 
LIBELLE                         VARCHAR(30) Nullable 
NO_TICKET                       INTEGER Nullable 
NOMBRE                          INTEGER Nullable 
LE                              TIMESTAMP Nullable 
NO_CLIENT                       INTEGER Nullable 
NO_FACTURE                      VARCHAR(15) Nullable 
RETOUR_RAMPE                    VARCHAR(1) Nullable 

Triggers on Table ACCORDS_TIC:
SET_NO_ACCORD_TIC, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table ADHESIONS
===================================================================================================================
NO_ADHESION                     INTEGER Nullable 
NO_TYPE_ADHESION                INTEGER Nullable 
PRIX                            DOUBLE PRECISION Nullable 
LE                              TIMESTAMP Nullable 
NO_CLIENT                       INTEGER Nullable 
DU                              TIMESTAMP Nullable 
AU                              TIMESTAMP Nullable 
NO_TICKET                       INTEGER Nullable 

Triggers on Table ADHESIONS:
SET_NO_ADHESION, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table ARTICLES
===================================================================================================================
NO_ARTICLE                      INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
CM                              VARCHAR(1) Nullable 
ACTIF                           VARCHAR(1) Nullable 
NO_TYPE_ARTICLES                INTEGER Nullable 
PRIX                            DOUBLE PRECISION Nullable 
MONNAIE                         VARCHAR(1) Nullable 
JL                              VARCHAR(10) Nullable 
RECETTE                         BLOB segment 80, subtype TEXT Nullable 
NB_PORTIONS                     INTEGER Nullable 
NO_TVA                          INTEGER Nullable 
COMPTEUR_CAISSE                 VARCHAR(1) Nullable 
MT_CAISSE                       VARCHAR(1) Nullable 
FORMULE                         VARCHAR(1) Nullable 
DIFFMT                          DOUBLE PRECISION Nullable 
PALIER                          INTEGER Nullable 
PRIXPLANCHER                    DOUBLE PRECISION Nullable 
EC_QTEDISPO                     INTEGER Nullable 
EC_QTECONSO                     INTEGER Nullable 

Triggers on Table ARTICLES:
SET_NO_ARTICLE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table ARTICLES_TIC
===================================================================================================================
NO_ARTICLE_TIC                  INTEGER Nullable 
NO_TICKET                       INTEGER Nullable 
NO_ARTICLE                      INTEGER Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
NOMBRE                          INTEGER Nullable 
NO_TYPE_ARTICLES                INTEGER Nullable 
LIBELLE                         VARCHAR(30) Nullable 
NO_PRESTA_RESIDENTS             INTEGER Nullable 
TYPE_CPTA                       VARCHAR(1) Nullable 
NO_TARIF                        INTEGER Nullable 
RETOUR_RAMPE                    VARCHAR(1) Nullable 
NOM_TRANSFERT                   VARCHAR(20) Nullable 
MT_U_HT                         DOUBLE PRECISION Nullable 

Triggers on Table ARTICLES_TIC:
SET_NO_ARTICLE_TIC, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table BANDEZ
===================================================================================================================
LIBELLE                         VARCHAR(40) Nullable 
SOMME                           DOUBLE PRECISION Nullable 
NOMBRE                          INTEGER Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
===================================================================================================================
Table CAISSES
===================================================================================================================
NO_CAISSE                       INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
ETAT                            VARCHAR(1) Nullable 
NO_CLAVIER                      INTEGER Nullable 
OUVERTURETACTILE                INTEGER Nullable 
ANTICIP                         VARCHAR(1) Nullable 

Triggers on Table CAISSES:
SET_NO_CAISSE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table CLAVIERS
===================================================================================================================
NO_CLAVIER                      INTEGER Nullable 
HAUTEUR                         INTEGER Nullable 
LARGEUR                         INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
LARGEUR_TOUCHE                  INTEGER Nullable 
ESPACE_LARGEUR_TOUCHE           INTEGER Nullable 
HAUTEUR_TOUCHE                  INTEGER Nullable 
ESPACE_HAUTEUR_TOUCHE           INTEGER Nullable 
IMP_COULEUR                     VARCHAR(1) Nullable 
TACTILE                         VARCHAR(1) Nullable 
NBLIGAFF                        INTEGER Nullable 
RETLIGNE                        INTEGER Nullable 
STYLE                           INTEGER Nullable 
TAILLE                          INTEGER Nullable 
COULEUR                         VARCHAR(10) Nullable 
COULEURFOND                     VARCHAR(10) Nullable 

Triggers on Table CLAVIERS:
SET_NO_CLAVIER, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table CLIENTS
===================================================================================================================
NO_CLIENT                       INTEGER Nullable 
BADGE                           VARCHAR(20) Nullable 
NO_TYPE_CLIENT                  INTEGER Nullable 
NO_ACCORD                       INTEGER Nullable 
RESIDENT                        VARCHAR(1) Nullable 
SOLDE                           DOUBLE PRECISION Nullable 
NOM                             VARCHAR(30) Nullable 
PRENOM                          VARCHAR(30) Nullable 
ADRESSE1                        VARCHAR(40) Nullable 
ADRESSE2                        VARCHAR(40) Nullable 
ADRESSE3                        VARCHAR(40) Nullable 
CP                              VARCHAR(5) Nullable 
VILLE                           VARCHAR(40) Nullable 
MEMO                            BLOB segment 80, subtype TEXT Nullable 
NO_RESIDENT                     INTEGER Nullable 
DATE_VALID                      TIMESTAMP Nullable 
SOLDE_C                         DOUBLE PRECISION Nullable 
DATE_C                          TIMESTAMP Nullable 
SOLDE_C_PROV                    DOUBLE PRECISION Nullable 
DATE_C_PROV                     TIMESTAMP Nullable 
FACTURER                        VARCHAR(1) Nullable 
MONTANT_FORFAIT                 DOUBLE PRECISION Nullable 
MONNAIE_FORFAIT                 VARCHAR(1) Nullable 
DEPASSEMENT                     VARCHAR(1) Nullable 
COMPTE                          VARCHAR(15) Nullable 
TRANSFERT                       VARCHAR(1) Nullable 
NO_PLANNING                     INTEGER Nullable 
AFF_COMPTEUR_CAISSE             VARCHAR(1) Nullable 
MNT_COMPTEUR                    DOUBLE PRECISION Nullable 
COURRIEL                        VARCHAR(80) Nullable 
TEL_FIXE                        VARCHAR(15) Nullable 
TEL_MOBILE                      VARCHAR(15) Nullable 
NE_LE                           TIMESTAMP Nullable 
CATEGORIE                       VARCHAR(40) Nullable 
EMPLOYEUR                       VARCHAR(40) Nullable 
SOLDE_INIT                      DOUBLE PRECISION Nullable 

Triggers on Table CLIENTS:
SET_NO_CLIENT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table CLIENTS_SUPP
===================================================================================================================
NO_CLIENT                       INTEGER Nullable 
BADGE                           VARCHAR(20) Nullable 
NO_TYPE_CLIENT                  INTEGER Nullable 
NO_ACCORD                       INTEGER Nullable 
RESIDENT                        VARCHAR(1) Nullable 
SOLDE                           DOUBLE PRECISION Nullable 
NOM                             VARCHAR(30) Nullable 
PRENOM                          VARCHAR(30) Nullable 
ADRESSE1                        VARCHAR(40) Nullable 
ADRESSE2                        VARCHAR(40) Nullable 
ADRESSE3                        VARCHAR(40) Nullable 
CP                              VARCHAR(5) Nullable 
VILLE                           VARCHAR(40) Nullable 
MEMO                            BLOB segment 80, subtype TEXT Nullable 
NO_RESIDENT                     INTEGER Nullable 
DATE_SUPP                       TIMESTAMP Nullable 
MNT_REMB                        DOUBLE PRECISION Nullable 
NO_TYPE_REGLT                   INTEGER Nullable 
SOLDE_C                         DOUBLE PRECISION Nullable 
DATE_C                          TIMESTAMP Nullable 
COMPTE                          VARCHAR(15) Nullable 
COMPTA_REMB_LE                  TIMESTAMP Nullable 
COURRIEL                        VARCHAR(80) Nullable 
TEL_FIXE                        VARCHAR(15) Nullable 
TEL_MOBILE                      VARCHAR(15) Nullable 
NE_LE                           TIMESTAMP Nullable 
CATEGORIE                       VARCHAR(40) Nullable 
EMPLOYEUR                       VARCHAR(40) Nullable 
SOLDE_INIT                      DOUBLE PRECISION Nullable 
===================================================================================================================
Table CPTA_ART
===================================================================================================================
NO_ARTICLE                      INTEGER Nullable 
NO_TYPE_CLIENT                  INTEGER Nullable 
TYPE_CPTA                       VARCHAR(1) Nullable 
COMPTE                          VARCHAR(15) Nullable 
CODE_ANA                        VARCHAR(15) Nullable 
REPARTITION                     DOUBLE PRECISION Nullable 
NO_CPTA_ART                     INTEGER Nullable 
NO_TARIF                        INTEGER Nullable 

Triggers on Table CPTA_ART:
SET_NO_CPTA_ART, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table DET_ACCORD
===================================================================================================================
NO_DET_ACCORD                   INTEGER Nullable 
NO_ACCORD                       INTEGER Nullable 
TYPE_ACCORD                     INTEGER Nullable 
LIBELLE                         VARCHAR(250) Nullable 
NO_TIERS                        INTEGER Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
MONNAIE                         VARCHAR(1) Nullable 
LIMITATION                      VARCHAR(1) Nullable 
NB_LIMIT                        INTEGER Nullable 
TYPE_LIMIT                      VARCHAR(1) Nullable 
PLANNING                        VARCHAR(1) Nullable 
L1                              VARCHAR(1) Nullable 
L2                              VARCHAR(1) Nullable 
L3                              VARCHAR(1) Nullable 
L4                              VARCHAR(1) Nullable 
L5                              VARCHAR(1) Nullable 
M1                              VARCHAR(1) Nullable 
M2                              VARCHAR(1) Nullable 
M3                              VARCHAR(1) Nullable 
M4                              VARCHAR(1) Nullable 
M5                              VARCHAR(1) Nullable 
E1                              VARCHAR(1) Nullable 
E2                              VARCHAR(1) Nullable 
E3                              VARCHAR(1) Nullable 
E4                              VARCHAR(1) Nullable 
E5                              VARCHAR(1) Nullable 
J1                              VARCHAR(1) Nullable 
J2                              VARCHAR(1) Nullable 
J3                              VARCHAR(1) Nullable 
J4                              VARCHAR(1) Nullable 
J5                              VARCHAR(1) Nullable 
V1                              VARCHAR(1) Nullable 
V2                              VARCHAR(1) Nullable 
V3                              VARCHAR(1) Nullable 
V4                              VARCHAR(1) Nullable 
V5                              VARCHAR(1) Nullable 
S1                              VARCHAR(1) Nullable 
S2                              VARCHAR(1) Nullable 
S3                              VARCHAR(1) Nullable 
S4                              VARCHAR(1) Nullable 
S5                              VARCHAR(1) Nullable 
D1                              VARCHAR(1) Nullable 
D2                              VARCHAR(1) Nullable 
D3                              VARCHAR(1) Nullable 
D4                              VARCHAR(1) Nullable 
D5                              VARCHAR(1) Nullable 
ACTIF                           VARCHAR(1) Nullable 
MONTANT2                        DOUBLE PRECISION Nullable 
MONNAIE2                        VARCHAR(1) Nullable 
RESUME                          BLOB segment 80, subtype TEXT Nullable 
TEXTE                           VARCHAR(30) Nullable 
NB_LIMIT_E                      DOUBLE PRECISION Nullable 
NO_TVA                          INTEGER Nullable 

Triggers on Table DET_ACCORD:
SET_NO_DET_ACCORD, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table DET_ACCORD_TA
===================================================================================================================
NO_DET_ACCORD                   INTEGER Nullable 
NO_TYPE_ARTICLES                INTEGER Nullable 
===================================================================================================================
Table ECRTACTILE
===================================================================================================================
NO_ECRTACTILE                   INTEGER Nullable 
NO_ECRAN                        INTEGER Nullable 
COLONNE                         INTEGER Nullable 
LIGNE                           INTEGER Nullable 
NO_ARTICLE                      INTEGER Nullable 
LIB_ECRAN                       VARCHAR(40) Nullable 
TAILLE                          INTEGER Nullable 
STYLE                           INTEGER Nullable 
COULEUR                         VARCHAR(10) Nullable 
COULEURFOND                     VARCHAR(10) Nullable 

Triggers on Table ECRTACTILE:
SET_NO_ECRTACTILE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table INGREDIENTS
===================================================================================================================
NO_INGREDIENT                   INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
TYPE_UNITE                      VARCHAR(20) Nullable 

Triggers on Table INGREDIENTS:
SET_NO_INGREDIENT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table LISTE_ARTICLES
===================================================================================================================
NO_ARTICLE                      INTEGER Nullable 
LIBELLE_ARTICLE                 VARCHAR(30) Nullable 
CM                              VARCHAR(1) Nullable 
ACTIF                           VARCHAR(1) Nullable 
PRIX                            DOUBLE PRECISION Nullable 
MONNAIE                         VARCHAR(1) Nullable 
JL                              VARCHAR(10) Nullable 
NO_TYPE_ARTICLES                INTEGER Nullable 
NO_TYPE_CLIENT                  INTEGER Nullable 
LIBELLE_TYPE_CLIENT             VARCHAR(30) Nullable 
NO_CPTA_ART                     INTEGER Nullable 
TYPE_CPTA                       VARCHAR(1) Nullable 
COMPTE                          VARCHAR(15) Nullable 
CODE_ANA                        VARCHAR(15) Nullable 
REPARTITION                     DOUBLE PRECISION Nullable 
NO_TARIF                        INTEGER Nullable 
===================================================================================================================
Table LISTE_TICKETS
===================================================================================================================
COL1                            INTEGER Nullable 
COL2                            VARCHAR(50) Nullable 
COL3                            DOUBLE PRECISION Nullable 
COL4                            VARCHAR(1) Nullable 
NO_TICKET                       INTEGER Nullable 
NOM                             VARCHAR(30) Nullable 
PRENOM                          VARCHAR(30) Nullable 
BADGE                           VARCHAR(20) Nullable 
RETOUR_RAMPE                    VARCHAR(1) Nullable 
TOT_A                           DOUBLE PRECISION Nullable 
TOT_C                           DOUBLE PRECISION Nullable 
TOT_R                           DOUBLE PRECISION Nullable 
===================================================================================================================
Table LST_CPTE_AVEC_HISTO
===================================================================================================================
NO_CLIENT                       INTEGER Nullable 
TYPE_CLIENT                     VARCHAR(50) Nullable 
NOM_CLIENT                      VARCHAR(30) Nullable 
PRENOM_CLIENT                   VARCHAR(30) Nullable 
BADGE                           VARCHAR(20) Nullable 
SOLDE                           DOUBLE PRECISION Nullable 
LA_DATE                         TIMESTAMP Nullable 
NBRE_REPAS                      INTEGER Nullable 
APPRO                           DOUBLE PRECISION Nullable 
SUBV                            DOUBLE PRECISION Nullable 
CONSO                           DOUBLE PRECISION Nullable 
SUPP                            VARCHAR(1) Nullable 
===================================================================================================================
Table PARAMS
===================================================================================================================
LIGNE1                          VARCHAR(40) Nullable 
LIGNE2                          VARCHAR(40) Nullable 
LIGNE3                          VARCHAR(40) Nullable 
LIGNE4                          VARCHAR(40) Nullable 
LIGNE5                          VARCHAR(40) Nullable 
LIGNE6                          VARCHAR(40) Nullable 
LARGEUR_TIC                     INTEGER Nullable 
GAUCHE                          INTEGER Nullable 
BAS                             INTEGER Nullable 
LARGEUR                         INTEGER Nullable 
SYMBOLE_EURO                    VARCHAR(1) Nullable 
DATE_CLOTURE                    TIMESTAMP Nullable 
DATE_CLOTURE_PROV               TIMESTAMP Nullable 
DATE_EDIT                       TIMESTAMP Nullable 
COMPTE_CLIENT                   VARCHAR(15) Nullable 
PREFIXE                         VARCHAR(30) Nullable 
SUFFIXE                         VARCHAR(30) Nullable 
MONNAIE                         VARCHAR(1) Nullable 
ANNULE_SUR_BANDEZ               VARCHAR(1) Nullable 
PREFIXE_PRESTA                  VARCHAR(30) Nullable 
RETOUR_RAMPE                    VARCHAR(1) Nullable 
NOM_TRANSFERT                   VARCHAR(30) Nullable 
PIED_UTIL_ADH                   VARCHAR(1) Nullable 
PIED_MESS_ADH                   VARCHAR(30) Nullable 
PIED_UTIL_SOLDE                 VARCHAR(1) Nullable 
PIED_MNT_SOLDE                  DOUBLE PRECISION Nullable 
PIED_MESS_SOLDE                 VARCHAR(30) Nullable 
PREMIER_BADGE                   VARCHAR(20) Nullable 
TICKET_RPM                      VARCHAR(40) Nullable 
ADHESIONS_EN_CAISSE             VARCHAR(1) Nullable 
DEDUIRE_ADHESION                VARCHAR(1) Nullable 
NB_ART_MAX                      INTEGER Nullable 
REGLT_MAX                       INTEGER Nullable 
COMPTE_CLIENT_DEFAUT            VARCHAR(15) Nullable 
BLOQUER_ADHESION_EN_CAISSE      VARCHAR(1) Nullable 
DELAIS_ADHESION_EN_CAISSE       INTEGER Nullable 
UTIL_RESA                       VARCHAR(1) Nullable 
UTIL_GROUPE                     VARCHAR(1) Nullable 
UTIL_COLOR                      VARCHAR(1) Nullable 
COMPTE_ARTICLE                  VARCHAR(15) Nullable 
ANA_ARTICLE                     VARCHAR(15) Nullable 
JOURNAL_ARTICLE                 VARCHAR(10) Nullable 
CD_PR                           VARCHAR(9) Nullable 
CD_UE                           VARCHAR(2) Nullable 
MT_FJT                          DOUBLE PRECISION Nullable 
NOM_FIC_TRSF                    VARCHAR(255) Nullable 
TYPE_LECTEUR                    INTEGER Nullable 
NBRE_CAR                        INTEGER Nullable 
COMPTE_TIERS_ACCORDS            VARCHAR(20) Nullable 
JOURNAL_ACCORDS                 VARCHAR(12) Nullable 
REGLT_TRANSF_CONSO              INTEGER Nullable 
ADH_SUR_ANNEE_CIVILE            VARCHAR(1) Nullable 
ACCESADM                        VARCHAR(1) Nullable 
PCLT_RECHERCHE                  VARCHAR(1) Nullable 
PCLT_PASSAGE                    VARCHAR(1) Nullable 
PCLT_BADGE                      VARCHAR(1) Nullable 
PFCT_QTE                        VARCHAR(1) Nullable 
PFCT_ACTIONS                    VARCHAR(1) Nullable 
PFCT_REGLT                      VARCHAR(1) Nullable 
ACT_ACCORD                      VARCHAR(1) Nullable 
ACT_BADGE                       VARCHAR(1) Nullable 
ACT_ADHESION                    VARCHAR(1) Nullable 
SAISIETYPECLT                   VARCHAR(1) Nullable 
IMPANNULTICK                    VARCHAR(1) Nullable 
SAISIEARTICLETACTILE            INTEGER Nullable 
LAST_NO_LOT_SELF                INTEGER Nullable 
CHQORDRE                        VARCHAR(48) Nullable 
CHQLIEU                         VARCHAR(12) Nullable 
MEMOACCORD                      VARCHAR(1) Nullable 
AFFICHEUR1                      VARCHAR(20) Nullable 
AFFICHEUR2                      VARCHAR(20) Nullable 
DCH_CBLONG                      INTEGER Nullable 
DCH_MTDEB                       INTEGER Nullable 
DCH_MTLONG                      INTEGER Nullable 
DCH_TYPDEB                      INTEGER Nullable 
DCH_TYPLONG                     INTEGER Nullable 
SUIVQTEDISPO                    INTEGER Nullable 
ENC_DATACARD                    VARCHAR(1) Nullable 
PREFIXE_BADGE                   VARCHAR(3) Nullable 
===================================================================================================================
Table PLANNING
===================================================================================================================
NO_PLANNING                     INTEGER Nullable 
LIBELLE                         VARCHAR(30) Nullable 
L1                              VARCHAR(1) Nullable 
L2                              VARCHAR(1) Nullable 
L3                              VARCHAR(1) Nullable 
L4                              VARCHAR(1) Nullable 
L5                              VARCHAR(1) Nullable 
M1                              VARCHAR(1) Nullable 
M2                              VARCHAR(1) Nullable 
M3                              VARCHAR(1) Nullable 
M4                              VARCHAR(1) Nullable 
M5                              VARCHAR(1) Nullable 
E1                              VARCHAR(1) Nullable 
E2                              VARCHAR(1) Nullable 
E3                              VARCHAR(1) Nullable 
E4                              VARCHAR(1) Nullable 
E5                              VARCHAR(1) Nullable 
J1                              VARCHAR(1) Nullable 
J2                              VARCHAR(1) Nullable 
J3                              VARCHAR(1) Nullable 
J4                              VARCHAR(1) Nullable 
J5                              VARCHAR(1) Nullable 
V1                              VARCHAR(1) Nullable 
V2                              VARCHAR(1) Nullable 
V3                              VARCHAR(1) Nullable 
V4                              VARCHAR(1) Nullable 
V5                              VARCHAR(1) Nullable 
S1                              VARCHAR(1) Nullable 
S2                              VARCHAR(1) Nullable 
S3                              VARCHAR(1) Nullable 
S4                              VARCHAR(1) Nullable 
S5                              VARCHAR(1) Nullable 
D1                              VARCHAR(1) Nullable 
D2                              VARCHAR(1) Nullable 
D3                              VARCHAR(1) Nullable 
D4                              VARCHAR(1) Nullable 
D5                              VARCHAR(1) Nullable 

Triggers on Table PLANNING:
SET_NO_PLANNING, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table RECETTES
===================================================================================================================
NO_RECETTE                      INTEGER Nullable 
NO_ARTICLE                      INTEGER Nullable 
NO_INGREDIENT                   INTEGER Nullable 
QUANTITE                        INTEGER Nullable 
UNITE                           VARCHAR(10) Nullable 
COEF                            DOUBLE PRECISION Nullable 

Triggers on Table RECETTES:
SET_NO_RECETTE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table REGLT_TIC
===================================================================================================================
NO_REGLT_TIC                    INTEGER Nullable 
NO_TICKET                       INTEGER Nullable 
MONTANT                         DOUBLE PRECISION Nullable 
NOMBRE                          INTEGER Nullable 
NO_TYPE_REGLT                   INTEGER Nullable 
NO_TYPE_REGLT_M                 INTEGER Nullable 
RETOUR_RAMPE                    VARCHAR(1) Nullable 
NOM_TRANSFERT                   VARCHAR(20) Nullable 
NO_LOT                          INTEGER Nullable 
DAT_DEPOT                       TIMESTAMP Nullable 
CPTA_REGLT                      TIMESTAMP Nullable 
NATURE                          INTEGER Nullable 
LIBELLE                         VARCHAR(30) Nullable 

Triggers on Table REGLT_TIC:
SET_NO_REGLT_TIC, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table RESERVATIONS
===================================================================================================================
NO_RESERVATION                  INTEGER Nullable 
NO_ARTICLE                      INTEGER Nullable 
NO_CLIENT                       INTEGER Nullable 
ETAT                            VARCHAR(10) Nullable 
PRIX                            DOUBLE PRECISION Nullable 
NO_SERVICE                      INTEGER Nullable 
LE                              TIMESTAMP Nullable 
NOMBRE                          INTEGER Nullable 
NOMBRE_PRIS                     INTEGER Nullable 

Triggers on Table RESERVATIONS:
SET_NO_RESA, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table SERVICES
===================================================================================================================
NO_SERVICE                      INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
ETAT                            VARCHAR(1) Nullable 
HEURE_DEB                       INTEGER Nullable 
MIN_DEB                         INTEGER Nullable 
HEURE_FIN                       INTEGER Nullable 
MIN_FIN                         INTEGER Nullable 
ACTIF                           VARCHAR(1) Nullable 
===================================================================================================================
Table TARIFS
===================================================================================================================
NO_TARIF                        INTEGER Nullable 
NO_ARTICLE                      INTEGER Nullable 
NO_TYPE_CLIENT                  INTEGER Nullable 
PRIX                            DOUBLE PRECISION Nullable 
MONNAIE                         VARCHAR(1) Nullable 
JOURNAL                         VARCHAR(10) Nullable 
NO_TVA                          INTEGER Nullable 
FORMULE                         VARCHAR(1) Nullable 
DIFFMT                          DOUBLE PRECISION Nullable 
PALIER                          INTEGER Nullable 
PRIXPLANCHER                    DOUBLE PRECISION Nullable 

Triggers on Table TARIFS:
SET_NO_TARIF, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table TICKETS
===================================================================================================================
NO_TICKET                       INTEGER Nullable 
NO_SERVICE                      INTEGER Nullable 
NO_CAISSE                       INTEGER Nullable 
NO_CLIENT                       INTEGER Nullable 
NO_TYPE_CLIENT                  INTEGER Nullable 
LE                              TIMESTAMP Nullable 
HEURE                           VARCHAR(8) Nullable 
NO_ACCORD                       INTEGER Nullable 
SOLDE                           DOUBLE PRECISION Nullable 
ANNULE_PAR                      INTEGER Nullable 
ANNULE_LE                       INTEGER Nullable 
NO_FACTURE                      VARCHAR(15) Nullable 
COMPTA_LE                       TIMESTAMP Nullable 
REGUL                           VARCHAR(1) Nullable 
COMMENTAIRE                     VARCHAR(100) Nullable 
OLD_NO_TICKET                   INTEGER Nullable 
SAISI_PAR                       VARCHAR(10) Nullable 
===================================================================================================================
Table TOUCHES
===================================================================================================================
TOUCHE                          VARCHAR(15) Nullable 
NO_CLAVIER                      INTEGER Nullable 
NO_ARTICLE                      INTEGER Nullable 
HAUTEUR                         INTEGER Nullable 
LARGEUR                         INTEGER Nullable 
NO_TYPE_REGLT                   INTEGER Nullable 
NO_ACTION                       INTEGER Nullable 
===================================================================================================================
Table TRANSFERT
===================================================================================================================
NOM                             VARCHAR(30) Nullable 
PRENOM                          VARCHAR(30) Nullable 
BADGE                           VARCHAR(20) Nullable 
SOLDE_AVANT                     DOUBLE PRECISION Nullable 
SOLDE_APRES                     DOUBLE PRECISION Nullable 
PRESTATIONS                     DOUBLE PRECISION Nullable 
MT_FORFAIT                      DOUBLE PRECISION Nullable 
MT_COMPLT                       DOUBLE PRECISION Nullable 
===================================================================================================================
Table TYPE_ADHESIONS
===================================================================================================================
NO_TYPE_ADHESION                INTEGER Nullable 
LIBELLE                         VARCHAR(50) Nullable 
PRIX                            DOUBLE PRECISION Nullable 
MONNAIE                         VARCHAR(1) Nullable 
COMPTE                          VARCHAR(15) Nullable 
JOURNAL                         VARCHAR(10) Nullable 
CODE_ANA                        VARCHAR(17) Nullable 
NO_TVA                          INTEGER Nullable 

Triggers on Table TYPE_ADHESIONS:
SET_NO_TYPE_ADHESION, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table TYPE_ARTICLES
===================================================================================================================
NO_TYPE_ARTICLE                 INTEGER Nullable 
LIBELLE                         VARCHAR(50) Nullable 
COULEUR                         VARCHAR(10) Nullable 
ALERTE                          VARCHAR(1) Nullable 

Triggers on Table TYPE_ARTICLES:
SET_NO_TYPE_ARTICLE, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table TYPE_CLIENTS
===================================================================================================================
NO_TYPE_CLIENT                  INTEGER Nullable 
LIBELLE                         VARCHAR(50) Nullable 
UTIL_DECOUV                     VARCHAR(1) Nullable 
DECOUV                          DOUBLE PRECISION Nullable 
MONNAIE_DECOUV                  VARCHAR(1) Nullable 
ADHESIONS_EN_CAISSE             VARCHAR(1) Nullable 

Triggers on Table TYPE_CLIENTS:
SET_NO_TYPE_CLIENT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table TYPE_REGLT
===================================================================================================================
NO_TYPE_REGLT                   INTEGER Nullable 
LIBELLE                         VARCHAR(40) Nullable 
MONNAIE                         VARCHAR(1) Nullable 
DETAIL                          VARCHAR(1) Nullable 
DEFAUT                          VARCHAR(1) Nullable 
COMPTE                          VARCHAR(15) Nullable 
JOURNAL                         VARCHAR(10) Nullable 
CLOTURE                         VARCHAR(1) Nullable 
TRI                             INTEGER Nullable 
NONAFFICHE                      VARCHAR(1) Nullable 
TPE                             VARCHAR(1) Nullable 

Triggers on Table TYPE_REGLT:
SET_NO_TYPE_REGLT, Sequence: 0, Type: BEFORE INSERT, Active
===================================================================================================================
Table VERSION
===================================================================================================================
NO_VERSION                      INTEGER Nullable 
===================================================================================================================
Table ZTBDETACC
===================================================================================================================
NO_FACTURE                      VARCHAR(15) Nullable 
NO_TICKET                       INTEGER Nullable 
NO_ARTICLE                      INTEGER Nullable 
MT_ACCORD                       DOUBLE PRECISION Nullable 
NO_DET_ACCORD                   INTEGER Nullable 
LIBELLE                         VARCHAR(30) Nullable 
QTE                             INTEGER Nullable 
